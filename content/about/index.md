+++
title= "About Me"
+++

{{<image-center src="pp-3.jpg" alt="sketch of me" size="40%" >}}

## BURAK TÜRKÖZ

Hi! My name is Burak and I'm an industrial designer. I am currently in the second year of my Master's in New Media at Aalto University, Finland. I also work as a student assistant at Aalto Fablab.

My main interest is utilizing makerspaces to bring a product from the ideation stage to a functioning prototype. Currently, I am working on creating a mobile makerspace for digital fabrication education as part of my master's thesis.

I began my journey through Fab Academy as a part of the academic overlay at Aalto in 2023. Now, I am working on getting the full diploma by going through the Global Fab Academy 2024. You can find the link to my new documentation website [here](https://fabacademy.org/2024/labs/aalto/students/burak-turkoz/)

If you want to learn more about me and my work, you can visit my [portfolio website](https://burakturkoz.gitlab.io/design-portfolio/).

<a href="/digital-fabrication-by-burak/docs/burak-turkoz-cv.pdf" download="burak-turkoz-cv.pdf">Download CV</a>
