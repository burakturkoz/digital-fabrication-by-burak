+++
title = "Applications and Implications"
week_order = 18
+++

## Bill of Materials

### Questions for Creating the BOM

Here are the answers to the questions that will help create the bill of materials. For more information on the planning of the final project, see [my final project page](/content/final-project/).

**What will the project do?**
- The project is an animatronic in the shape of a toucan. The aim is to achieve natural movements of the head, beak and hips of the bird. The movements will be adjustable and controllable by the user- specifically their speed and angle. The control interface will consist of a web interface that communicates with the microcontroller through a wireless network.


**Who has done what beforehand?** 
- My main inspiration for this project is the [Animatronic raven DIY kit](https://www.youtube.com/watch?v=y0R8-F4TmPI&list=LL&index=5) by Jasper J. Anderson. In addition, I will also employ principles from other DIY animatronic projects as well, such as [this eye animatronic](https://www.youtube.com/watch?v=DxjTiZKzxVo) and [this one](https://www.youtube.com/watch?v=Ftt9e8xnKE4&t=378s)

**What will you design?**
- My contribution to the pre-existing raven project by Jasper Anderson will be to make the movements of the animatronic more natural, variable, and controllable by the user. I want my animatronic to be able to move at varying speeds, and have an interface that allows me to precisely control them. I also opted for replacing the servo motors for steppers in my design for better speed control. Taking all of these changes into account, I need to design a body to accommodate the electronics I am using, a circuit board and it's schematics, a head - body connection with universal joint and wires that allows for free movement, and a network interface for precise control of the steppers.

### Bill of Materials

{{< iframe >}}
{{< iframe >}}
<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vTZ79bxYeJ6w-CcjFVHnSs6JlxOPxNWpMuyj-9Gcy_Xb2-oMWJlr6LdZeoQwOiSpCIZdjfmMd_Q0ncw/pubhtml?widget=true&amp;headers=false" style="width: 800px; height: 600px;"></iframe>
{{< /iframe >}}
{{< /iframe >}}

#### Electronic Components

(Digikey pages of components are included in the links down below)

1. [ROB-10551 Stepper Motor](https://www.digikey.fi/en/products/detail/sparkfun-electronics/ROB-10551/5766908)
- **Quantity:** 3
- **Unit Price:** 8.65e

2. [TMC 2208 Stepper Motor Driver](https://www.digikey.fi/en/products/detail/trinamic-motion-control-gmbh/TMC2208-LA-T/6572858)
- **Quantity:** 3
- **Unit Price:** 2.35e

3. [Seeed Studio XIAO ESP32C WIFI+B Microcontoller](https://www.digikey.fi/en/products/detail/seeed-technology-co-ltd/113991054/16652880)
- **Quantity:** 1
- **Unit Price:** 4.62e

4. [Mornsun LM15-23B12 Enclosed AC/DC Converter](https://www.digikey.fi/en/products/detail/mornsun-america-llc/LM15-23B12-C/15214141)
- **Quantity:** 1
- **Unit Price:** 10.11e

5. [Rocker Switch](https://www.digikey.fi/en/products/detail/nkk-switches/CWSB11AA1F/671493)
- **Quantity:** 1
- **Unit Price:** 3.03e
- (The digikey page and unit price for this item is taken from a similar product, since I could not find the digikey page of the actual component I used.)

6. [AC Power Connector with Fuse](https://www.digikey.fi/en/products/detail/schurter-inc/GSF2-2011-01/641118)
- **Quantity:** 1
- **Unit Price:** 6.17e
- (The digikey page and unit price for this item is taken from a similar product, since I could not find the digikey page of the actual component I used.)

7. Jumper cables
- **Quantity**: 14

---

#### Body & Box Structure

1. Acrylic Sheet Semi-Opaque 3mm
- **Manufacturing Process:** Laser cutting
- **Required Minimum Material Area:** 100x100 cm

---

#### Head & Beak

1. Sikablock (for vacuum forming mold)
- **Manufacturing Process:** CNC milling
- **Material Size:** 7x22x5.5 cm 

2. White Polystyrene Sheet 0.5mm
- **Manufacturing Process:** Vacuum forming 
- **Material Size:** 53x47 cm 
- **Material Price:** 0.99e [(from the Aalto Studios pricelist)](https://studios.aalto.fi/fablab/)

---

#### Universal Joint

1. Main Joint
- **Manufacturing Process:** FDM 3D Printing
- (Made by me)

2. [Bearings (4x13x5 mm)](https://www.mcmaster.com/products/bearings/bearings-1~/system-of-measurement~metric/id~4-000-mm/id~4-0-mm/id~4-mm/od~13-000-mm/od~13-0-mm/od~13-mm/width~5-000-mm/width~5-mm/)
- **Quantity:** 2
- **Unit Price:** 6.71

---

#### Nuts & Screws

1. M2 Screws
- **Quantity:** 2

2. M3 Screws
- **Quantity:** 6

3. M4 Screws
- **Quantity:** 2

4. M5 Screws
- **Quantity:** 3

5. M3 Nuts
- **Quantity:** 6

6. M4 Nuts
- **Quantity:** 4

7. M5 Nuts
- **Quantity:** 3

8. M5 Washers
- **Quantity:** 2

---

#### Other Parts

1. Stepper End Wheels
- (Made by me)
- **Quantity**: 2
- **Manufacturing Process:** FDM 3D Printing

2. Steel Wires
- **Length**: 30cm

