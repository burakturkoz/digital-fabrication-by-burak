+++
title = "Principles and Practices"
week_order = 2
+++

## What is Hugo and Why Is Hugo?

### Setting Up

In order to streamline the process of making new HTML pages and copying the necessary code between them, we can use Hugo. It is a static website generator that allows us to automate many parts of maintaining a website.


#### Download HUGO

To install Hugo for Windows, [go here](https://github.com/gohugoio/hugo/releases).
For windows, amd64.zip is the file that I downloaded. 

After unzipping, you will find a file called "hugo". Create a new folder somewhere and put it inside.


#### New Beginnings for HUGO

Locate to this folder within GitBASH. You can type `ls` to see that hugo is in fact in the folder. You can also type ./hugo version to see which version you are running.

However, for GitBASH to find and use Hugo it needs to be located in a specific folder. To know where this folder is, we can type `which mv`.

After learning about this folder, we need to move hugo here by `mv hugo /folderURL` on Windows.

Now, we can go to any directory in our system by `cd` and run hugo. Try going into different folder and running `hugo version`.

After making sure hugo works, we locate to the folder where we want our website to be in. Type `hugo new site` to start creating the hugo site.

Now if we type `ls`, we can see which new files hugo has created after it started the site. We don't need every one of them so we can start removing some.

`rm -rf archetypes assets data public themes`

After this, we can open the file in Visual Studio Code. Inside "config.toml", we should change the baseURL with the URL of our existing website.

In here we can also change the title of our website. 

#### Populating the Website

We can start populating our website by creating our first HTML file. Create "index.html" under the "layouts" folder. For now, we structured this page like a standart HTML page, like the ones we did before.

To view it locally, type `hugo serve` in GitBASH. You can access the website by copying the link GitBASH

We can write the content inside the HTML file like we did before. But, there is an easier way with Hugo.

In the content directory, create a new .md file. Name it "_index.md". This is where the homepage draws content from.

Here is the markdown file for my homepage: 

```go
+++
title = "Home"
+++

Welcome to my Digital Fabrication Documentation Homepage

![Picture of a bear](bear.jpg)
```
But this page will not appear until we refer to it in the index.html. The syntax for referencing variables is like this:

> `{{ .Variablename }}`

Below is an example on how to use them in the index.html file. `{{ .Site.Title }}` is the title variable we established in the previous step. `{{ .Content }}` refers to the "_index.md" file under the content folder.

```go
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>{{ .Site.Title }}</title>
</head>
<body>
  <h1>{{ .Site.Title }}</h1>
  
  {{ .Content }}
</body>
</html>
```

Now we move to making more pages. To add an "About" page for example, we create a folder titled "about" under "content". Within this folder, we create a markdown file titled "index.md". There is no "_" is time.

The structure is the same as the previous markdown file we created:

```go
+++
title= "About Me"
+++

This page tells about me.
```

For this page to work howevever, we need another html file. The HTML file for the homepage (index.html) was under "layouts". For the single pages, we need to create another one. 

Create a folder titled "_default" under "layouts". Inside this folder, create an HTML file named "single.html". This will be the HTML file for all of our single pages. We can copy the contents of "index.html" exactly as they are and paste them to "single.html". Now, if we add /about to the end of the URL, we can go to the about page.

But for normal navigation we need to add a navigation bar. We can add this to the `<body></body>` section of both "_index.html" and "single.html". Here is what it looks like:
```go
<nav>
  <ul>
    <li><a href="{{ .Site.BaseURL }}">Home</a></li>
    <li><a href="{{ .Site.BaseURL }}/about">About</a></li>
  </ul>
</nav>
```

#### Partials

In Hugo, we can streamline this process of writing endless HTML code by using partials. They are tools that allow us to write an HTML code one time, and then reference that with a single line of code.

To add partials, we create a folder called "partials" under "layouts".

We can start by making a navigation partial. We create a file named "nav.html" under "partials". We then copy the navigation code to this file.

In order to refer to this nav in other HTML files, it is enough to write:
> `{{ partial "nav.html" . }}`

We can repeat the same process for the other components of the website as well:

> **Head**
> > ```go
> > <!DOCTYPE html>
> > <html lang="en">
> > <head>
> >  <meta charset="UTF-8">
> >  <meta http-equiv="X-UA-Compatible" content="IE=edge">
> >  <meta name="viewport" content="width=device-width, initial-scale=1.0">
> >  <title>{{ .Title }} : {{ .Site.Title }}</title>
> >  <style>
> >    img {
> >      width: 50%
> >    }
> >  </style>
> > </head>
> > <body>
> > ```
> **Foot**
> > ```go
> > </body>
> > </html>
> > ```
> **Footer**
> > ```go
> > <footer>
> >  <p>&copy;{{ .Site.Copyright }}</p>
> > </footer>
> > ```

These partials are referenced as follows in the index.html:
```go
{{ partial "head.html" . }}

  {{ partial "nav.html" . }}

  <h1>{{ .Title }}</h1>
  
  {{ .Content }}

{{ partial "footer.html" . }}

{{ partial "foot.html" . }}
```

By repeating all these processes as needed, we can populate our website:
> Markdown files inside new folders for new pages.
> Partials for creating new code blocks to reference elsewhere.

Here is what the final file tree looks like:

![hugofiletree](tree.JPG)

#### Adding Media

To add media, we put the media file inside the folder which has the markdown file containing the content of the page. 

Then, we follow the image and video adding convention outlined [here](https://about.gitlab.com/handbook/markdown-guide/#images) and [here](https://about.gitlab.com/handbook/markdown-guide/#videos)

#### How Do We Get this to GitLab?

After logging into GitLab, we need to clone our existing repository. In the GitLab repository, copy the link under "Clone".

In GitBASH, locate the folder you want to clone the repository to. Type `git clone [clone link]`. The repository will be cloned to the desired folder.

Now, we can delete all the folders inside except readme.md

Then we go back to the Hugo folder and copy it's contents to the clone folder. What we need to copy is "content", "layouts", "static" and "config.toml".

If we type `git status` now, we can see a lot of files had been changed. The command `git add .` allows us to add all the changes at once. Then we `git commit` the changes. Finally, we `git push`.

When we refresh the GitLab page we can see that the new files are now in the repository.

After `git push`, we still need to do stuff. Like modifying the ci.yml file. Now it is configured for HTML. We need to change it to Hugo.

In GitLab to "New File", name it .gitlab-ci.yml and apply the template "Hugo". We won't need this file, since we already have a ci.yml file. We only need the text it generates. Copy it.

In the local websiten directory, open ".gitlab-ci.yml" in VSCode. We can see that it is still configured to generate a static HTML page. We will delete all of it and paste the new Hugo code.

After saving the changes, we can go through the usual `git status` `git add` `git commit` `git push`.

We can now go back to GitLab and see that it has created the pipeline succesfully.

Then BAM, website is online.