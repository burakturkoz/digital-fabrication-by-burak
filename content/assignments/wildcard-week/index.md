+++
title= "Wildcard Week"
week_order=17
+++

## Vacuum Forming

### Designing the Mold

#### CAD Modeling

For the wildcard week, I decided to try out the vacuum forming machine. It was something I had never worked with before so it was a new challenge for me.

The first step is to create a mold for the plastic to form around. We can create this out of a variety of materials, as long as it does not deform under heat. The vacuum forming process utilizes heat to soften sheet plastic and make it stretchable, then we lower the heated sheet onto the mold. During this step the mold also gets hot, so we must prevent it from sticking it to the plastic or losing it's shape.

I decided to make a small version of the toucan's head that I was going to need for my final project. The wildcard week would act as a proof of concept, and if it works, I will scale it up to use in the final project.

I used Fusion 360 to model the head. I also made a base for it, so it can stand on the vacuum forming bed properly.

{{<image src="s1.jpg" alt="toucan head" size="48%" >}}
{{<image src="s2.jpg" alt="toucan head with base" size="48%" >}}

#### Generating Toolpaths

I created the CNC toolpaths in Fusion's CAM interface. I used "3D Adaptive Clearing" for milling the general shape, and "3D Parallel Clearing" for the finishing passes. For the tool, I chose a 6mm two-flute endmill for all operations.

{{<image src="s3.jpg" alt="toucan head" size="48%" >}}

### CNC Milling

I chose Sikablock as the stock material for milling. It does not deform under heat and is softer compared to wood, therefore can be milled faster.

{{<video src="output1.mp4">}}Parallel finishing passes{{</video>}}

The result turned out well, with great surface finish quality. The only issue was that the 6mm endmill could not enter the sharp & deep corners at the back.

{{<image src="i1.jpg" alt="milling result" size="48%" >}}
{{<image src="i3.jpg" alt="milling result" size="48%" >}}
{{<image src="i2.jpg" alt="milling result" size="48%" >}}

### Vacuum Forming

The vacuum forming machine we have at the lab is a "C.R. Clarke Vacuum Former 70FLB". The material I used was a 0.5mm Polystyrene sheet. Here is how the machine is operated:

{{<image src="i4.jpg" alt="Vacuum Former" size="48%" >}}

1. Turn on the machine with the button on the left.
2. Make sure all the knobs are set to "FULL".
3. Turn ON the vacuum switch.
4. Leave it to heat up for 15 minutes.
5. In the meantime, set the correct heating time for your material from the screen on the right. The information is printed on a small sticker above.
6. Choose an appropriately sized black mesh plate and place it on the bottom of the machine's opening.
7. Place your mold in the middle of the mesh plate.
8. Choose an appropriately sized clamp plate and place it on the top of the opening.
9. Clamp the plate using the metal rectangle.
10. Place your sheet material of choice on top of the plate, and clamp it into place. Make sure it covers all of the opening, including the seals. (See picture below)
11. When the machine is heated up, and everything is clamped in place, you can start the heating process.
12. Pull the yellow heater box from the handle and slide it on top of the material. The counter will go off, and the material will start to be heated.
13. When the counter reaches zero, you will hear a beep. Push the heater away in it's original place.
14. Now we can start the vacuuming. Make sure the small vacuum switch is set to ON. Then, press the black vacuum button to start the vacuum. You might need to press it twice to activate it. You will hear the vacuum when it goes off.
15. Slowly pull the lever on the left. This will elevate the bottom platform where the mold is sitting on. When the lever is fully pulled, the sheet material will start to wrap around the mold.
16. WAIT for the sheet to completely surround itself over the mold. This can take 5-10 seconds.
17. Then, press the vacuum button again. This will suck air and allow the sheet to wrap itself tighter around the mold. Do this a few times.
18. When the sheet is satisfactorily wrapped around the mold, slowly push the lever back. You might need to hit the vacuum button briefly to seperate the mold from the sheet. Push the lever fully and lower the mold.
19. Turn off the vacuum. Open the clamps and take out the sheet material and mold.
20. Turn off the machine after you're done.

{{<image src="i5.jpg" alt="sheet placement" size="48%" >}}

Here are my results from the vacuum former.

{{<image src="i6.jpg" alt="result" size="48%" >}}

Then, I cleaned up the polystyrene sheet using a box cutter and scissors. I learned that you need to be very careful during this process, because the material can tear itself very fast. Also, if you want good edge quality, you need to be careful with the box cut.

{{<image src="i7.jpg" alt="cleaning up" size="48%" >}}
{{<image src="i8.jpg" alt="cleaning up" size="48%" >}}

{{<image src="i9.jpg" alt="cleaning up" size="48%" >}}
{{<image src="i10.jpg" alt="cleaning up" size="48%" >}}

It turned out well as a proof of concept for me. I decided to scale the model up, clean up the CNC mill path, and use the same process for the full-sized toucan head.