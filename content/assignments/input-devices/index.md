+++
title="Input Devices"
week_order=13
+++

## Thermistor

### Designing the Circuit Board

For the input devices week I decided to try out to use a thermistor. A thermistor utilizes resistance to measure temperature. Every thermistor has a specific internal resistance, which can be measured using a voltage divider circuit, and then converted to temperature using an equation.

The thermistor I used was a [10K NTC Thermistor](https://www.digikey.fi/en/products/detail/amphenol-thermometrics/NHQ103B375T10/374815).

To learn about thermistors, I used [this guide](https://www.electronics-tutorials.ws/io/thermistors.html) and [this guide](https://circuitdigest.com/microcontroller-projects/interfacing-Thermistor-with-arduino).

By following the [tutorial here](https://circuitdigest.com/microcontroller-projects/interfacing-Thermistor-with-arduino), I designed a circuit board for the thermistor setup. Since my thermistor was 10K, I used a 10K reference resistor.

{{<image src="s1.jpg" alt="board design" size="48%" >}}

### Milling & Assembling 

I moved on to milling the board. In my first try, the board broke at the contouring stage of the milling. 

{{<image src="i1.jpg" alt="broken board" size="48%" >}}

I figured that this was because the board was much smaller than the ones I did before, and it flew away when cutting the last layer. To solve this, I reduced the speed of the cutting tool to 50% at the last stage. After this, I did the soldering and connected it to the XIAO.

{{<image src="i2.jpg" alt="finished board" size="48%" >}}
{{<image src="i3.jpg" alt="finished board" size="48%" >}}

### Thermistor Code

I followed the code in the [tutorial](https://circuitdigest.com/microcontroller-projects/interfacing-Thermistor-with-arduino) and adapted the defined values to the specific thermistor that I was using. Using this code, I was able to read the resistance in the thermistor and convert it into a temperature value. 

{{<video src="output1.mp4">}}Reading temperature through termistor{{</video>}}

```go
/*!
 * @file        Arduino_NTC_Interface.ino
 * @brief       Interfacing NTC Thermistor With Arduino
 * @author      Jobit Joseph
 * @copyright   Copyright (c) 2022 Semicon Media Pvt Ltd (https://circuitdigest.com)
 */

#define ntc_pin A0         // Pin,to which the voltage divider is connected
#define vd_power_pin D2        // 5V for the voltage divider
#define nominal_resistance 10000       //Nominal resistance at 25⁰C
#define nominal_temeprature 25   // temperature for nominal resistance (almost always 25⁰ C)
#define samplingrate 5    // Number of samples
#define beta 3750  // The beta coefficient or the B value of the thermistor (usually 3000-4000) check the datasheet for the accurate value.
#define Rref 10000   //Value of  resistor used for the voltage divider

int samples = 0;   //array to store the samples

void setup(void) {
  pinMode(vd_power_pin, OUTPUT);
  Serial.begin(19200);  
}

void loop(void) {
  uint8_t i;
  float average;
  
  samples = 0; // take voltage readings from the voltage divider
  digitalWrite(vd_power_pin, HIGH);
  for (i = 0; i < samplingrate; i++) {
    samples += analogRead(ntc_pin);
    delay(10);
  }
  digitalWrite(vd_power_pin, LOW);
  
  average = 0;
  average = samples / samplingrate;
  Serial.println("\n \n");
  Serial.print("ADC readings ");
  Serial.println(average);

  // Calculate NTC resistance
  average = 1023 / average - 1;
  average = Rref / average;
  Serial.print("Thermistor resistance ");
  Serial.println(average);
  
  float temperature;
  temperature = average / nominal_resistance;     // (R/Ro)
  temperature = log(temperature);                  // ln(R/Ro)
  temperature /= beta;                   // 1/B * ln(R/Ro)
  temperature += 1.0 / (nominal_temeprature + 273.15); // + (1/To)
  temperature = 1.0 / temperature;                 // Invert
  temperature -= 273.15;                         // convert absolute temp to C
  Serial.print("Temperature ");
  Serial.print(temperature);
  Serial.println(" *C");
  
  delay(2000);
}
```

### Controlling Stepper Motor with Thermistor

I wanted to combine what I learned in the output devices week with the input devices week. So, I utilized the temperature reading from the thermistor to control the speed of a stepper motor. I combined the code above with one of the codes I used in the output week.

After the temperature is read, the "ADC Readings" value is mapped to the interval value of the stepper. Therefore, when the temperature becomes higher, the interval between steps decrease- resulting in the motor turning faster.

I also learned to use a `constrain` function to limit the mininum and maximum number the interval could take, to avoid putting too much pressure on the stepper accidentally.

{{<image src="i4.jpg" alt="thermistor + motor" size="48%" >}}


{{<video src="output2.mp4">}}Controlling stepper motor with thermistor{{</video>}}

```go
#define ntc_pin A0         // Pin,to which the voltage divider is connected
#define vd_power_pin D2        // 5V for the voltage divider
#define samplingrate 5    // Number of samples
#define DIR_PIN D5       // Direction pin for stepper
#define STEP_PIN D4      // Step pin for stepper
#define ENABLE_PIN D6    // Enable pin for stepper
int samples = 0;   //array to store the samples
float average;

void setup(void) {
  Serial.begin(19200);  
  pinMode(vd_power_pin, OUTPUT);
  pinMode(STEP_PIN,OUTPUT);
  pinMode(DIR_PIN,OUTPUT);
  pinMode(ENABLE_PIN,OUTPUT);  
  digitalWrite(ENABLE_PIN, LOW);  
}


void loop(void) {
  readTemp(); 

  int mappedAverage=map(average, 440,550,1000,4000);
  mappedAverage = constrain(mappedAverage, 800, 4000);
  Serial.println(mappedAverage);

  oneTurn(mappedAverage);


  delay(2000);  
}

void readTemp(){
  uint8_t i;
  
  samples = 0; // take voltage readings from the voltage divider
  digitalWrite(vd_power_pin, HIGH);
  for (i = 0; i < samplingrate; i++) {
    samples += analogRead(ntc_pin);
    delay(10);
  }
  digitalWrite(vd_power_pin, LOW);
  
  average = 0;
  average = samples / samplingrate;
  Serial.println("\n \n");
  Serial.print("ADC readings ");
  Serial.println(average);

  delay(10);
}

void oneTurn(int interval){
  int steps = 385;
  for (int i=0; i < steps; i++){
    digitalWrite(STEP_PIN, HIGH);
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(interval);
  } 
}
```