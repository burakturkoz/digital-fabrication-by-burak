+++
title = "Laser Cutting"
week_order = 5
+++

## Modeling for Laser Cutting

### My Aim

While I was searching the internet for laser cutting projects, I came across this "screw reinforced joint". It was interesting to me because I would like to experiment with combining laser cut profiles with other materials and joining methods. I decided to give it a try.

{{<image src="screwnew.jpg" alt="screw reinfored joint" size="45%" >}}

### Demo Piece with FreeCAD

After last week, I wanted to try out FreeCAD as an alternative to Fusion 360. It was my first time using this software, so I followed this [tutorial](https://www.youtube.com/watch?v=u8otDF_C_fw&t=1502s).

#### Some Setting Up

The default settings for FreeCAD need some tweaking to ensure a smooth user experience. When you open the settings, this is the window that you should see:

{{<image src="s2.jpg" alt="setting window" size="45%" >}}

Here are some settings I changed with the help of the tutorial:

{{<image src="i1.jpg" alt="changing settings" size="45%" >}}

{{<image src="i2.jpg" alt="changing settings" size="45%" >}}

{{<image src="i3.jpg" alt="changing settings" size="20%" >}}
{{<image src="i4.jpg" alt="changing settings" size="20%" >}}

{{<image src="i5.jpg" alt="changing settings" size="20%" >}}
{{<image src="i8.jpg" alt="changing settings" size="20%" >}}

{{<image src="i7.jpg" alt="changing settings" size="20%" >}}
{{<image src="i6.jpg" alt="changing settings" size="20%" >}}

#### Modeling

I wanted to model something simple since it was my first time using FreeCAD, therefore I decided to replicate the screw reinforced joint example I found online. 

In FreeCAD, there are several "workspaces" that allow the user to utilize different parts of the CAD workflow. For our purposes, we use the "Part Design" workspace. 

The workflow in "Part Design" goes like this: You first "create part", this is the same as a "component" in Fusion 360. Inside the part, you "create sketch".

{{<image src="i10.jpg" alt="creating body" size="20%" >}}
{{<image src="i11.jpg" alt="creating sketch" size="20%" >}}

I first started by sketching the profile. In contrast to Fusion, FreeCAD doesn't provide snapping or automatically make constraints. You have to manually select lines and then select constraints from the upper menu. After making sure that all the lines are defined and constrained, I mirrored the sketch to finalize the profile. 

{{<image src="s4.jpg" alt="sketching" size="45%" >}}
{{<image src="s5.jpg" alt="sketching" size="45%" >}}

The extrusion command in FreeCAD is called "pad". To "pad" a sketch, it has to be a fully closed profile. Additionally, it can't be more than one profile, as FreeCAD doesn't allow selecting which profile you want to "pad". I extruded the sketch to be 3mm, the thickness of the cardboard in the lab.

{{<image src="s6.jpg" alt="padded part" size="45%" >}}

I replicated the same workflow and created another body and sketch. The final component tree and the finished part looks like this:

{{<image src="i13.jpg" alt="tree" size="20%" >}}
{{<image src="s8.jpg" alt="finished model" size="45%" >}}

#### Exporting as DXF

In order to export this piece as a DXF file, we have to use the "TechDraw" workspace. You select the workspace from the drop down menu and click "insert default page". After this, we click on the body in the tree, and "insert view".

{{<image src="i14.jpg" alt="techdraw interface" size="17%" >}}
{{<image src="i15.jpg" alt="techdraw interface" size="17%" >}}

At first, the vertices look huge and distracting. We can open settings and set their size to zero. After doing this, we can place the drawings wherever we want on the page. Using the export settings, we then export the drawings as a DXF file.

{{<image src="s8.5.jpg" alt="techdraw interface" size="30%" >}}
{{<image src="s8.6.jpg" alt="techdraw interface" size="30%" >}}
{{<image src="s8.7.jpg" alt="techdraw interface" size="30%" >}}

### Main Piece with Fusion 360

After my trial with FreeCAD, I wanted to make a more complicated piece to test the "screw reinforced joint". I had the idea to make a moving part by using the same principle, while also using the screws to create a hinge movement. I decided to design an adjustable phone holder, since I actually needed one.

#### Setting Parameters & McMaster Carr

As always, I started by setting parameters in Fusion to make changes easier in the future. For this project I also utilized the "McMaster Carr" parts import function embedded inside the software. This is a tool which allows us to import a variety of mechanical parts into our design. I imported the screws and nuts that I wanted to use, to design the part around their dimensions.

{{<image src="s9.jpg" alt="mcmaster carr importing" size="45%" >}}
{{<image src="s10.jpg" alt="mcmaster carr importing" size="45%" >}}

#### Modeling

I modeled the piece with using the same workflow I outlined in the previous week. I made three seperate pieces, one for the bottom, one for the top which would carry the phone, and a middle piece. Every piece consists of 3mm plywood profiles. There are holes which allow for the "screw reinforced joint" method.

{{<image src="s11.jpg" alt="finished model" size="45%" >}}
{{<image src="s12.jpg" alt="finished model" size="45%" >}}

#### Exporting with DXF Add-In

I had never previously exported anything from Fusion for laser cutting. While researching, I stumbled upon an add-in for Fusion. It allows us to insert the kerf value in the very end of the process, and adjusts the DXF files accordingly. This way, we can design the part in Fusion like we want it to look in real life and change the kerf very easily.

After finishing the model, we activate the add-in from the drop down menu. We then select faces to be turned into dxf profiles for laser cutting. After entering the kerf, it exports. We transfer the files to illustrator to collect them into one file.

{{<image src="i17.jpg" alt="dxf add-in" size="20%" >}}
{{<image src="s15.jpg" alt="illustrator file" size="45%" >}}

## Using the Laser Cutter

### Cardboard Prototypes

Before moving onto plywood, I decided to test the prototypes on cardboard. After putting my piece inside the laser cutter, I calibrated the focus of the laser. We do this by adjusting the small metal piece near the laser, and ensuring that it provides a bit of a friction. 

After importing our design to the computer in the lab, we open it in Illustrator. We then "print" it with "media size" set to "custom". This opens the laser cutter interface. We place our design with the help of the camera. It is better to do the trial cuts in the bottom right, because that is the place where the machine is the least accurate. If our cut works there, it will work anywhere.

I chose the settings for "cardboard 3mm" and started cutting.

{{<image src="c1.jpg" alt="calibrating" size="35%" >}}
{{<image src="c2.jpg" alt="cardboard settings" size="20%" >}}

After cutting, I started assembling the pieces. There were some apparent issues.

{{<image src="c3.jpg" alt="cardboard pieces" size="35%" >}}

The dimensions of the pieces were not what I wanted them to be. The notches on the first piece did not fit the second one. The screw holes were too big for the screws that I was using. The reason for these inconsistencies were two things. Firstly I did not measure the kerf correctly for cardboard, and secondly I imported the wrong size screw from McMaster Carr (M3 instead of M2).

{{<image src="c4.jpg" alt="notch inconsistency" size="35%" >}}
{{<image src="c5.jpg" alt="too big holes" size="35%" >}}

For the phone holder piece, things were not looking well either. In addition to the dimensional errors, the structure of the piece was also not working. The middle piece was too short and caused imbalance. Although some of these structural issues were caused by the nature of cardboard, most were due to my measurement errors. I had to fix my design before moving onto plywood.

{{<image src="c7.jpg" alt="bottom piece" size="35%" >}}
{{<image src="c8.jpg" alt="top piece" size="35%" >}}
{{<image src="c9.jpg" alt="top piece-imbalance" size="35%" >}}

### Plywood Kerf Measurements and Misery

Before going back to CAD, I had to measure the kerf for plywood. I drew 10mm squares in Illustrator and imported them to the laser cut software. However, the laser cutter did not cut, it only burned the top of the wood. 

{{<image src="c10.jpg" alt="laser settings" size="20%" >}}
{{<image src="c11.jpg" alt="result in plywood" size="35%" >}}

I realized this was because I had not set the line width to "0.001mm" in Illustrator. I went back and did that, but the laser cutter did not cut again.

{{<image src="c12.jpg" alt="laser settings" size="20%" >}}
{{<image src="c13.jpg" alt="result in plywood" size="35%" >}}

The power and speed settings looked fine, so I tried changing the location of the plywood to the upper left corner of the machine. This time it cut, but when I attempted to remove the piece, all the small squares fell into the laser cutter. 

{{<image src="c14.jpg" alt="misery" size="35%" >}}

I realized why nobody else was using 10mm squares. I repeated the same steps with 20mm. It worked. For kerf measurement, I lined up the 5 pieces and measured with a caliper. I subtracted the real length (99mm) from the expected length (100mm), and divided the difference with 5. The kerf was 0.2mm for 3mm plywood.

{{<image src="c15.jpg" alt="kerf measurement" size="35%" >}}

## Second Iteration

### Fixing the CAD Model

I went back to Fusion 360 and fixed the previous issues. It was a fast process because I was working with parameters. I changed the hole sizes to accomodate for M2 screws and nuts, and made the middle piece longer. When exporting I entered the kerf as "0.2 mm".

{{<image src="s16.jpg" alt="changing parameters" size="45%" >}}
{{<image src="s17.jpg" alt="changing middle piece length" size="45%" >}}

### Laser Cutting with Plywood

When I was cutting with cardboard, I realized an error about the order the machine was cutting the pieces. For pieces with holes inside it was cutting the contour first, which made the piece fall down. Then it was cutting the holes, resulting in inconsistencies. To prevent this, I gave a different color to the prioritized cuts in Illustrator.

This way, in the laser cutter software I made sure that the red lines would be cut first.

{{<image src="c17.jpg" alt="changing parameters" size="20%" >}}
{{<image src="c16.jpg" alt="changing middle piece length" size="20%" >}}

After cutting, I assembled the pieces. The dimensions were much more accurate than the first iteration with cardboard. Although some notches were too tight so I had to sand them down a bit. The screw holes were working fine, but they could have been a bit smaller for a tighter fit. For my next projects with the "screw reinforced joint" method, I will make the nut and screw holes tighter than they actually need to be.

All in all, I think the design works as intented in the second iteration. The hinge mechanism allows for the readjustment of the piece, although it can be more refined.

{{<image src="c18.jpg" alt="final product" size="35%" >}}
{{<image src="c19.jpg" alt="final product" size="35%" >}}

{{<image src="c20.jpg" alt="final product" size="35%" >}}
{{<image src="c21.jpg" alt="final product" size="35%" >}}



