+++
title = "Computer-Aided Design"
week_order = 4
+++

## Basics of CAD Software

### Using Fusion 360 to Model a Sample Final Project

This week, we covered the basics of CAD software. We looked at some alternatives like FreeCAD or SolveSpace. 

For the program that I was going to use, I chose Autodesk Fusion 360. This is a program I have quite a bit of experience in, therefore I am the most comfortable with it.

#### Ideation

Before starting the modeling on Fusion, I ideated on possible alternatives.

I did some reflection my final project topic, which you can find in the "final project page".

{{<image src="sketchsmall.jpg" alt="final project sketches" size="60%" >}}

I decided to go with the "feed me trashcan". This is a trashcan that constantly bothers you asking for food. It will have wheels that will move it around, and a moving mouth. And maybe it will make sound.

#### Setting Parameters

Before starting modeling, I always set parameters. A parameter in modeling allows us to set certain values to certain variables. These can all be changed later on, which provides a huge advantage when we want to modify a value that we set in the beginning.

{{<image src="img1.jpg" alt="parameters" size="60%" >}}

You can see here that I have created parameters for variables such as the diameter of the base plate. If I decide to make the base larger later on in the process, I can simply change this parameter and the model will adapt accordingly.

#### Modeling

For the main body of the can, I sketched the profile on a plane. Then, I used the _revolve_ tool to create my first solid body.

{{<image src="img2.jpg" alt="2d sketch" size="45%" >}}
{{<image src="img3.jpg" alt="revolving" size="45%" >}}

Then I added the upper parts with the _revolve_ tool again.

{{<image src="img4.jpg" alt="upper parts" size="45%">}}

After the body was complete, I moved on the other component: the rotating head. For this, I created a seperate component first. In Fusion 360, we always create components that have a single "body" in them. Every object that is seperate from each other should be a different component.

I experimented with a few arrangements on how the moving head would be modeled. Some of them did not work due to the placement of the hinge and the position of the mouthpiece relative to it, so I had to scrap them. Here are the options I worked with.

{{<image src="img5.jpg" alt="experiment 1" size="45%" >}}
{{<image src="img6.jpg" alt="experiment 2" size="45%" >}}

{{<image src="img7.jpg" alt="experiment 3" size="45%" >}}
{{<image src="img8.jpg" alt="experiment 4" size="45%" >}}

I settled on the last option, since it provided a solid hinge point and it was easy to model. Here is how the upper mouth piece moves in combination with the body:

{{<image src="img9.jpg" alt="head-3d view" size="45%" >}}
{{<image src="img10.jpg" alt="head-section" size="45%" >}}

#### Adding .STL Files

For components such as motors or wheels, I prefer to use ready-made models. Since they provide accurate measurements, especially if I am able to find the exact model of the component that I am going to use.

Fusion works well with .STEP or .SLDPRT files, so I use [Grabcad](https://grabcad.com/library) to find models.

For this model I used models for a step motor, a dc-motor and wheel assembly, and a 360 wheel.

I modeled the holes that they would be mounted on.

{{<image src="img11.jpg" alt="stepper motor placement" size="45%" >}}
{{<image src="img13.jpg" alt="wheels placement" size="45%" >}}

I added the 360 wheel as well. I made a small mounting point at the bottom for it and also closed off the top of the wheel enclosure. Here is how it looks:

{{<image src="img15.jpg" alt="wheels" size="45%" >}}
{{<image src="img16.jpg" alt="wheels section" size="45%" >}}

#### Filleting

After adding these, the only thing left was to do some filleting/ chamfering. This is important both from a manufacturing standpoint, because it allows for smoother production in many different manufacturing types; and from a rendering standpoint, because without fillets the CAD model does not look well when rendered.

{{<image src="img22.jpg" alt="final-full" size="45%" >}}
{{<image src="img23.jpg" alt="final-top" size="45%" >}}

{{<image src="img24.jpg" alt="final-front wheel detail" size="45%" >}}
{{<image src="img25.jpg" alt="final-all wheels detail" size="45%" >}}


