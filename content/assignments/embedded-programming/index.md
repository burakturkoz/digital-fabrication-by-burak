+++
title = "Embedded Programming"
week_order = 6
+++

## XIAO Board

### Soldering Pins to the Board

The first thing I had to do was to solder pins to thw XAIO board. After soldering, I found out that one pin was not soldered properly. I went back and fixed the solder. 

{{<image src="is1.jpg" alt="solder-setup" size="33%" >}}
{{<image src="is3.jpg" alt="solder error" size="33%" >}}{{<image src="is2.jpg" alt="final solders" size="33%" >}}

### Programming the Board

#### Adding the Board to Arduino IDE
 
In order to be able to program the XIAO board through Arduino IDE, we have to add it manually. We can do this by following the instructions in the [Seeed Studio Wiki](https://wiki.seeedstudio.com/XIAO-RP2040-with-Arduino/).

After we set it up, we must select it from the menu below:

{{<image src="ss2.jpg" alt="menu" size="70%" >}}

#### Blink Test [TUTORIAL]

The first example we did was the blink test. However, there was an issue connecting the XAIO board, Arduino IDE was not registering it as connected. 

To fix this we have to "check" the correct port from `Tools > Port > COM6`. It was COM6 for my case, but it can change.

After this, I conducted the blink test with the code below.

```go
void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);  
  delay(1000);                     
  digitalWrite(LED_BUILTIN, LOW);  
  delay(3000);                      
}
```

#### Blinking the Green LED

Then, I tried to blink only the green LED, instead of the default LED.

```go
int greenLed = PIN_LED_G;

void setup() {
  pinMode(greenLed, OUTPUT);
}

void loop() {
  digitalWrite(greenLed, HIGH);  
  delay(1000);                     
  digitalWrite(greenLed, LOW);  
  delay(3000);                      
}
```

{{<video src="vs1.mp4">}}Green LED blink test{{</video>}}

#### Using All the RGB Leds

I tried to use functions to make the LED's blink in a specific order. I created a "blink" function.

```go
int ledG = PIN_LED_G;
int ledR = PIN_LED_R;
int ledB = PIN_LED_B;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledG, OUTPUT);
  pinMode(ledR, OUTPUT);
  pinMode(ledB, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  blink(ledG);
  blink(ledR);
  blink(ledG);
  blink(ledB);
}

void blink(int led){
  digitalWrite(led, LOW);
  delay(1000);
  digitalWrite(led, HIGH);
  delay(1000);
}
```

{{<video src="vs2.mp4">}}Blinking the LED's in order{{</video>}}

After this, I experimented with using "for loops" to create a flashing effect.

```go
int ledG = PIN_LED_G;
int ledR = PIN_LED_R;
int ledB = PIN_LED_B;
int dur=20;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledG, OUTPUT);
  pinMode(ledR, OUTPUT);
  pinMode(ledB, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  blink(ledG);
  blink(ledR);
  flash(ledB);
}

void flash(int led){
  for (int i=0; i<dur; i++){
    digitalWrite(led, LOW);
    delay(100);
    digitalWrite(led, HIGH);
    delay(100);
  }
}

void blink(int led){
  digitalWrite(led, LOW);
  delay(1000);
  digitalWrite(led, HIGH);
  delay(1000);
}
```

{{<video src="vs3.mp4">}}Using "for loop"s to create flashing{{</video>}}


#### Using the Neopixel LED- Kris' Example [TUTORIAL]

To use the Neopixel LED built into the XIAO board, we first have to load it's library. We just search for "neopixel adafruit" in Arduino IDE, and install the one made by Adafruit.

In this example, we also learned how to detect a button press. We basically designate a pin for the button, and watch if it is OFF or ON.

In XIAO boards, in contrast to Arduino's, "HIGH" or "1" means there is no current. "LOW" or "0" means there is current.

Here is Kris' working example code for the changing the color of the Neopixel LED to a random RGB value, when detecting a button press.

```go
#include <Adafruit_NeoPixel.h>

int powPin = NEOPIXEL_POWER;
int neoPin = PIN_NEOPIXEL;
int btnPin = D10;

#define NUM_PIXELS 1

Adafruit_NeoPixel pix(NUM_PIXELS, neoPin, NEO_GRB + NEO_KHZ800);

void setup() {
  pix.begin();
  pinMode(powPin, OUTPUT);
  pinMode(btnPin, INPUT_PULLUP);
  digitalWrite(powPin, HIGH);
}

void loop() {
  int btnState = digitalRead(btnPin);

  if(btnState == LOW){
      int r=random(0,255);
      int g=random(0,255);
      int b=random(0,255);
      pix.clear();
      pix.setPixelColor(0, pix.Color(r,g,b)); //setting an rgb color
      pix.show();
      delay(200);
  }
}
```
{{<video src="vs4.mp4">}}Working example for button and neopixel{{</video>}}

#### Communicating Between the Board and Computer [TUTORIAL]

The TX and RX pins on the XIAO are used for communicating with other devices.

We can use the Serial connection to send and receive messages to the XIAO from our computer.

In the tutorial example below, when we write a letter to the serial monitor, it returns "received: letter". And the led blinks the amount of letters we type +1.

```go
void setup() {
  Serial.begin(19200);
  pinMode(LED_BUILTIN,OUTPUT);

}

void loop() {
  while(Serial.available()>0){
    char message= Serial.read();
    Serial.print("Received: ");
    Serial.println(message);

    digitalWrite(LED_BUILTIN, LOW);
    delay(100);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(100);
  }
}
```

{{<video src="vs5.mp4">}}Working example for serial communication{{</video>}}

### Combining What We Learned

#### Turn On Random LED After Button Press

After going through the tutorials, I wanted to combine them and play around with them.

In this example, when I press the button, a random LED will go off. Holding the button down will not do anything.

For a previous Physical Computing course, we had learned how to code a single button press. I first tested this code.

```go
int btnPin = D10;
int btnState = 1; //btnState 1 means not pressed
int prevBtnState = 1;

void setup() {
  //beginning serial communication
  Serial.begin(19200);

  //defining the button pin
  pinMode(btnPin, INPUT_PULLUP); 
}

void loop() {

  btnState = digitalRead(btnPin);

  if (btnState != prevBtnState){
    if (btnState==0){
      Serial.println("button pressed!!");
    }
  }

  prevBtnState = btnState;
  
  delay(10);
}
```

After I got it working, what I wanted to do was when I press the button, either the red, green or blue LED will blink randomly. The code below was my first attempt on testing it, but the switch function didn't work because it was not choosing between integers.

```go
    if (btnState != prevBtnState){
    if (btnState==0){
      Serial.print("button pressed!!");
      
      switch(random(1,3)){
        case 1: 
          Serial.println("1");
          break;
        case 2:
          Serial.println("2");
          break;
        case 3:
          Serial.println("2");
          break;
      }
    }
  }
```

Then, I got some help from ChatGPT on why this wasn't working. I learned that this is how I generate a random integer.

```go
int chooser = random(1,4); //generate random integer 1, 2 or 3.
      switch(chooser){
        case 1:
          Serial.println("red");
          blink(ledR);
          break;
        case 2:
          Serial.println("green");
          blink(ledG);
          break;
        case 3:
          Serial.println("blue");
          blink(ledB);
          break;
      }
```

But this had an issue as well, as one of the LED's was always turned ON in the beginning. After a while, it turns off when the randomizer chooses it. To solve this, I added the following code in `void setup()`:

```go
//turn off all LEDs in the beginning
  digitalWrite(ledR, HIGH);
  digitalWrite(ledG, HIGH);
  digitalWrite(ledB, HIGH);
```

After this, it was working as intended. Here is the final code:

```go
int ledR = PIN_LED_R;
int ledG = PIN_LED_G;
int ledB = PIN_LED_B;

int btnPin = D10;
int btnState = 1; //btnState 1 means not pressed
int prevBtnState = 1;

void setup() {
  //beginning serial communication
  Serial.begin(19200);

  //defining the LED pins
  pinMode(ledR, OUTPUT);
  pinMode(ledG, OUTPUT);  
  pinMode(ledB, OUTPUT);

  //defining the button pin
  pinMode(btnPin, INPUT_PULLUP);

  //turn off all LEDs in the beginning
  digitalWrite(ledR, HIGH);
  digitalWrite(ledG, HIGH);
  digitalWrite(ledB, HIGH);
}

void loop() {
    
  btnState = digitalRead(btnPin);
  
  if (btnState != prevBtnState){
    if (btnState==0){
      Serial.print("turning on LED: ");
      
      int chooser = random(1,4); //generate random integer 1, 2 or 3.
      switch(chooser){
        case 1:
          Serial.println("red");
          blink(ledR);
          break;
        case 2:
          Serial.println("green");
          blink(ledG);
          break;
        case 3:
          Serial.println("blue");
          blink(ledB);
          break;
      }
    }
  }

  prevBtnState = btnState;
  delay(10);
}

void blink(int led){
  digitalWrite(led,LOW);
  delay(1000);
  digitalWrite(led,HIGH);
  delay(10);
}
```

{{<video src="vs7.mp4">}}Blinking a random LED after a single button press{{</video>}}

#### Controlling Neo-Pixel by Writing

After this, I moved onto the NeoPixel. I wanted to type certain colors in the serial, and the neopixel will produce those colors.

I was trying to make the program read the things I write in the serial monitor. I tried this code first:

```go
#include <Adafruit_NeoPixel.h>

int powPin = NEOPIXEL_POWER;
int neoPin = PIN_NEOPIXEL;

#define NUM_PIXELS 1

Adafruit_NeoPixel pix(NUM_PIXELS, neoPin, NEO_GRB + NEO_KHZ800);

void setup() {
  Serial.begin(19200);
  
  pix.begin();
  pinMode(powPin, OUTPUT);
  digitalWrite(powPin, HIGH); //turning off neopixel
}

void loop() {
  while(Serial.available()>0){
  char message= Serial.read();
  if(message == "a"){
    Serial.println("yes");
  }
  }
}
```

But there was a problem with the if statement. I asked ChatGPT and apparently we need to use single quotes('a') instead of double quotes when comparing characters with a character literal. This fixed the issue.

But still, I was not able to type whole words and get the code to recognize them, I only could work with characters. I could not figure out how to use words, that is for an other time.

I changed the if statement to a switch statement for better readability. Here is the final code:

```go
#include <Adafruit_NeoPixel.h>

int powPin = NEOPIXEL_POWER;
int neoPin = PIN_NEOPIXEL;

#define NUM_PIXELS 1

Adafruit_NeoPixel pix(NUM_PIXELS, neoPin, NEO_GRB + NEO_KHZ800);

void setup() {
  Serial.begin(19200);
  
  pix.begin();
  pinMode(powPin, OUTPUT);
  digitalWrite(powPin, HIGH); //turning off neopixel
}

void loop() {
  
  while(Serial.available()>0){
    char message= Serial.read();
    switch(message){
      case 'a':
        Serial.println("purple");
        changeColor(255,0,255);
        break;
      case 'b':
        Serial.println("green");
        changeColor(0,128,0);
        break;      
      case 'c':
        Serial.println("yellow");
        changeColor(255,255,0);
        break;
      case 'd':
        Serial.println("off");
        digitalWrite(powPin, HIGH);
        delay(1000);
        break;      
    }
  }
}

void changeColor(int r, int g, int b){
  pix.clear();      
  pix.setPixelColor(0,r,g,b);
  pix.show();
  delay(200);
}
```
{{<video src="vs8.mp4">}}Typing on serial monitor to control neopixel color{{</video>}}

With this, I can change the color of the neopixel by typing "a", "b", or "c". 

I also tried to add a switch case where I can turn off power completely, but that did not seem to work. I could not find out why.