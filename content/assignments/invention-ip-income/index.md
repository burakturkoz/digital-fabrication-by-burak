+++
title = "Invention, IP, and Income"
week_order = 19
+++

## My Final Project and IP Rights

### What I Plan On Doing With My Final Project in the Future

After my final project is done, I will continue improving it along a few points.

Firstly, I will work on integrating stronger stepper motors to the design, since the ones I currently have are not enough to provide the array of movements I want from the head & neck. I will further optimize the wire connections to smooth out the movements. I have to tinker with the code as well, to achieve accelerating simultaneous movements between the motors. After I achieve what I want with two motors, I will move on to integrating new features.

A servo will be integrated to the head to control the mouth movements, for it to open & close it's beak. Another stepper will be put on the body for hip movement from the leg joints. They will be integrated into the control interface as well.

The last step would be to update the control interface. I can turn it into something more user-friendly or physical, like a joystick. Finally, I can integrate some sensors into the design to make it reactive to the environment, which would make it an exhibition piece.

For IP purposes, this design can either stay as an exhibition piece, or it can be commercialized into a DIY kit like the [animatronic raven kit](https://www.youtube.com/watch?v=y0R8-F4TmPI&list=LL&index=5).

### Comparing 3 Open Source Licences

#### CERN Open Hardware License

CERN OHL is a license that encompasses hardware and product designs and regulations regarding their studying, sharing and distribution. It aims provide a legal basis for the sharing of open-source hardware, and the creation of subsequent iterations over these shared works. 

Products of all sorts such as ventilators, cameras, satellites and even beer has been licensed under a form of CERN OHL. Electronics projects such as microcontrollers or integrated circuit designs can also be included within CERN OHL.

My project could be licensed under CERN OHL, since it is a physical product design. Hardware design in this context also covers things like PCB schematics too, for example. Therefore it would be a good fit to license my work as a whole. If it becomes a DIY kit in the future as I said, licensing it under CERN OHL could be especially helpful.

#### MIT License

MIT License is mainly used for open-source software projects. It is a relatively accessible FOSS licence, which allows users to do pretty much everything with the code as long as they include the original MIT license in all copies of the end product. 

It allows for commercializiation and any developer can use an MIT License in their works if they are familiar with the terms.

Compared to CERN OHL, MIT License does not include hardware or physical products. And it would not be very applicable to my final project, as I mainly used and modified code made by other people and the project is not software-heavy.

Although, if I wanted to, I could licence the project under several different licences. For example, the hardware could be under CERN OHL and the software could be under MIT. As long as I explicitly communicate this, it would be valid.

#### Creative Commons Attribution 4.0 International

Creative commons licenses are used for works of media such as videos, photographs, podcasts, stories, datasets, scientific publications etc. They are not recommended to be used for hardware or software. For a creative commons license to be applied to a work, it's enough to publicly declare it in a way that is comprehensible by the viewer. Once a license is applied, it cannot be revoked.

CC-BY-4.0 is a license that allows for the remix, adaptation, distribution or commercialisation of the work, as long as credit is given to the original author.

CC-BY-4.0 can also be applied to documentation. So if I wanted to apply a license to my Fab Academy documentation page (but for the documentation part only!), this could be a good option.

### My License of Choice

For this weeks assignment, I chose to license my design under *CERN Open Hardware License v2 Permissive*. 

In order to add this license to my work I followed [this guide](https://choosealicense.com/licenses/cern-ohl-p-2.0/):

> "Create a text file (typically named LICENSE or LICENSE.txt) in the root of your source code and copy the text of the license into the file."

I also added a text in my footer saying "This work is licensed under CERN Open Hardware License v2 Permissive."

```go

CERN Open Hardware Licence Version 2 - Permissive


Preamble

CERN has developed this licence to promote collaboration among hardware
designers and to provide a legal tool which supports the freedom to use,
study, modify, share and distribute hardware designs and products based on
those designs. Version 2 of the CERN Open Hardware Licence comes in three
variants: this licence, CERN-OHL-P (permissive); and two reciprocal licences:
CERN-OHL-W (weakly reciprocal) and CERN-OHL-S (strongly reciprocal).

The CERN-OHL-P is copyright CERN 2020. Anyone is welcome to use it, in
unmodified form only.

Use of this Licence does not imply any endorsement by CERN of any Licensor or
their designs nor does it imply any involvement by CERN in their development.


1 Definitions

  1.1 'Licence' means this CERN-OHL-P.

  1.2 'Source' means information such as design materials or digital code
      which can be applied to Make or test a Product or to prepare a Product
      for use, Conveyance or sale, regardless of its medium or how it is
      expressed. It may include Notices.

  1.3 'Covered Source' means Source that is explicitly made available under
      this Licence.

  1.4 'Product' means any device, component, work or physical object, whether
      in finished or intermediate form, arising from the use, application or
      processing of Covered Source.

  1.5 'Make' means to create or configure something, whether by manufacture,
      assembly, compiling, loading or applying Covered Source or another
      Product or otherwise.

  1.6 'Notice' means copyright, acknowledgement and trademark notices,
      references to the location of any Notices, modification notices
      (subsection 3.3(b)) and all notices that refer to this Licence and to
      the disclaimer of warranties that are included in the Covered Source.

  1.7 'Licensee' or 'You' means any person exercising rights under this
      Licence.

  1.8 'Licensor' means a person who creates Source or modifies Covered Source
      and subsequently Conveys the resulting Covered Source under the terms
      and conditions of this Licence. A person may be a Licensee and a
      Licensor at the same time.

  1.9 'Convey' means to communicate to the public or distribute.


2 Applicability

  2.1 This Licence governs the use, copying, modification, Conveying of
      Covered Source and Products, and the Making of Products. By exercising
      any right granted under this Licence, You irrevocably accept these terms
      and conditions.

  2.2 This Licence is granted by the Licensor directly to You, and shall apply
      worldwide and without limitation in time.

  2.3 You shall not attempt to restrict by contract or otherwise the rights
      granted under this Licence to other Licensees.

  2.4 This Licence is not intended to restrict fair use, fair dealing, or any
      other similar right.


3 Copying, Modifying and Conveying Covered Source

  3.1 You may copy and Convey verbatim copies of Covered Source, in any
      medium, provided You retain all Notices.

  3.2 You may modify Covered Source, other than Notices.

      You may only delete Notices if they are no longer applicable to the
      corresponding Covered Source as modified by You and You may add
      additional Notices applicable to Your modifications.

  3.3 You may Convey modified Covered Source (with the effect that You shall
      also become a Licensor) provided that You:

       a) retain Notices as required in subsection 3.2; and

       b) add a Notice to the modified Covered Source stating that You have
          modified it, with the date and brief description of how You have
          modified it.

  3.4 You may Convey Covered Source or modified Covered Source under licence
      terms which differ from the terms of this Licence provided that You:

       a) comply at all times with subsection 3.3; and

       b) provide a copy of this Licence to anyone to whom You Convey Covered
          Source or modified Covered Source.


4 Making and Conveying Products

You may Make Products, and/or Convey them, provided that You ensure that the
recipient of the Product has access to any Notices applicable to the Product.


5 DISCLAIMER AND LIABILITY

  5.1 DISCLAIMER OF WARRANTY -- The Covered Source and any Products are
      provided 'as is' and any express or implied warranties, including, but
      not limited to, implied warranties of merchantability, of satisfactory
      quality, non-infringement of third party rights, and fitness for a
      particular purpose or use are disclaimed in respect of any Source or
      Product to the maximum extent permitted by law. The Licensor makes no
      representation that any Source or Product does not or will not infringe
      any patent, copyright, trade secret or other proprietary right. The
      entire risk as to the use, quality, and performance of any Source or
      Product shall be with You and not the Licensor. This disclaimer of
      warranty is an essential part of this Licence and a condition for the
      grant of any rights granted under this Licence.

  5.2 EXCLUSION AND LIMITATION OF LIABILITY -- The Licensor shall, to the
      maximum extent permitted by law, have no liability for direct, indirect,
      special, incidental, consequential, exemplary, punitive or other damages
      of any character including, without limitation, procurement of
      substitute goods or services, loss of use, data or profits, or business
      interruption, however caused and on any theory of contract, warranty,
      tort (including negligence), product liability or otherwise, arising in
      any way in relation to the Covered Source, modified Covered Source
      and/or the Making or Conveyance of a Product, even if advised of the
      possibility of such damages, and You shall hold the Licensor(s) free and
      harmless from any liability, costs, damages, fees and expenses,
      including claims by third parties, in relation to such use.


6 Patents

  6.1 Subject to the terms and conditions of this Licence, each Licensor
      hereby grants to You a perpetual, worldwide, non-exclusive, no-charge,
      royalty-free, irrevocable (except as stated in this section 6, or where
      terminated by the Licensor for cause) patent licence to Make, have Made,
      use, offer to sell, sell, import, and otherwise transfer the Covered
      Source and Products, where such licence applies only to those patent
      claims licensable by such Licensor that are necessarily infringed by
      exercising rights under the Covered Source as Conveyed by that Licensor.

  6.2 If You institute patent litigation against any entity (including a
      cross-claim or counterclaim in a lawsuit) alleging that the Covered
      Source or a Product constitutes direct or contributory patent
      infringement, or You seek any declaration that a patent licensed to You
      under this Licence is invalid or unenforceable then any rights granted
      to You under this Licence shall terminate as of the date such process is
      initiated.


7 General

  7.1 If any provisions of this Licence are or subsequently become invalid or
      unenforceable for any reason, the remaining provisions shall remain
      effective.

  7.2 You shall not use any of the name (including acronyms and
      abbreviations), image, or logo by which the Licensor or CERN is known,
      except where needed to comply with section 3, or where the use is
      otherwise allowed by law. Any such permitted use shall be factual and
      shall not be made so as to suggest any kind of endorsement or
      implication of involvement by the Licensor or its personnel.

  7.3 CERN may publish updated versions and variants of this Licence which it
      considers to be in the spirit of this version, but may differ in detail
      to address new problems or concerns. New versions will be published with
      a unique version number and a variant identifier specifying the variant.
      If the Licensor has specified that a given variant applies to the
      Covered Source without specifying a version, You may treat that Covered
      Source as being released under any version of the CERN-OHL with that
      variant. If no variant is specified, the Covered Source shall be treated
      as being released under CERN-OHL-S. The Licensor may also specify that
      the Covered Source is subject to a specific version of the CERN-OHL or
      any later version in which case You may apply this or any later version
      of CERN-OHL with the same variant identifier published by CERN.

  7.4 This Licence shall not be enforceable except by a Licensor acting as
      such, and third party beneficiary rights are specifically excluded.

```
