+++
title = "Machine Building"
week_order = 12
+++

## Building the "Prusa Mini+"

### Group Project Week

This week, we built and tested the Prusa Mini+ 3D printer kit as a group.

The machine was built and tested over the course of two days. I was not able to participate in the building day, but I was there in the testing day. Together, we completed the machines first time set up and tested it by running it through a few prints. I wrote the documentation page for the testing day.

[The group documentation page can be found here](https://aaltofablab.gitlab.io/machine-building-2023-group-b/).
