+++
title = "Project Management"
week_order = 3
+++

## Starting Documentation and Media Optimization

### Imagemagick and Image Optimization

#### Basics of Imagemagick

Imagemagick is a tool that allows us to optimize media before uploading to our website. I downloaded it from [here](https://imagemagick.org/script/download.php).

After downloading and installing, we need to make it functional by typing a few commands. 

At this point, the usual "which magick" command did not work for me. Here is what we need to do to fix this:
> Find where you installed imagemagick (e.g. C:/Program Files/..../).
>
> In GitBASH, locate to this directory with cd.
>
> Type "nano .bashrc". This will create a new file and open it. 
>
> ".bashrc is a configuration file for shell and we create it to instruct the shell to do something for us when we open GitBASH."(Kris, 01.02.23)
>
> You will see a text editor interface. Enter the following code: 
>
> > export PATH="$PATH:/C/Program Files/...../
>
> Press ctrl+x.
>
> Nano is going to ask to confirm, type y and hit enter.
>
> Restart GitBASH.
>
> Now, typing "which magick" will show us the location of it.

Now that imagemagick is functional, we can start using it. We can type `magick -h` to see the list of commands.

We can do a number of things, like converting, resizing, identifying etc.

> `magick identify demoimage.jpg`
>
> `magick convert demoimage.jpg -resize 800x500 smallimage.jpg`
>
> `magick convert demoimage.jpg -resize 800x500 -quality 50 smallimage2.jpg` where quality is in percentages.

#### Imagemagick Mogrify

In order to put the screenshots I took to the website, I had to resize and convert them in bulk.

To do this, I used imagemagick mogrify tool.

First, we need to copy the folder the images are in, and rename it. Since the mogrify tool works on the original images and changes them, we wouldn't want to lose those changes.

Then I executed the following command to optimize and convert all of the images in the folder in bulk.

> `magick mogrify -quality 60 -format jpg *.png`

If you want to optimize a single file, you can do so like this:

> `magick convert sketch.jpg -resize 50% -quality 70 sketchnewname.jpg`
>
> or if you want to convert it to another format as well:
>
> `magick convert img5.png -resize 50% -quality 60 img.jpg`
>
For me, after some experimentation, I decided to use "quality 60", jpg format and no resizing, on screenshots.

And for pictures taken with phone camera:
>
> `magick convert test.jpg -resize 40% -quality 40% test8.jpg`
>

### FFMPEG Video Compress

For compressing videos, I used FFMPEG. Here is the code I used for videos shot on my phone:

`ffmpeg -i v1.mp4 -c:v libx264 -preset slow -crf 40 -an -vf scale=720:-2 output.mp4`

Here, -an means that there is no audio.

### Image Shortcodes

To control the size and other attributes of individual images, we can use shortcodes. Create a folder called "shortcodes" under "layouts". In here, create "image.html".

The following shortcode allows us to control size for example:

> `<img style="width:{{ .Get `size` }};height:auto;" src="{{ .Get `src` }}" alt="{{ .Get `alt` }}">`
>
> Pay attention to the backticks!! (``)

Then, in anywhere in the website we can use the following shortcode to add images:
> `{ {<image src="imagename.jpg" alt="2d sketch" size="25%" >} }` (but with no space between {{ ) (I couldn't do this because it was being interpreted as real code.)

### Video Shortcodes

Here is the video shortcode used:

```go
<video controls width="500">
  <source src="{{ .Get `src` }}" type="video/mp4">
</video>
<p>{{ .Inner }}</p>
```

And here is what we write in the markdown doc to add a video:

```go
{{<video src="vs1.mp4">}}LED Blink Test{{</video>}}
```

### CSS

In order to add CSS, we first create a folder on the main local directory called "static". Then we create "static/assets/css" and under that, the file "style.css". 

For an example CSS content, we will use:

> ```go
> body{
>  background: yellowgreen
> }
> ```

To refer to this file, we should edit the "head" partial. We add the following line:
> `<link rel="stylesheet" href="{{ .Site.BaseURL }}assets/css/style.css">`

Now, the website will start applying the changes we make to the CSS.






