+++
title = "Molding and Casting"
week_order = 14
+++

## Creating a Simple Mold

### Designing the Piece

For this week, I wanted to design a simple piece to cast since it was my first time doing so. I sketched out the piece and molds, and made sure that it would actually come out of a mold.

{{<image src="i1.0.jpg" alt="sketches" size="35%" >}}
{{<image src="s1.jpg" alt="piece" size="48%" >}}

I proceeded to 3D model the mold and master mold for the piece. The piece on the left is the mold, and the right is the master-mold. A master-mold is the mold of the mold. I put section views below so how the pieces fit inside each other can be better understood (Pink: piece, Yellow: mold, Blue: master-mold).

{{<image src="s5.jpg" alt="mold" size="48%" >}}
{{<image src="s4.jpg" alt="master mold" size="48%" >}}

{{<image src="s2.jpg" alt="section-mold" size="48%" >}}
{{<image src="s3.jpg" alt="section-mastermold" size="48%" >}}

### Toolpaths in Fusion 360

I moved on the configuring the tool paths. In the CNC cutting week, I ended up creating the toolpaths in VCarve to save time and not take risks with the machine. But this time, I wanted to do the whole workflow in Fusion and upload it directly to the CNC.

#### Configuring Tool Data

First, we configure the stock and tool data. While choosing the stock, make sure that the origin is like the image down below.

{{<image src="s6.jpg" alt="origin" size="48%" >}}

Then I measured the tool I was going to use, and added it to the Fusion library.

{{<image src="s7.jpg" alt="tool data" size="48%" >}}
{{<image src="s8.jpg" alt="tool data" size="48%" >}}

#### Creating Toolpaths

I moved on to creating the toolpaths. The most vital settings I used were these:

- Maximum Stepdown: 1mm (This prevents the tool from going in too much. I was using a 6mm tool with 1mm stepdown)
- Conventional milling (I changed the toolpath settings so it would do conventional milling)
- Top Height Offset: 2mm (When configuring the toolpath heights, I put 2mm offset to the top so the tool always cuts air first) 

The strategy I followed was a four-step one. First, we make sure that the face of the stock is completely straight. Then, we gradually cut through the middle parts, making sure to not go too deep in one go. Finally, we cut the hole in the middle.

{{<image src="s11.jpg" alt="toolpaths" size="48%" >}}
{{<image src="s12.jpg" alt="toolpaths" size="48%" >}}

{{<image src="s13.jpg" alt="toolpaths" size="48%" >}}
{{<image src="s14.jpg" alt="toolpaths" size="48%" >}}

When exporting these toolpaths, I made sure to choose a Roland generic post-processor.

### Milling the Master-Mold

#### Trial Piece with Foam

After my toolpaths were ready and exported, I moved on to milling. First, I tried the toolpaths with foam to see if everything was correct.

{{<image src="i1.jpg" alt="foam trial" size="48%" >}}

{{<video src="output1.mp4">}}Milling Trial with Foam{{</video>}}

{{<image src="i2.jpg" alt="foam trial" size="48%" >}}

#### Master-Mold 

After the foam was successfull, I started to set up the wax stock.  To set the Z-height, I put a piece of paper under the tool and gradually lowered it. I zero'd the Z-height when the friction started to not allow me to move the paper.

{{<video src="output2.mp4">}}Setting Z height{{</video>}}

Before cutting the wax, I made sure to start the cutting speed from 10%. This way, we prevent any errors resulting from too much cutting speeds. As the process goes on, we gradually increase the cutting speed. For me, I could go up the 80% at the end.

{{<image src="i3.jpg" alt="settings" size="48%" >}}

{{<video src="output3.mp4">}}Milling the Master-Mold{{</video>}}
{{<image src="i4.jpg" alt="master mold" size="48%" >}}

### Casting

#### Casting the Mold

After my mold was milled, I started casting. Firstly, we have to cast the mold. For this we use the MoldStar 15 Slow. Read the instructions first always. 

MoldStar 15 Slow requires a 1A:1B by weight mixing ratio. I first measured the weight of the cup I was going to use. Then, I filled the master-mold with water and poured it into the cup, measuring how much material I would need to fill it up. 

I mixed Part A and Part B using different mixing sticks in their own container. Then I poured and mixed them together according to the weights I had just calculated. I degassed this mixture, and poured it into the master-mold. 

{{<image src="i5.jpg" alt="measuring" size="48%" >}}
{{<image src="i6.jpg" alt="materials" size="48%" >}}

The next day, my mold was complete.

{{<image src="i7.jpg" alt="casting" size="48%" >}}
{{<image src="i8.jpg" alt="final mold piece" size="48%" >}}

#### Casting the Piece

The final step is to cast the piece itself. For this, I used Smooth-cast 305. I followed the same steps as above for the measuring of the weight and mixing Part A and Part B. Only difference being, Smooth-cast 305 required a 100A:90B mix ratio by weight, so I had to do some calculations. I poured the mixture and waited 40 mins.

{{<image src="i10.jpg" alt="materials" size="48%" >}}
{{<image src="i11.jpg" alt="casted piece" size="48%" >}}

The final piece can be seen here. Although the dimensional accuracy is fine, the top of the mold did not come out as I wanted. I learned that I can overpour a bit more, so the shrinkage doesn't affect the surface as much.

Also, there were some air holes near the top. The instructions said not to degass, but maybe a bit of degassing could help. Or I did not pour the mixture from a high enough point.

{{<image src="i12.jpg" alt="final piece" size="48%" >}}
{{<image src="i13.jpg" alt="final piece" size="48%" >}}

{{<image src="i14.jpg" alt="final piece" size="48%" >}}
{{<image src="i16.jpg" alt="final piece" size="48%" >}}