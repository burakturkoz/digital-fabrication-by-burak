+++
title = "Web Basics"
week_order = 1
+++

## Building an HTML Website or: How I Learned to Stop Worrying and Love the Code

### Basics of HTML

#### The Boilerplate Code

First of all, we looked into just creating a plain HTML document. To do this, we need _boilerplate code_, which is the bare bones structure.

HTML elements open and close with tags, such as `<head>` and `</head>`. We use these to create certain sections on our website.

> `<head>`: Contains the necessary code for the website to run, but it is invisible. Like character set or display width.
>
> `<body>`: Contains the contents of the website that we are able to see. Menus, paragraphs, media are all under `<body>`.

Other than these, we have the `<title>`- which is the title displayed on the browser- language, and the necessary `<html>` element.

Here is what a boilerplate code looks like:

```go
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Web Page</title>
  </head>
  <body>
    <!-- Add your HTML content here -->
  </body>
</html>
```

Within the body, we can add some additional elements:

>`<header></header>`: Located on the top of the page, can contain the main page title or the navigation bar.
>
>`<nav></nav>`: We use this to specify that the elements within this tag constitute the navigation bar.
>
>`<ul></ul>` and `<ol></ol>`: Unordered and ordered list elements, respectively. Each element under these needs to be contained within `<li></li>` to be a list.
>
>`<table></table>`: We use this to create a table. `<tr></tr>` elements are used to indicate rows, while `<td></td>` are used to indicate columns.
>
>`<section></section>`: Can be used to divide the page into certain sections.
>
>`<footer></footer>`: Located on the bottom of the page. Can contain a copyright symbol for example.

In order to have titles and paragraphs in our page, we use the following:

>`<h1></h1>`: We put the title between these. These should always be in a specific order, from `<h1>` to `<h6>`: the former always being the main title of the page. Do not skip any of them, always write them in order.
>
>`<p></p>`: We put text between these to indicate that it is a paragraph. 

The first draft of my homepage is below. I used a table as a navigation bar instead of a list.

```go
<!DOCTYPE html>
<html>
  <head>
    <title>DF Documentation- Burak Türköz</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device width">
  </head>

<body>
<header>
<h1>Digital Fabrication Documentation by Burak Türköz</h1>
</header>

<nav>
  <table width = "20%">
    <tr>
      <td><a href="index.html">Home</a></td>
      <td><a href="aboutMe.html">About Me</a></td>
      <td><a href="finalProject.html">Final Project</a></td>
    </tr>
  </table>
</nav>

<main>
  <p>Welcome to my documentation website.</p>
  <p>This is some content text.</p>
</main>

<footer>
  &copy; Burak Türköz 2023
</footer>

</body>
</html>
```

#### Adding More Pages

We  can add more pages by creating HTML files under the same folder and linking to them. The seperate pages need to have the same head information. Also they need to have the same navigation table if we want to navigate between them.

The code for linking to another HTML page is: `<a href="pagename.html">Page Name</a>`

For example, here is my "About Me" page:

```go
<!DOCTYPE html>
<html>
  <head>
    <title>DF Documentation- About Me</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device width">
  </head>

<body>
<header>
<h1>Digital Fabrication Documentation by Burak Türköz</h1>
</header>

<nav>
  <table width = "20%">
    <tr>
      <td><a href="index.html">Home</a></td>
      <td><a href="aboutMe.html">About Me</a></td>
      <td><a href="finalProject.html">Final Project</a></td>
    </tr>
  </table>
</nav>

<main>
  <h2>About Me</h2>
  <table width="25%">
    <tr>
      <td>
        <img src="pp.png" width="100%">
      </td>
    </tr>
    <tr>
      <td>
      <p>I am Burak Türköz, I am a New Media student with a background in industrial design.</p>
    </td>
    </tr>
  </table>
</main>

<footer>
  &copy; Burak Türköz 2023
</footer>
  
</body>
</html>
```

#### Adding Images

To add media, we first need to copy it to our website folder. Then, we can refer to it by name inside our code.

>To add images: `<img src="filename.jpg">`
> > Control it's width: `<img src="filename.jpg" width="100%">`
> >
> > Add a description: `<img src="filename.jpg alt="desciption">`
>
>To add videos: 
> > ```go
> > <video controls width="250">
> >   <source src="videoname.mp4" type="video/mp4>
> > </video>
> > ```
>
>We can also autoplay videos by:
> > ```go
> > <video controls autoplay width="250">
> >   <source src="videoname.mp4" type="video/mp4>
> > </video>
> > ```

We have built the basic boilerplate HTML page. Now, we need to start using Git in order to publish it.

#### Why Is Git?

Git is a tool for _version control_. It gives us the ability to access the previous versions of a website. This way, if we make a mistake that breaks the whole thing, we can always revert to a previous version. Amazing.

For Windows, I used **Git for Windows**. You can download it from [here](https://gitforwindows.org/)

This tool also comes with **GitBASH**. It is a terminal that allows us to give Git commands through typing.

Within GitBASH, we first navigate to the website folder by `cd [directory URL]`. By typing `git init`, we initalize an empty Git repository in the selected folder. 

We can change our global username and e-mail within Git as well. These are the credentials we use to sign our changes.

>`git config --global user.name "Burak Turkoz"`: change username
>
>`git config --global user.email burak.turkoz@aalto.fi`: change e-mail.

After initalizing empty Git repo and deciding our username, we can continue making changes in our HTML code. We can use `git status` to learn about which documents we have changed.

In order to add these changes we have made, we use `git add`. Going from Kris' bookshelf analogy, we can imagine the folder as a room with books on the floor. We can make all the changes to the books we want when they are on the floow. When we use `git add`, we take the changed books in our hands. This is a temporary space.

In order to put the books in the bookshelf -which is our repository- we use `git commit -m "our message here"`. By using the message, we put a label on the book and put it on the shelf.

Here are the collection of the commands we used during the process:

>`cd`: Navigation to the desired folder.
>
>`ls`: Lists the files inside the folder.
>
>`git init`: Initialize empty Git repository.
>
>`git status`: Learn about which documents were changed.
>
>`git diff`: See more details on the changes we made.
>
>`git add`: Apply the changes we made. This is only changes them in a "temporary space".
>
>`git commit -m "message"`: Commit the changes we made, get them out of the temporary space. We can also add a message to label our change.
>
>`git log`: See the history of changes with the labels and who did them.

We can go back and continue making changes to our files. We need to repeat the same process again to commit those changes.


#### Creating the SSH Key

In order to publish our website, we need an SSH key. We can generate this through GitBASH by `ssh-keygen -t ed25519 -C "Key Name"`. Then the program asks where we want to save the key, we can enter the directory we want here. We can leave the passphrases blank for now.

If we receive the alert `Your identification has been saved in 'directory URL'`, we have succesfully generated an SSH key. By typing `ls` we can see the key files have been created.

The command `cat id_ed25519.pub` gives us the SSH key for us to copy. "id_ed25519" here is only the name of the file we create for the key. This can be changed in the previous step while we are typing the directory URL. In this case, we need to write that changed version instead of "id_ed25519" in the cat command.

We copy the SSH key now, and continue to GitLab.


#### When GitLab Comes In

In order to transfer the repository over to GitLab, we need to create an account first.

Then, we have to copy the SSH key we just generated to our GitLab account. "Settings > SSH Keys" takes us to the page where we add our keys. We give the key a title, no expiration date and add key.

After copying the public key to GitLab, we need to remove it from the local repository which we are going to push. If we leave it there, it may create a potential danger to our website. We can do it with the command `rm -id_ed25519.pub` and `rm -id_ed25519`, where "id_ed25519" is the name of the key.

Going back to GitLab, we now have to create a blank project. Name it, and set it to public view. When it is created, GitLab gives you instructions on how to populate the repository.

Since we already initated Git in our local repository, we only need to push it. So we go back to GitBASH to type some commands. It is a good idea to check `git status` before pushing, to see if there are any uncommitted changes.

Then we just need to follow GitLab's instructions. We first add an origin by `git remote add origin [URL]`. (The URL is provided by the GitLab repository).

We can use `remote -v` to see the names of the origins.

Then we use `git push -u origin --all`.

If this succeeds, you will know by the text "Branch set up to track remote branch from origin". We can also check if it succeeded by refreshing the GitLab repo.

If this doesn't succeed and it says "make sure you have access rights", try to change to URL in the command `remote add origin` into the https one.

#### Create README file

Now, we create a README.md file for our website. We can use the command `touch README.md` to create a new file. 

".md" means Markdown, which is a markup language used to format documents. Here is a [markdown guide](https://about.gitlab.com/handbook/markdown-guide/).

After creating the file we can edit it with Visual Studio Code. Here is an example README.md:

```go
# Fab Academy

This is the demo Digital Fabrication documentation website. This is my second attempt after starting over.

## Usage

Clone repository and edit it according to your needs.

## More

You can learn more about Fab Academy on [this website](https://fabacademy.org).
```
After creating this file, we follow the standart procedure: `git status` > `git add` > `git commit -m "message"` > `git push`

#### Publishing the Website

In order to publish the website, we go back to the GitLab interface. Click the plus icon at the top of the repo, and choose New File.

Write `.gitlab-ci.yml` as the file name. Apply template for HTML. We can write a commit message and commit changes. 

To check if our website is running, we can check the pipeline icon that should appear on the top of the repository.

The link to our website can be found under Pages.