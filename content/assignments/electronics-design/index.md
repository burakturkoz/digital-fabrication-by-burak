+++
title = "Electronics Design"
week_order = 8
+++

## Electronics Design Using KiCAD 

### How to Set Up KiCAD?

#### Download KiCAD

To download KiCAD, we go to their [website](https://www.kicad.org/download/) and download the latest installment.

#### Clone and Install Fab Repository

After downloading KiCAD, we need to install the Fab library, so we can work with the specific parts that we have available in the lab. To do this, we go to the [Fab Electronics Library for KiCAD repository](https://gitlab.fabcloud.org/pub/libraries/electronics/kicad). We can clone this repository with Git, so any updates done can easily be adapted to.

To add this library to KiCAD, we can follow the instructions outlined in the repository:

> 1. Clone or download this repository. You may rename the directory to fab.
>
> 2. Store it in a safe place such as ~/kicad/libraries or C:/kicad/libraries.
>
> 3. Run KiCad or open a KiCad .pro file.
>
> 4. Go to "Preferences / Manage Symbol Libraries" and add fab.kicad_sym as symbol library.
>
> 5. Go to "Preferences / Manage Footprint Libraries" and add fab.pretty as footprint library.

### Designing in the Shematic Editor

#### Adding 

After we install our library, we can start designing our part. We create a new project and make sure to check "create a new folder for the project."

The `place symbol` command allows us to place electronic parts into our designs. We make sure to choose our parts from the Fab Library. First, we add the XIAO board.

{{<image src="i1.jpg" alt="adding xiao board part" size="65%" >}}

Using the `add power symbol` command, we should add a power input and ground to the XIAO board. At this point, we should also pay attention to the type of grid we are using. For now, we will stick with the default one (1.27mm).

{{<image src="i2.jpg" alt="adding power symbols" size="48%" >}}
{{<image src="i3.jpg" alt="adding power symbols" size="48%" >}}

Then, we add the LED and resistor pieces from the same Fab Library

{{<image src="i4.jpg" alt="adding LED" size="48%" >}}
{{<image src="i5.jpg" alt="adding resistor" size="48%" >}}

We can connect these parts together with the XIAO board using wires. We can also put custom names to parts by right clicking and `edit main fields/ edit value`.

{{<image src="i6.jpg" alt="connected circuit" size="65%" >}}

#### Calculating the Resistance

We need to calculate the value that we give to the resistor. For this, we need to know the specifics of the LED that we are going to use. We can check the LED's data sheet through the right click menu. But at this case, it was just a generic LED datasheet, so we checked the [Fab database](http://inventory.fabcloud.io/?purpose=Electronics%20Design). Through there, we found the details in the [product page](https://www.digikey.com/en/products/detail/lumex-opto-components-inc/SML-LX1206IC-TR/229140 ).

We learned that the recommended forward current in 30 mA. The information that we need is the "forward current" and "forward voltage".

{{<image src="i7.jpg" alt="forward current" size="48%" >}}
{{<image src="i8.jpg" alt="forward voltage" size="48%" >}}

Then, we start our calculations. In KiCAD, we can add text through the `add text` command on the right menu. What we need to find is the required resistance. For this, we use the formula "V=I*R", where V is voltage (volts), I is current (amps), and R is resistance (ohm). We need to convert the mA values to A for this to work.

We find 43 ohm as the required resitance. We dont have exactly 43 ohms in the inventory but we can use 49.9 instead, as the closest one.

{{<image src="i9.jpg" alt="calculations" size="48%" >}}
{{<image src="i10.jpg" alt="calculations" size="48%" >}}

#### Adding Buttons and Modularizing

Next, we added a button. We use the same process for adding a part. But this time, we will not connect it straight to the board in the schematic, but modularize it. For this we can use the `net label` command on the menu. We put the button cable somewhere else and label it. This is how we can modularize schematics.

{{<image src="i11.jpg" alt="button" size="48%" >}}

#### No Connection Flags

If there are pins that are not connected, we add "no connection" flags to them, so the electronics rules checker does not see them as errors. These are displayed as small blue crosses.

#### Adding PWR_FLAGS

We can run the electronics rule check now, but it fails. Because there is no power input to the circuit. For this, we need to add power flags to the 3.3V output and ground. We add it though the `add power symbols`. After this, the rule check is succesfull.

{{<image src="i12.jpg" alt="rule check fail" size="48%" >}}
{{<image src="i13.jpg" alt="power flags" size="48%" >}}

#### Adding NeoPixel 

After completing the tutorial, I decided to try adding the neopixel component. After adding it through the Fab Library, I checked out it's [datasheet](https://www.adafruit.com/product/1938).

{{<image src="i14.jpg" alt="neopixel pinout" size="48%" >}}
{{<image src="i15.jpg" alt="neopixel pinout" size="48%" >}}

I assumed I should hook it up to one of the analog pins, because I want to send a range of data, not only on or off. So I chose D3 as it's data pin. Since it requires 5V, I hooked it up to the 5V power pin. I ran electrical rules check but got two errors.

{{<image src="i16.jpg" alt="neopixel connection" size="48%" >}}
{{<image src="i17.jpg" alt="neopixel error" size="48%" >}}

I deleted the pwr flag connected to neopixel ground, one of the errors went away. The other error said DOUT pin not connected. According to the schematic, it doesn't have to be connected, so I added a "no connection" flag, which solved the problem.

{{<image src="i18.jpg" alt="neopixel error" size="48%" >}}

But when I tried to update it in the PCB board editor, it said no footprint. So I went back to the `place symbol` menu and relized I had not selected a footprint. The error got solved when I added this.

{{<image src="i19.jpg" alt="neopixel connection" size="48%" >}}
{{<image src="i20.jpg" alt="neopixel error" size="48%" >}}

#### Adding Potentiometer

My idea was to make the neopixel be controlled by a potentiometer and map it to the RGB values. So when I turn the knob, I can move through the RGB spectrum. After looking at the data sheet, I connected one end to 5v, one end to GROUND and one end to an analog pin.

{{<image src="i21.jpg" alt="potentiometer" size="65%" >}}


### Using the PCB Board Editor

#### Update PCB from Schematic

The next step is to use the "PCB Board Editor" workspace. After opening the file, we should execute the "update PCB from schematic" command. If there are no errors, our schematic is imported.

We need to flip the board, since we are making a shield for the XIAO, and it makes sense to place the components to the other side of the board. We select board, press f. The pins turns blue. In this context, F means front and B means back.

{{<image src="i22.jpg" alt="pcb board editor" size="65%" >}}

#### Placing Components and Drawing Edges

In the PCB board editor, there are many layers we can use to draw different parts of the design. For the actual shape of the board, we use "edge cut". Edge cut allows us to make any shape we want out of the board. 

The faint blue lines between, called a rats nest, shows connections between the components. We can move the components around, rotate them with "R" and place them wherever we like. Don't forget to switch to "1mm" grid when doing this.

{{<image src="i23.jpg" alt="pcb board editor" size="65%" >}}

#### Editing Net Classes and Adding Connections

Before adding connections between our placed parts, we need to edit pre-defined sizes. These are limiting values that depend on our manufacturing method. We wrote the values according to CNC milling, which is the method that we are going to use. 

We go to `edit pre-defined sizes` thorugh the upper menu and select `net classes`. We changed the first three values: "clearance", "track width" and "via size" to the values below.

{{<image src="i24.jpg" alt="edit net classes" size="65%" >}}

Now we can start adding connections. We can change the grid to 0.1mm to allow for more flexibility. We then switch to "Front Copper Layer" to add the connections.

We can use the connect tool and connect the components manually. We can go a certain point an hit F to connect the rest auromativally. Or if we select a pad and click shit+F that completes the whole route automatically.

It is very important to edit net classes before we start drawing, because it will not update by itself.

At this point, I experimented with different connection configurations. Since it was my first time doing this, it was very hard to actually find a connection setup that did not interrupt each other's paths. It was like a game of escaping a maze.

{{<image src="i26.jpg" alt="experimenting connections" size="65%" >}}

{{<image src="i27.jpg" alt="experimenting connections" size="65%" >}}

Most of the configurations I tried did not work at all. Finally, I found a placement that allowed me to connect everything together.

{{<image src="i28.jpg" alt="working connections" size="65%" >}}

The final step is to run "design rules checker". However, I had an issue here: The footprint of the neopixed LED doesn not seem to be in accordance with our milling rules. 

{{<image src="i29.jpg" alt="design rules issue" size="65%" >}}

### Questions and Issues

> 1. Why does the footprint cause issues in the design rules checker? Should I have used a different one?
>
> 2. What part is the "header connector"? There are many of them in the Fab Library, which of those should I have connected?
>
> 3. Is the potentiometer and neopixel LED connected correctly?
