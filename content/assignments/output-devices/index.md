+++
title = "Output Devices"
week_order = 11
+++

## Stepper Motor

### Trials on a Breadboard

#### Wiring

This week, I decided to try to set up a stepper motor with the Xiao and make a circuit board. I had never worked with a stepper motor before, so I wanted to try everything on a breadboard first. To do this, I followed this [tutorial](https://www.youtube.com/watch?v=5CmjB4WF5XA&t=502s).

The components I was using were as follows:
- ST-PM35-15-11C Stepper Motor
- TMC2208 Driver Chip for Stepper Motors ([product page](https://wiki.fysetc.com/TMC2208/)).
- Xiao RP2040

I followed the [tutorial](https://www.youtube.com/watch?v=5CmjB4WF5XA&t=502s) for the wiring. The TMC2208 used in the video and the one we had available at FabLab were different. [This page](https://wiki.fysetc.com/TMC2208/) has the written explanations of the abbreviations on the chip. I used this to figure out which output was which.

{{<image src="s1.jpg" alt="wiring scheme" size="52%" >}}
{{<image src="i1.jpg" alt="wiring on breadboard" size="40%" >}}

At first, I wanted to power the stepper with the 5V output from the Xiao, but I realized that this was not viable and I needed an external power source. I used an 12V adapter for this. When setting it up, we need to be very careful with the colors of the wires connecting to the AC supply. 

The attaching of the wires using the screws should be done BEFORE you plug the adapter in. After this, you can plug the adapter, and measure the voltage with a multimeter. Pay attention that the  V+ and V- line up with the multimeter reading.

{{<image src="i9.jpg" alt="color codes of wires" size="30%" >}}

{{<image src="i4.jpg" alt="wiring 12v adapter" size="48%" >}}
{{<image src="i5.jpg" alt="checking 12v adapter with multimeter" size="48%" >}}

The circuit draws power from two different sources. The Xiao board draws power from the USB connection of the computer. The TMC2208 is connected to the 3.3V output of the Xiao. The stepper draws power from the 12V adapter. Keep in mind that the grounds for the 3.3V and 12V power sources are different, as shown in the drawing above.

Figuring out the connections of the servo motor was also a challenge for me. It has two sets of wires connected to each other in pairs. It was a challenge to figure out how exactly I was supposed to connect them to the driver. With some help from Yuhan and the stepper's data sheet, this is how I connected them at the end:
- A+(Black) > 2B
- A-(Brown) > 2A
- B+(Orange) > 1A
- B-(Yellow) > 1B

{{<image src="s2.jpg" alt="connecting stepper's wires" size="30%" >}}

#### Setting Motor Current

The TMC2208 driver has a built-in feature that allows us to adjust the reference voltage according to the current limit of the motor. It is outlined here in this [tutorial](https://wiki.fysetc.com/TMC2208/).

Firstly, we check the max current of the stepper motor. According to the datasheet it is 400mA (0.4 A). to calcuate the reference voltage needed, we can use the calculator in the [tutorial](https://wiki.fysetc.com/TMC2208/) or the calculator [here](https://learn.watterott.com/silentstepstick/faq/#how-to-set-the-stepper-motor-current). They give roughly the same result. From the calculation we learn that the reference voltage should be 0.61V.

Before we start measuring the reference voltage, we have to disconnect the motor since it can easily get harmed during the process. Then we connect the USB power source and 12V adapter power source. According to the tutorial, we should put one end of the multimeter to the motor ground on the chip, and one end in the Vref pin.

{{<image src="i6.jpg" alt="wiring setup" size="48%" >}}
{{<image src="s3.jpg" alt="how to measure ref voltage" size="48%" >}}

When I tried this, I could not get a reading on the multimeter. With Yuhan's advice, I tried putting the (+) end of the multimeter to the potentiometer on the chip instead. This should give us the same reference voltage reading.

{{<image src="i2.jpg" alt="how to measure ref voltage not working" size="48%" >}}
{{<image src="i3.jpg" alt="how to measure ref voltage working" size="48%" >}}

Our aim is to make this reading 0.61V, since this was the result of our calculation. With small screwdriver, or tweezer's end, we turn the potentiometer and measure again until we get 0.61V.

{{<image src="i7.jpg" alt="adjusting potentiometer" size="48%" >}}

#### Testing the Code

After I adjusted the reference voltage, I plugged the motor back in and started to work on the code to make the stepper turn.

{{<image src="i8.jpg" alt="wiring setup" size="48%" >}}

This is the first code I wrote following the tutorial. It did nothing.

```go
int stepPin = D4;
int dirPin = D5;


void setup() {
  pinMode(D4,OUTPUT);
  pinMode(D5,OUTPUT);

}

void loop() {
  digitalWrite(dirPin,HIGH);

  for(int x=0; x<200; x++){
    digitalWrite(stepPin,HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin,LOW);
    delayMicroseconds(500);    
  } 

  delay (1000);

  digitalWrite(dirPin,LOW);

  for(int x=0; x<400; x++){
    digitalWrite(stepPin,HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin,LOW);
    delayMicroseconds(500);    
  } 

  delay (1000);
}
```

I tried adding `serial.println` into the for loops for debugging. So it could print `turning right` and `turning left` if the for loops were being executed. It printed correctly, so the code was being executed. But the motor still did not turn.

I tried downloading libraries from Arduino IDE, specifically "TMC2208Stepper" and "TMCStepper". I tried using examples from both libraries. They still did not work.

I tried looking up guides from [Yuhan's documentation on stepper motors](https://yuhantyh.gitlab.io/digital-fabrication/assignments/output_devices/), which led me to [this video](https://www.youtube.com/watch?v=fHAO7SW-SZI). I replicated the code from the video, as well as adding an ENABLE_PIN and connecting it to the EN pin on the TMC2208. It still did not work.

With Yuhan's help the next day, I managed to get the stepper to work. The two changes that solved it were:

1. Connecting the grounds between Motor Power Supply (12V) and Logic Power Supply (3.3V)
2. Setting up an "ENABLE_PIN", connecting it between the "En" pin in the TMC and a digital pin in Xiao. And it has to be configured in `void setup()` as `pinMode(ENABLE_PIN, OUTPUT);` and `digitalWrite(ENABLE_PIN, LOW);`

Below is the code that made it work. The `delay(10)` controls the speed of the turning. If it gets smaller, the motor turns faster. `digitalWrite(DIR_PIN, LOW)` controls the direction. LOW turns clockwise, HIGH turns counter-clockwise.

{{<video src="output1.mp4">}}Turning motor{{</video>}}

```go
int DIR_PIN = D5;
int STEP_PIN = D4;
int ENABLE_PIN = D6;

void setup() {
  Serial.begin(19200);
  pinMode(STEP_PIN,OUTPUT);
  pinMode(DIR_PIN,OUTPUT);
  pinMode(ENABLE_PIN,OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);

}

void loop() {
  digitalWrite(DIR_PIN, LOW);
  digitalWrite(STEP_PIN, HIGH);
  digitalWrite(STEP_PIN, LOW);
  delay(1);
}
```

Next, I wanted to turn it in a controlled manner. Following the [tutorial](https://www.youtube.com/watch?v=fHAO7SW-SZI), I gradually advanced the code. In the code below, the motor does one complete turn clockwise. `interval` controls the speed of the turning motion (higher = slower). `steps` controls how many steps the motor takes, (or how much it turns). By trial and error, I found out that the motor I had requires 385 steps to make a 360 degree turn. `while(true)` makes the loop stop when it does a single turn.

{{<video src="output2.1.mp4">}}One single turn{{</video>}}

```go
int DIR_PIN = D5;
int STEP_PIN = D4;
int ENABLE_PIN = D6;

void setup() {
  Serial.begin(19200);
  pinMode(STEP_PIN,OUTPUT);
  pinMode(DIR_PIN,OUTPUT);
  pinMode(ENABLE_PIN,OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);

}

void loop() {
  digitalWrite(DIR_PIN, LOW);
  simpleMove(385);
 /* digitalWrite(DIR_PIN, HIGH);
  simpleMove(385); */

  while(true);
}

void simpleMove(int steps){
  int interval = 3000;
  for (int i=0; i < steps; i++){
    digitalWrite(STEP_PIN, HIGH);
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(interval);
  } 
}
```

#### Adding Potentiometer

I moved on to adding the potentiometer. After I wired it to the 3.3V of the XIAO and common GRD, I wrote the code to control the motor with the turning of the potentiometer. 

My aim was to make the motor turn 1 step if the potentiometer's value was different than a few microseconds before. I tried the code below, but the issue was the 0-1024 scale of the potentiometer was too sensitive, and the analog reading was constantly fluctuating. This meant that the previous potentiometer value was always different, even if it was not turning; leading the motor to constantly try to move.

The code below was my first attempt at doing this. It did not work because it didn't account for these fluctuations.

```go
 int DIR_PIN = D5;
int STEP_PIN = D4;
int ENABLE_PIN = D6;
int POT_PIN = D1;
int potValue;
int prevPotValue =0;
int interval=3000;

void setup() {
  Serial.begin(19200);
  pinMode(STEP_PIN,OUTPUT);
  pinMode(DIR_PIN,OUTPUT);
  pinMode(ENABLE_PIN,OUTPUT);
  pinMode(POT_PIN, INPUT);
  digitalWrite(ENABLE_PIN, LOW);

}

void loop() {
  potValue=analogRead(POT_PIN);
  Serial.println(potValue);

  if(potValue>prevPotValue){
   moveClock(1);
  }else if(potValue<prevPotValue){
    moveCounterClock(1);
  } 

  prevPotValue = potValue;  
}

void moveClock(int steps){
  digitalWrite(DIR_PIN, LOW);
  for(int i=0; i<steps; i++){
    digitalWrite(STEP_PIN, HIGH);
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(interval);
  }
}

void moveCounterClock(int steps){
  digitalWrite(DIR_PIN, HIGH);
  for(int i=0; i<steps; i++){
    digitalWrite(STEP_PIN, HIGH);
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(interval);
  }
}

```

So I tried to map the scale of the potentiometer value to be less sensitive. I used the code below to debug the mapping in the serial monitor, instead of risking the mechanics of the stepper motor. 

Here are the variables that can be changed to have better control over the motor:

1. The last value in the map, `adjustPot=map(potValue,0,1024,0,100)` controls how sensitive the potentiometer is going to be. This also has an effect on the rest of the variables, so it is best to leave it at a rough value that you like and fine tune with the other variables.
2. `adjustPot+1` and `adjustPot-1` values in the if statements, prevent the motor from stepping when the potentiometer is not turning but the value is fluctuating. 
3. `delay(13)` gives a delay between the analog readings. This controls when the program actually registers a potentiometer turn as a step (the if statements being true or false). When this value is too low or too high, it causes problems with registering these turns. It's best to find a middle-ground sweet spot for this.

{{<video src="output3.mp4">}}Debugging potentiometer{{</video>}}

```go
void loop() {
  potValue=analogRead(POT_PIN);
  adjustPot=map(potValue,0,1024,0,100);
  
  Serial.print("adjustPot: ");
  Serial.print(adjustPot);

  if(adjustPot+1<prevAdjustPot){
    Serial.println(" // Turning: COUNTERCLOCKWISE"); 
  }else if(adjustPot-1>prevAdjustPot){
    Serial.println(" // Turning: CLOCKWISE");
  }else{
    Serial.println(" ");
  }

  delay(13);

  prevAdjustPot=adjustPot;
}
```

And here is the full final code that made it work.

{{<video src="output4.mp4">}}Working motor{{</video>}}

```go
int DIR_PIN = D5;
int STEP_PIN = D4;
int ENABLE_PIN = D6;
int POT_PIN = D1;
int potValue;
int adjustPot;
int prevAdjustPot=0;
int interval=3000;

void setup() {
  Serial.begin(19200);
  pinMode(STEP_PIN,OUTPUT);
  pinMode(DIR_PIN,OUTPUT);
  pinMode(ENABLE_PIN,OUTPUT);
  pinMode(POT_PIN, INPUT);
  digitalWrite(ENABLE_PIN, LOW);  
}

void loop() {
  potValue=analogRead(POT_PIN);
  adjustPot=map(potValue,0,1024,0,100);
  
  Serial.print("adjustPot: ");
  Serial.print(adjustPot);

  if(adjustPot+1<prevAdjustPot){
    Serial.println(" // Turning: COUNTERCLOCKWISE"); 
    moveCounterClock(10);
  }else if(adjustPot-1>prevAdjustPot){
    Serial.println(" // Turning: CLOCKWISE");
    moveClock(10);
  }else{
    Serial.println(" ");
  }

  delay(13);
  
  prevAdjustPot=adjustPot;  
}

void moveClock(int steps){
  digitalWrite(DIR_PIN, LOW);
  for(int i=0; i<steps; i++){
    digitalWrite(STEP_PIN, HIGH);
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(interval);
  }
}

void moveCounterClock(int steps){
  digitalWrite(DIR_PIN, HIGH);
  for(int i=0; i<steps; i++){
    digitalWrite(STEP_PIN, HIGH);
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(interval);
  }
}
```

### Making the Circuit Board

#### Milling

After making sure that the circuit works, I designed a circuit board for it. I used KiCad for this.

{{<image src="s5.jpg" alt="schematic" size="48%" >}}
{{<image src="s4.jpg" alt="schematic" size="48%" >}}

In the PCB editor, I realized that the holes for the TMC2208 were too big, since I planned to solder headers to the holes. In order to make them the same size as the XIAO's holes, I edited the holes' properties. We can access the properties by right clicking and then we edit "Hole shape- diameter" to be 0.8mm.

{{<image src="s6.jpg" alt="editing holes" size="48%" >}}

This time for milling I used Roland MDM-40. The online tutorial for it is a bit outdated so I still followed this [tutorial](https://wiki.aalto.fi/display/AF/PCB+Milling+with+Roland+SRM-20) for setting up the machine. The interface was a bit different however, and the final settings for that are below. It is best practice to reduce the Cutting Speed to %50 in the beginning of the cut, and increase it afterwards.

{{<image src="i9.1.jpg" alt="CNC interface" size="30%" >}}
{{<image src="i10.jpg" alt="CNC interface" size="30%" >}}

In the first cut, the bottom part of the circuit was too shallow.

{{<image src="i11.jpg" alt="failed cut" size="48%" >}}

To solve this, I placed the copper to more in the middle of the machine. And I did the Z height zeroing more to the middle of the circuit board. 

{{<image src="i12.jpg" alt="placing in the middle" size="48%" >}}
{{<image src="i13.jpg" alt="final board" size="48%" >}}

#### Assembling

I soldered headers to the board to mount the TMC and XIAO.

{{<image src="i14.jpg" alt="final board" size="48%" >}}
{{<image src="i15.jpg" alt="final board" size="48%" >}}

I then placed all the components in place and connected them with jumper cables. 

{{<image src="i17.jpg" alt="connecting with cables- front" size="48%" >}}
{{<image src="i18.jpg" alt="connecting with cables- back" size="48%" >}}

However, when I connected the XIAO to the computer and plugged in the 12V adapter, the code I uploaded did not turn the motor. I tried debugging with various methods. The serial monitor was registering that the potentiometer was turning, so the problem was with the motor. I checked with a multimeter and verified that power was going to the motor. So it was something else.

While debugging, I found out that when I connect the ENABLE_PIN to GND, the motor suddenly starts turning. However, when I touch the end of the cable to the solder, it stops. This made me realize that it was a soldering issue, as the ENABLE_PIN was not completely connected. After re-doing the solder it worked.

{{<video src="output5.mp4">}}Debugging{{</video>}}

Here is how the final product looks and works:

{{<image src="i19.jpg" alt="connecting with cables- back" size="48%" >}}

{{<video src="output6.mp4">}}Final Version{{</video>}}




