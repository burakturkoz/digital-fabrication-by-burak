+++
title = "3D Printing"
week_order = 7
+++

## Printing the Test Piece

I started this week by printing one of the test pieces provided by Fab Academy. It was the clearance piece. I first sliced it in Cura, and I decided to try the "Normal 1.5 mm" resolution setting. I printed with supports, and I was curious as to how the clearance would turn out.

{{<image src="ss1.jpg" alt="slicing clearance piece" size="45%" >}}

The print ran without issues.

{{<video src="output1.mp4">}}Printing the clearance piece{{</video>}}

Although there were problems with the output. The clearances were close to their real-world values, since the 1mm was flowing freely and the 0.1mm was basically fixed in place. But due to the layer height being 1.5mm, it actually lost it's dimensional accuracy. On top of this, the surface quality was really poor, especially on the sides with supports.

{{<image src="i2.jpg" alt="slicing clearance piece" size="45%" >}}
{{<image src="i3.jpg" alt="slicing clearance piece" size="45%" >}}

With this knowledge in mind, and knowing that I got familiar with the specific 3D printer in the lab, I moved onto my custom piece.

## Printing My Custom Piece

### Generative Design in Fusion 360

A workspace I've always wanted to explore further in Fusion was Generative Design. I messed around with it for a bit in the past, but I did not utilize it to create actual prototypes. In the course of Digital Fabrication, I want to explore it further and actually create things with it.

The process starts with us creating the base pieces in the design environment.

Then, we move on to the Generative Design environment. Here, the workflow follows the icons on the top from left to right. We assign certain pieces as "Preseve Geometry" (green) and some as "Obstacle Geometry" (red). 

Fusion will try to create a design based on the parameters we will give it, connecting the green pieces, while avoiding the red pieces.

{{<image src="ss2.jpg" alt="designing the pieces" size="45%" >}}
{{<image src="ss3.jpg" alt="preserve and obstacle geometry" size="45%" >}}

Now comes the part where we add the "load case". We basically give Fusion where and how much load is going to be applied to the piece, for it to use in the simulation. For my example, I chose 1kg of load to be carried by this piece.

Then, we can give it more constraints like "fixing" a piece in place, to simulate it being mounted on a wall for example.

In my example, I want the big horizontal circle have a pipe running through the middle. That's why I added a red obstacle geometry there, and also fixed it.

{{<image src="ss4.jpg" alt="load cases" size="45%" >}}
{{<image src="ss5.jpg" alt="constraints" size="45%" >}}

Also, we have to define the "objective" in the top panel. This is what Fusion will aim to do with the design. For example, I chose "minimal mass". Now the program will try to design a piece that can hold the specific load case I've given it, while using the less amount of material. Basically designing a strong but light part.

We also choose a manufacturing method in this stage. I've chosen "unrestriced", since I will be 3D printing it. But there are options to choose Milling, or Additive Manufacturing for example. 

After this, we choose the material and the study will use. I chose "Nylon 11" for this, since it the closest option in Fusion to a 3D printed plastic. But I was aware that this will not give me very exact real world conditions.

{{<image src="ss6.jpg" alt="materials" size="45%" >}}

After we start generating, the results start rolling in. It generates a few different results for us and asks us to choose. Here is the option I chose.

{{<image src="ss7.jpg" alt="generative results" size="45%" >}}
{{<image src="ss8.jpg" alt="chosen generative result" size="45%" >}}

### Printing Two Alternative Resolutions

I sliced it in Cura and started printing. Since my previous trial with 1.5 mm resolution didn't go as expected, I chose 1mm (Fine) and 0.8mm (Extra Fine). So I printed two different options to try and see the differences between them.

{{<image src="ss9.jpg" alt="slicing in cura" size="45%" >}}
{{<image src="ss10.jpg" alt="slicing in cura" size="45%" >}}

After printing and cleaning up, here are the results. The one on the left is 1.0 mm(Fine). The one on the right is 0.8mm(Extra Fine)

{{<image src="i6.jpg" alt="1.0mm print" size="45%" >}}
{{<image src="i7.jpg" alt="0.8mm print" size="45%" >}}

Cleaning up both was very hard. Since it had to use supports, the surface quality in these parts were very bad. I could not find a way yet to make the supports not destroy the surfaces.

Although the 0.8 mm print was better than the other one, in terms of dimensional accuracy.

{{<image src="i8.jpg" alt="bad surface quality" size="60%" >}}

## Structural Tests

Since this design was created with the purpose of carrying 1 kg of load. I wanted to try and see if Fusion and I were successfull in creating a part that lives up to this expectation.

{{<image src="i9.jpg" alt="testing setup" size="62%" >}}
{{<image src="i10.jpg" alt="weights" size="35%" >}}

### 860 gr Load

I used a water bottle as a weight. I first started with a smaller weight of 860 gr.

The part succesfully carried the load without failing.

{{<video src="output2.mp4">}}860gr Load Test{{</video>}}

### 1100 gr Load

I increased the load with an improvised contraption, to see if it fails around 1kg.

The part passed this test too. It can actually carry more than 1kg.

{{<video src="output3.mp4">}}1100gr Load Test{{</video>}}

### Structural Failure Test

Since it was apparent that the part was succesfull in carrying the expected load, the next thing I wanted to see was where it would fail when applied enough force. So I applied as much force as I could, until the part snapped.

My aim was to determine the weakest point of the structure. 

{{<video src="output4.mp4">}}Structural Failure Test{{</video>}}

Interestingly enough, the weak point was not the generatively designed connection that Fusion designed, but the initial circular geometry that I had given it.

I suppose this means that Fusion succeeded in designing a structurally sound part.

{{<image src="i11.jpg" alt="snap test result" size="62%" >}}