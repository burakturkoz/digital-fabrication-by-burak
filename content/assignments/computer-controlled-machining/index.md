+++
title = "Computer-Controlled Machining"
week_order = 9
+++

## CNC Milling

### Primary Instructions for Designing for CAM

1. Measure the material. Height, length and thickness. 
2. Measure the tool. Total length, flute length, tool diameter, number of flutes.
3. Generate toolpaths in a program such as VCarve, Fusion 360 or FreeCAD.

### Toolpaths in Fusion 360

#### Tool Dimensions, Feeds & Speeds

To design toolpaths in Fusion 360, I first added the tool that I was going to use. You can choose the shape and dimensions of the mill, and the feeds & speeds of the machine.

{{<image src="ss1.jpg" alt="adding tool in Fusion" size="65%" >}}

The properties of the mill are as follows:
> No. of Flutes = 2
>
> Material = HSS
>
> Diameter = 6mm
>
> Shaft Diameter = 6mm
>
> Overall Length = 79.5mm
>
> Length Below Holder = 50mm
>
> Shoulder Length = 46mm
>
> Flute Length = 19mm

{{<image src="ss2.jpg" alt="tool properties" size="65%" >}}

Here is the formula we use to calculate the feeds & speeds:
> **fr = tf * nf * ss**
>
> fr: feed rate
>
> tf: tooth feed (in mm/ tooth/ revolution)
>
> nf: number of flutes
> 
> ss: spindle speed (rpm)

Using this formula, I calculated the feeds and speeds for plywood as listed below: [DO NOT USE THESE VALUES. THEY ARE WRONG. CORRECT VALUES LISTED FURTHER DOWN]
> Spindle Speed = 16000 rpm
>
> Cutting Feedrate = 1000 mm/min
>
> Plunge Feedrate = 560 mm/min

#### Modeling the Test Piece

After setting up the tool, I started modeling a test piece. With this piece I wanted to test different joints, dimensional accuracy, and the feeds & speeds.

Initially for the joints, I wanted to have a snug fit without doing any refining operations such as sanding after the milling. The solution I came up with for this, was to add holes slightly larger than the tool diameter. My friend pointed out that these looked like dog bones. Conveniently, I later found out that this method was actually called "dog bone".

{{<image src="ss3.2.jpg" alt="test piece" size="65%" >}}

Then, I moved on to the manufacturing workspace. I first added a stock, with the same thickness of the plywood that I was going to use. I selected the origin as the left bottom corner of the stock.

{{<image src="ss3.jpg" alt="adding stock" size="65%" >}}

After the setup was complete, I started to generate toolpaths. I did not know exactly how to do this properly, so I got help from this [tutorial](https://www.youtube.com/watch?v=VPMvnzmuTOw).

Here is a quick explanation of what I learned in terms of toolpaths that I used:

> **2D Toolpaths:** Starts from nothing and you add what you want to cut.
>
> **3D Toolpaths:** Starts by cutting everything and you limit what you want to cut.
>
> **2D Adaptive Clearing:** Used to clear pockets or sloped surfaces.
>
> **2D Contour:** Used to cut contours around the parts. Supports tabs.

For example, on the left is an example of 3D Adaptive Clearing, which clears all the stock to create the selected geometry. On the right is an example of 2D Adaptive Clearing, which only clears the geometry you choose.

{{<image src="ss7.jpg" alt="3d adaptive" size="45%" >}}
{{<image src="ss6.jpg" alt="2d adaptive" size="45%" >}}

However, when I tried to generate toolpaths I realized that the "dog bone" type of joints weren't cutting although they were larger than the tool diameter. This was because the tool was plunging down in a helix motion. So I went back and made the dog bone holes bigger.

{{<image src="ss4.jpg" alt="dog bone cutting" size="45%" >}}
{{<image src="ss5.jpg" alt="dog bone cutting" size="45%" >}}

For setting the heights, I put the "top height" of the stock 2mm higher than it actually is, to be safe during machining. And I put the bottom height offset 0.3mm from the bottom of the stock to ensure that the machine cut through.

{{<image src="ss9.jpg" alt="heights" size="65%" >}}

Finally, I added tabs with the contour, to prevent the piece from falling down in the final layer cut. I also added two more holes in this stage, to test the differences between the dog bone cut and normal cut.

{{<image src="ss10.jpg" alt="tabs" size="65%" >}}

#### Modeling the Coffee Table

After getting familiar with the manufacturing workspace in Fusion, I moved on to making my main project. I decided to make a coffee table for my apartment.

{{<image src="ss13.jpg" alt="coffee table model" size="65%" >}}

I went through the same steps as I did with the test piece, and set up the toolpaths for the individual pieces.

{{<image src="ss14.jpg" alt="coffee table stock" size="45%" >}}
{{<image src="ss15.jpg" alt="coffee table toolpaths" size="45%" >}}

Finally, for both the test piece and coffee table, we have to post-process it before exporting it as a G-Code file. In this stage, we have to select a post-processor. For the Recontech 1312 that we have at the lab, we should choose "Mach 3 Generic Profile" in Fusion. 

{{<image src="ss12.jpg" alt="coffee table model" size="65%" >}}

### Using the CNC Mill

#### Learning the Basics

The machine that we have in FabLab is a Recontech 1312. There are a few steps to get the machine working:

1. Choose a fitting tool for your job.
2. According to the dimensions of the tool, calculate the feeds and speeds required for the job.
3. Generate your toolpath on VCarve or Fusion. 
 1. Make sure to set the origin correctly. It should be on the left bottom with Z pointing towards the camera, Y+ pointing up, and X+ pointing to the right (bird's eye view).
4. Export the toolpath as G-Code.
5. Turn on the CNC machine with the two-stage power buttons.
6. Open Mach 3 in the computer in the CNC room.
7. Hit "Reset"
8. Try jogging the machine by hitting "tab" and pulling up the jog menu. Arrows for horizontal, PgUp PgDown for vertical movement.
9. Open the vacuum covers if you're gonna use the vacuum, put in rubber bands.
10. Put your sacrificial layer and stock. Clamp if needed.
11. Put on ear protection and turn on the vacuum. Tighten clamps more.
12. Zero the X and Y axis with jogging the machine and hitting zero in Mach3.
13. Zero the Z axis by using the device attached to the machine and hitting "teran mittaus" in Mach3.
14. Load G Code into Mach 3. Check from preview window.
15. Go outside, close the door and hit the green button to begin.

{{<image src="i12.jpg" alt="recontech 1312" size="65%" >}}

#### Problems with the Machine

We encountered lots of problems with the CNC during this week. The one that came up while I was using it was concerning the vacuum.

We put the stock and sacrifical layer, and opened all the vacuum covers. However, when we turned on the vacuum, it did not stabilize the stock. We tried rearranging the rubber bands, turning on only one vacuum chamber, and turning over the stock but nothing worked. 

At the end, we decided to use clamps without the vacuum, and arranged the toolpath to steer clear from them.

{{<image src="i13.jpg" alt="clamps" size="65%" >}}

#### New Tool, New Feeds, New Speeds

Due to some issues with the 6mm tool being damaged, I had to switch to using a 3mm tool. For this, we had to calculate new values.

The properties of the mill are as follows:
> No. of Flutes = 2
>
> Material = HSS
>
> Diameter = 3mm
>
> Shaft Diameter = 3mm
>
> Overall Length = 75mm
>
> Length Below Holder = 40mm
>
> Shoulder Length = 40mm
>
> Flute Length = 25mm

For the spindle speed, we used the maximum speed that Recontech 1312 could go up to. We calculated the feed from that, with the equation fr = tf * nf * ss. But this feed turned out to be too slow, so we used a larger value that has been tried in this machine before.
 
Here are the final feeds and speeds we used:

> Spindle Speed = 18000 rpm
>
> Cutting Feedrate = 1200 mm/min
>
> Plunge Feedrate = 140 mm/min
>
> Pass Depth = 1.5 mm
>
> Stepover = 1.5 mm (50% of tool diameter)

{{<image src="i1.jpg" alt="feed and speed chart" size="65%" >}}

#### Generating Toolpaths in VCarve

Since VCarve already had the machine's settings saved inside, we decided to generate the toolpaths there instead of Fusion to avoid any errors.

The workflow is similar to Fusion, but you have to import DXF files into here. Since my sketches in Fusion was not optimized for this, I used the "DXF for Laser Cut" extension that I used in the Laser Cutting week to export the faces as DXF files.

We entered the values I stated above into VCarve, and configured two different cutting profiles: one for the contours and one for the holes. For the holes, we also used a contour style cut, which saved a very big amount of time compared to Fusion's adaptive clearing method.

We set the "cut depth" to be 14.9, to have a paper thin layer at the last stage of the cut. We also added tabs.

#### Machining the Test Piece

After setting up, I machined the test piece to test out the cuts. I modified the design slightly before this to reduce the cutting time.

The cut turned out well. Although the surface quality was low, it was sufficient enough for this stage.

{{<image src="i8.jpg" alt="test piece cut" size="45%" >}}
{{<image src="i9.jpg" alt="test piece cut" size="45%" >}}

What I wanted to test with this piece was the joints. What I learned is that the "dog bone" type of joint was not ideal for my specific use case. Since I was working with a thinner piece, the dog bone joint did not have enough contact surfaces to be a snug fit, it was too loose. 

The alternative normal joint requires a bit of post-cnc treatment in terms of sanding and filing, but since I did not have too many such holes in my design, doing this was a better alternative for me. This way I could have a more snug fit.

{{<image src="i10.jpg" alt="test piece joinery" size="45%" >}}
{{<image src="i11.jpg" alt="test piece joinery" size="45%" >}}

#### Machining the Coffee Table

I decided to use the normal joints with fillets in my final cut. I offset the holes by 0.2 mm to the inside anyway, to avoid any errors. If the hole is smaller, I can always sand it down and make the joint fit, but if it is large and loose that is a bigger problem.

I used the same settings that I used for the test piece. The cuts turned out really well.

{{<image src="i14.jpg" alt="coffee table milled" size="45%" >}}


### Post-CNC Work

After I was done with CNC milling, I first cut off the tabs.

{{<image src="i15.jpg" alt="cutting off tabs" size="65%" >}}

Then, I sanded the individual pieces. I used 100 and 245 sanding paper for the surfaces. I also used a circular sanding machine.

{{<image src="i16.jpg" alt="sanding" size="45%" >}}

To make it resistant to water and other such elements, I varnished the pieces. It was my first time applying varnish.

{{<image src="i17.jpg" alt="varnishing" size="65%" >}}

### Finished Product

{{<image src="i18.jpg" alt="final product" size="65%" >}}

