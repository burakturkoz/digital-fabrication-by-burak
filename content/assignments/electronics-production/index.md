+++
title = "Electronics Production"
week_order = 10
+++

## Manufacturing a Circuit Board

### Exporting From KiCad

After we prepared our KiCad file in Week 8, now is the time to export it. Before exporting however, I decided to make some changes in my circuit board.

My last design featured a neo-pixel LED, however I realized this week that we did not have it in the FabLab inventory. I accidentally used something from the default KiCad libraries.

I found the neopixel LED that was present in the lab, but couldn't figure out how to wire it. I will have to ask it later in this week's review. Anyway, I decided to not use the neo-pixel for this assignment.

My updated design consists of LEDs(x2), resistors(x2), button(x1) and potentiometer(x1). I also changed their orientation to make the paths simpler.

{{<image src="s1.1.jpg" alt="old design" size="48%" >}}
{{<image src="s1.jpg" alt="new design" size="48%" >}}

{{<image src="s2.jpg" alt="new design" size="65%" >}}


To export our design as a gerber file, we follow these steps:

1. Choose the "Plot" option on the top bar
2. Choose gerber as the output format, and select directory
3. Select the layers we want to export
 1. Include layers: Front copper layer
 2. Plot on all layers: Edge cut
4. Uncheck footprint values and reference designators since we're doing it ourselves
5. Don't generate gerber job file
6. Use extended x2
7. Uncheck include netlist
8. Hit plot

At this point, I got an error saying "width correction constrained". I closed the windows, did it again, and it solved itself.

{{<image src="s4.jpg" alt="plot window" size="65%" >}}

For drilling the holes, we do the following:

9. Choose "generate drill files" 
10. Change drill file format to Gerberx2
11. Click "generate drill files" again

{{<image src="s5.jpg" alt="generating drill files" size="65%" >}}

This will create two files: "plated through holes" and "non-plated through holes". Plated through holes are taken into account in the milling process in Coppercam. Non-plated through holes are used for mechanical purpoeses. So the only file we need is the PTH file.

We can now close the window and go to gerber viewer in KiCad, and do the following:

1. File>Open gerber plot files
2. Open f_cu gbr and pth gbr

At this point, I realized that the gerber file I created had only consisted of the lines between the components. This would work fine, but the milling process would take longer since it would have to mill the whole surface but the connection lines.

{{<image src="s6.jpg" alt="wrong gerber file" size="65%" >}}

In order to fix this, I learned that I would have to create a "ground plane" in KiCad. This way, I could avoid unneccessary connection lines for connecting to ground since the whole surface would be ground. This would also save a lot in milling times.

To do this, I went back to the PCB editor and added a "copper zone". I selected the front copper layer and "PWR_GND". Then, we just have to draw an outline for the copper zone.

{{<image src="s7.jpg" alt="creating ground copper zone" size="48%" >}}
{{<image src="s8.jpg" alt="creating ground copper zone" size="48%" >}}

I went back and re-did the same steps, to export my gerber file.

### Coppercam

In order to create toolpaths to mill our circuit board, we use a software called Coppercam.

Initially, I downloaded it to my own computer and followed this [tutorial for PCB milling with Roland SRM-20](https://wiki.aalto.fi/display/AF/PCB+Milling+with+Roland+SRM-20).

After going through all the steps outlined in the tutorial, I got an error that said "demonstration version".

{{<image src="s10.jpg" alt="coppercam error" size="65%" >}}

It turned out that this error was probably caused because Coppercam is a paid program and I had the free version. So I re-did the same process in the computer at the FabLab that we use with the Roland SRM-20.

It ran without errors and I got two ".egx" files, one for Tool 1 (engraving) and one for Tool 2 (cutting). 

### Milling with Roland SRM-20

After I got my toolpaths, I moved on to milling with the Roland SRM-20. Again, I continued following the same [tutorial](https://wiki.aalto.fi/display/AF/PCB+Milling+with+Roland+SRM-20).

At first when I tried to open VPenel for SRM-20, I got this error message:

{{<image src="i1.jpg" alt="vpenel error" size="48%" >}}

The reason for this was because I left the cover of the machine open. If the cover is open, you cannot interact with it using VPenel.

After going through the tutorial and setting up the toolpaths, I started milling.

{{<video src="output1.mp4">}}Milling first iteration{{</video>}}

#### Problems with the First Iteration

The first board that I milled had several issues. Firstly, the engravings on the left side and the right side were not equal in depth. Secondly, the hole that was supposed to be cut in the middle was not there.

Also, some connections were too close to each other for comfort. There were some unnecessary turns and corners as well.

{{<image src="i2.jpg" alt="first iteration errors" size="48%" >}}
{{<image src="i3.jpg" alt="first iteration errors" size="48%" >}}

I first cleaned up the connections in KiCad. I got rid of unnecessary twists & turns, and routes that were too close to each other were split farther apart.

{{<image src="s11.jpg" alt="cleaning up in KiCad" size="48%" >}}

After exporting, I started changing some settings in Coppercam. I realized that I set the Z thickness as 1.9mm, when my board was only 1.5mm. I changed this to 1.6mm (adding a 0.1mm clearance).

I also set the cutting and drilling depths as 1.7mm. The final working tool settings are down below.

{{<image src="i4.jpg" alt="z thickness correction" size="48%" >}}
{{<image src="i5.jpg" alt="final tool settings" size="48%" >}}

And for the uncut hole in the middle, I learned that I had to specifically set it as a contour inside Coppercam.

{{<image src="i6.jpg" alt="fixing the uncut hole" size="48%" >}}
{{<image src="i7.jpg" alt="fixing the uncut hole" size="48%" >}}

In the first iteration, I had taken the measurement for the Z-height from the side of the board. This time, I made sure to take it from the middle of my cutting area.

{{<image src="i8.jpg" alt="Z height measurement" size="48%" >}}

I started milling the second iteration

{{<video src="output2.mp4">}}Milling second iteration{{</video>}}

#### Problems with the Second Iteration

Even after the troubleshooting, the second iteration's engraving was still very shallow.

{{<image src="i9.jpg" alt="shallow engraving" size="48%" >}}

To fix this, we tried changing the engraving tool in case it was damaged. After the change we did not change any other settings about the job other than re-calibrating the Z-height.

This worked really well. The new engraving tool cut as deep as I wanted.

{{<image src="i10.jpg" alt="with changed tool" size="48%" >}}

Here is a comparison of the first iteration and second iteration

{{<image src="i11.jpg" alt="comparison" size="48%" >}}

### Soldering 

After succesfully milling, I moved on to soldering. Using a 240 sandpaper, I gently sanded the copper layer first.

I first applied flux to the area I wanted to solder.

{{<image src="i13.jpg" alt="applying flux" size="48%" >}}

Then, I started soldering the surface mount components. It was my first time soldering surface mounts. At first, I was using a really thick solder wire. This resulted in really bad quality solders and was very hard to control.

{{<image src="i14.jpg" alt="soldering with thick solder wire" size="48%" >}}
{{<image src="i15.jpg" alt="soldering with thick solder wire" size="48%" >}}

When I moved onto thinner solder wire, everything was much easier. Here is the steps I followed for soldering surface mount components:

1. Apply a small amount of solder to one of the pads. First heat the pad with the iron a bit, then touch the solder wire and let it melt.
2. With the tweezers, grab the component. While heating up the solder with the iron, push the component down onto the solder.
3. After one side is secure, solder the other pads.
4. After all the pads are securely soldered, we can return back to our first solder and re-do it properly.

{{<image src="i16.jpg" alt="steps of soldering surface mounts" size="48%" >}}
{{<image src="i17.jpg" alt="steps of soldering surface mounts" size="48%" >}}
{{<image src="i18.jpg" alt="steps of soldering surface mounts" size="48%" >}}

I soldered all the components but the headers for the Xiao board. I couldn't solder that yet because we didn't have it in stock at the moment.

{{<image src="i19.jpg" alt="soldered all components" size="48%" >}}

After this, I realized that I had some solder bridges and sloppy solders at some points. I tried cleaning them up as much as I could.

{{<image src="i20.jpg" alt="fixing solder bridges" size="48%" >}}
{{<image src="i21.jpg" alt="fixing solder bridges" size="48%" >}}

To connect the XIAO to the circuit board, I soldered the headers first. After mounting the XIAO to the headers, I tested the connections with a multimeter to avoid any short circuits. Most importantly, I checked if there was a short between 3.3V-Ground and 5V-Ground.

{{<image src="i23.1.jpg" alt="mounting the xiao" size="48%" >}}
{{<image src="i22.1.jpg" alt="checking the multimeter" size="48%" >}}


### Programming the XIAO Board

#### Turning on LEDs with Button Press

Firsly, I tested the button press code to see if the board was registering button presses. It worked. Also don't forget to add input_pullup to the pinMode to use the internal resistor.

{{<video src="output1.1.mp4">}}Testing button press{{</video>}}

```go
int btnPin = D4;
int btnState;

void setup() {
  Serial.begin(19200);
  pinMode(btnPin, INPUT_PULLUP); 
}

void loop() {
  if (digitalRead(btnPin)==1){
    Serial.println("no press");
  }else{
    Serial.println("press!");
  }
  delay(10);
}
```

Then I started testing with the LED's. However, the LED's were turned ON when I set them as HIGH. This was not the case with the built in LED's of the XIAO board. So I changed the HIGH's with LOW's and re-ran the code. It worked this time.

{{<video src="output2.1.mp4">}}LOW and HIGH reversed{{</video>}}

{{<video src="output3.mp4">}}LOW and HIGH correct order{{</video>}}

```go
int btnPin = D4;
int btnState;
int ledPinA = D0;
int ledPinB = D1;

void setup() {
  Serial.begin(19200);

  pinMode(btnPin, INPUT_PULLUP); 
  pinMode(ledPinA, OUTPUT);
  pinMode(ledPinB, OUTPUT);

  digitalWrite(ledPinA, LOW);
  digitalWrite(ledPinB, LOW);
}

void loop() {

  if (digitalRead(btnPin)==1){
    Serial.println("no press");
    digitalWrite(ledPinA, LOW);
  }else{
    Serial.println("press!");
    digitalWrite(ledPinA, HIGH);
  }

  delay(10);
}
```

After this, I experimented with turning the LED's on and off with a single button press. After messing around a bit, I found the solution to make an increasing counter and then check if the number is even or odd. The method of increasing the counter with each button press is the same one I used in the Embedded Programming week. I added the odd/ even number checker to this code by using the "modulus operator". Which is a fancy name for division in Arduino IDE.

{{<video src="output4.mp4">}}Turning on LED with single button press{{</video>}}

```go
int btnPin = D4;
int btnState = 1; //1 means not pressed
int prevBtnState = 1;
int counter = 1;

int ledPinA = D0;
int ledPinB = D1;

void setup() {
  Serial.begin(19200);

  pinMode(btnPin, INPUT_PULLUP); 
  pinMode(ledPinA, OUTPUT);
  pinMode(ledPinB, OUTPUT);

  digitalWrite(ledPinA, LOW);
  digitalWrite(ledPinB, LOW);
}

void loop() {

  btnState = digitalRead(btnPin);

  if (btnState != prevBtnState){
    if(btnState == 0){
      counter ++; //with every press of the button, counter goes up by one
      switch(counter % 2){ //checks if counter is even or odd
        case 0:
          Serial.println("LED ON");
          digitalWrite(ledPinA, HIGH);
          digitalWrite(ledPinB, HIGH);
          break;
        case 1:
          Serial.println("LED OFF");
          digitalWrite(ledPinA, LOW);
          digitalWrite(ledPinB, LOW);
          break;
      }
    }
  }

  prevBtnState = btnState;

  delay(10);
}
```

#### Controlling LED Brightness with Potentiometer 

I moved onto the potentiometer. I was not sure if I should write a `pinMode(potPin, INPUT_PULLUP)` statement for it like the button, so I asked ChatGPT. It said that it would be wrong to write PULLUP, because potentiometers don't have internal resistors. Also, it was not needed to write this statement at all, since analog pins are read as inputs by default.

{{<image src="s12.jpg" alt="chatgpt answer" size="48%" >}}

I tried the code below with the `pinMode(potPin, INPUT)` and without it. Both worked well and I got a reading.

```go
int potPin = D3;
int potVal = 0;

void setup() {
  Serial.begin(19200);
}

void loop() {

  // detecting potentiometer input for brightness
    
  potVal = analogRead(potPin);
  Serial.println(potVal);

  delay(10);
}
```

{{<video src="output5.mp4">}}Controlling brightness of one LED{{</video>}}

To bring it all together, I first tried the code below. However, the digitalWrite and analogWrite did not work at the same time.

```go
int btnPin = D4;
int btnState = 1; //1 means not pressed
int prevBtnState = 1;
int counter = 1;

int ledPinA = D0;
int ledPinB = D1;
int ledBrightness = 255;

int potPin = D3;
int potVal = 0;

void setup() {
  Serial.begin(19200);

  pinMode(btnPin, INPUT_PULLUP); 

  pinMode(ledPinA, OUTPUT);
  pinMode(ledPinB, OUTPUT);

  digitalWrite(ledPinA, LOW);
  digitalWrite(ledPinB, LOW);
}

void loop() {

  //detecting button press to turn on/off LEDs

  btnState = digitalRead(btnPin);

  if (btnState != prevBtnState){
    if(btnState == 0){
      counter ++; //with every press of the button, counter goes up by one
      switch(counter % 2){ //checks if counter is even or odd
        case 0:
          Serial.println("LED ON");
          digitalWrite(ledPinA, HIGH);
          digitalWrite(ledPinB, HIGH);
          break;
        case 1:
          Serial.println("LED OFF");
          digitalWrite(ledPinA, LOW);
          digitalWrite(ledPinB, LOW);
          break;
      }
    }
  }
  prevBtnState = btnState;
  
  // detecting potentiometer input for brightness
    
  potVal = analogRead(potPin);
  Serial.println(potVal);

  ledBrightness = map(potVal, 0, 1000, 0, 255);
  analogWrite(ledPinA, ledBrightness);

  delay(10);
}
```

I tried putting the potentiometer code inside `case 0:` to only execute it when the LED's were on. But this did not work either. The brightness stayed the same. 

{{<video src="output7r.mp4">}}Can't control brightness{{</video>}}

```go
switch (counter % 2) {  //checks if counter is even or odd
        case 0:
          Serial.println("LED ON");
          digitalWrite(ledPinA, HIGH);
          digitalWrite(ledPinB, HIGH);

          // detecting potentiometer input for brightness
          potVal = analogRead(potPin);
          Serial.println(potVal);
          ledBrightness = map(potVal, 0, 1000, 0, 255);
          analogWrite(ledPinA, ledBrightness);
          break;
        case 1:
          Serial.println("LED OFF");
          digitalWrite(ledPinA, LOW);
          digitalWrite(ledPinB, LOW);
          break;
      }
```

This led me to remove the `digitalWrite(ledPinA, HIGH)` statement because it was setting it to a high from the beginning. After this, I could control the brightness of the LED with the potentiometer, but not in real time. The change in brightness registered only when I turned it on and off again.

Most probably, the reason for this was that once the button is pressed, `btnState=prevBtnState` gets executed. This causes the switch statement to not be read on a loop, but only when the button is pressed. Therefore I cannot get the brightness control in real-time.

{{<video src="output8.mp4">}}Can't control brightness in real time{{</video>}}

```go
void loop() {
  //detecting button press to turn on/off LEDs
  btnState = digitalRead(btnPin);
  if (btnState != prevBtnState) {
    if (btnState == 0) {
      counter++;              //with every press of the button, counter goes up by one
      switch (counter % 2) {  //checks if counter is even or odd
        case 0:
          Serial.println("LED ON");
          /* digitalWrite(ledPinA, HIGH); */
          digitalWrite(ledPinB, HIGH);
          // detecting potentiometer input for brightness
          potVal = analogRead(potPin);
          Serial.println(potVal);
          ledBrightness = map(potVal, 0, 1000, 0, 255);
          analogWrite(ledPinA, ledBrightness);
          break;
        case 1:
          Serial.println("LED OFF");
          digitalWrite(ledPinA, LOW);
          digitalWrite(ledPinB, LOW);
          break;
      }
    }
  }
  prevBtnState = btnState;
  delay(10);
}
```

In order to get it to work, I created a `turnOn` variable that would be either true or false. When the button is pressed and the LED's are on, `turnOn` is true. When this happens, the potentiometer brightness control code is allowed to execute. When the button is pressed again, `turnOn` becomes false. LED's are turned off by `digitalWrite`, and the potentiometer code cannot be executed. 

This way, I can turn the LED's on and off with the button, and control their brightness with the potentiometer in real time. Below is the code and video.

{{<video src="output9.mp4">}}Final working example{{</video>}}

```go
int btnPin = D4;
int btnState = 1;  //1 means not pressed
int prevBtnState = 1;
int counter = 1;

int ledPinA = D0;
int ledPinB = D1;
int ledBrightness=255;

int potPin = D3;
int potVal = 0;

int turnOn = false; 

void setup() {
  Serial.begin(19200);

  pinMode(btnPin, INPUT_PULLUP);

  pinMode(ledPinA, OUTPUT);
  pinMode(ledPinB, OUTPUT);

  digitalWrite(ledPinA, LOW);
  digitalWrite(ledPinB, LOW);
}

void loop() {

  //detecting button press to turn on/off LEDs
  btnState = digitalRead(btnPin);

  if (btnState != prevBtnState) {
    if (btnState == 0) {
      counter++;              //with every press of the button, counter goes up by one
      switch (counter % 2) {  //checks if counter is even or odd
        case 0:
          Serial.println("LED ON");
          analogWrite(ledPinA, ledBrightness); 
          analogWrite(ledPinB, ledBrightness);
          turnOn = true;
          break;
        case 1:
          Serial.println("LED OFF");
          digitalWrite(ledPinA, LOW);
          digitalWrite(ledPinB, LOW);
          turnOn = false;
          break;
      }
    }
  }

  prevBtnState = btnState;

  if (turnOn == true){
       // detecting potentiometer input for brightness
          potVal = analogRead(potPin);
          Serial.println(potVal);
          ledBrightness = map(potVal, 0, 1000, 0, 255);
          analogWrite(ledPinA, ledBrightness);
          analogWrite(ledPinB, ledBrightness);
  }

  delay(10);
}
```