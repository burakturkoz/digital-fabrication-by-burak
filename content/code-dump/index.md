+++
title = "Code Dump"
+++

## A Place to Store Work-In-Progress Code

{{< detail-tag "CODE: Accelerating Stepper Motor / WiFi Controlled / Final Project [WORKING v1]" >}}

### Accelerating Stepper Motor / WiFi Controlled / Final Project [WORKING v1]

#### Arduino IDE

```go
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

#define STEPS 400

// Some variables we will need along the way
const char *ssid = "salvatore ferragamo";
const char *password = "macncheese";
const char *PARAM_MESSAGE = "message";
int webServerPort = 80;

int DIR_PIN = D5;
int STEP_PIN = D4;
int ENABLE_PIN = D6;
int POT_PIN = D1;
int potValue;
int adjustPot;
int prevAdjustPot = 0;
int interval = 3000;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void sendResponse(AsyncWebServerRequest *request, String message) {
  AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", message);
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void setup() {

  pinMode(STEP_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  pinMode(POT_PIN, INPUT);
  digitalWrite(ENABLE_PIN, LOW);

  Serial.begin(19200);
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    sendResponse(request, "Hello!");
  });

  server.on("/motor", HTTP_GET, [](AsyncWebServerRequest *request) {
    int state;  // motor state
    if (request->hasParam("state")) {
      // The incoming params are Strings
      String param = request->getParam("state")->value();
      // .. so we have to interpret or cast them
      if (param == "CWturn") {
        state = 2;
      } else if (param == "CCWturn") {
        state = 3;
      } else {
        state = 0;
      }
    } else {
      state = 0;
    }

    // Send back message to human

    String stateString;  // Declare the variable outside the if statement

    if (state == 2) {
      Serial.println("turningcw");
      digitalWrite(DIR_PIN, LOW);
      constantAccel();
      stateString = "isTurningCW";
    } else if (state == 3) {
      Serial.println("turningCCW");
      digitalWrite(DIR_PIN, HIGH);
      constantAccel();
      stateString = "isTurningCCW";
    } else {
      stateString = "notTurning";
    }

    String responseJSON = "{\"motorState\":\"" + stateString + "\"}";
    sendResponse(request, responseJSON);

  });

  server.on("/params", HTTP_GET, [](AsyncWebServerRequest *request) {
    int param1 = random(100);
    int param2 = random(100);
    int param3 = random(100);
    int param4 = random(100);

    String responseJSON = "{";
    responseJSON += "\"param1\":" + String(param1) + ",";
    responseJSON += "\"param2\":" + String(param2) + ",";
    responseJSON += "\"param3\":" + String(param3) + ",";
    responseJSON += "\"param4\":" + String(param4) + ",";
    responseJSON += "}";

    sendResponse(request, responseJSON);
  });

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();
}

void loop() {

}

void moveClock(int steps) {
  digitalWrite(DIR_PIN, LOW);
  for (int i = 0; i < steps; i++) {
    digitalWrite(STEP_PIN, HIGH);
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(interval);
  }
}

void moveCounterClock(int steps) {
  digitalWrite(DIR_PIN, HIGH);
  for (int i = 0; i < steps; i++) {
    digitalWrite(STEP_PIN, HIGH);
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(interval);
  }
}

void constantAccel(){
  int delays[STEPS];
  float angle = 1;
  float accel = 0.01;
  float c0 = 2000 * sqrt(2 * angle / accel ) * 0.67703;
  float lastDelay = 0;
  int highSpeed = 100;
  for (int i=0; i< STEPS; i++){
    float d = c0;
    if (i>0)
      d = lastDelay - (2 * lastDelay)/(4*i+1);
    if (d<highSpeed)
      d = highSpeed;
    delays[i] = d;
    lastDelay = d;   
  }

  for (int i= 0; i<STEPS; i++){
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds (delays[i]);
    digitalWrite(STEP_PIN, LOW);
  }    

    for (int i= 0; i<STEPS; i++){
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds (delays[STEPS-i-1]);
    digitalWrite(STEP_PIN, LOW);
  }    
}
```


#### jQuery 

```go
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>BootStrap Rest DEMO</title>
  <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
</head>

<body>

  <header class="container mt-3">
    <div id="status-bar" class="alert alert-light" role="alert">
      API Status: <span id="status-api">Undefined</span>
    </div>
  </header>

  <main class="container">
    <h1>stepper control test</h1>
    <div id="motor-sign">Motor Rotation</div>
    <button id="button-counter-clockwise" class="btn btn-warning">Turn Counter-clockwise</button>
    <button id="button-clockwise" class="btn btn-info">Turn Clockwise</button>
  </main>

  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/jquery/jquery-3.6.4.min.js"></script>
  <script>
    function checkAPIStatus() {
      $.ajax({
        url: "http://192.168.223.93/",
        timeout: 5000
      })
        .done(function () {
          $("#status-api").text("Connected");
          if ($("#status-bar").hasClass("alert-light")) {
            $("#status-bar").removeClass("alert-light");
          }
          $("#status-bar").addClass("alert-success");
        })
        .fail(function () {
          $("#status-api").text("Not connected");
        })
    }

    function triggerTurn(direction) {
      if (direction == 2) {
        $("#motor-sign").data("motorState", "CWturn");
      } else if (direction == 3) {
        $("#motor-sign").data("motorState", "CCWturn");
      } else {
        $("#motor-sign").data("motorState", "noAction");
      }

      const myState = $("#motor-sign").data("motorState");
      console.log("sent: " + myState);

      $.ajax({
        url: "http://192.168.223.93/motor",
        data: {
          state: myState
        },
        timeout: 5000
      })
        .done(function (response) {
          const responseJSON = JSON.parse(response); //we convert this into an object so that:
          console.log("received: " + responseJSON.motorState);
          if (responseJSON.motorState == "isTurningCW") {
            $("#motor-sign").css("background", "cyan");
          } else if (responseJSON.motorState == "isTurningCCW") {
            $("#motor-sign").css("background", "orange");
          } else {
            $("#motor-sign").css("background", "gray");
          }
        })
        .fail(function () {
          console.log("motor trigger call failed.");
        })
    }

    $(document).ready(function () {
      console.log("Document has loaded!");

      setInterval(checkAPIStatus, 2000);

      $("#button-clockwise").click(function () {
        triggerTurn(2);
      });

      $("#button-counter-clockwise").click(function () {
        triggerTurn(3);
      });
    });
  </script>
</body>

</html>
```
{{< /detail-tag >}}

{{< detail-tag "CODE: Accelerating Stepper Motor / WiFi Controlled / With Angle Controlled / Final Project [WORKING v2]" >}}

### Accelerating Stepper Motor / WiFi Controlled / With Angle Controlled / Final Project [WORKING v2]

#### Arduino IDE

```go
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

//#define STEPS 400

// Some variables we will need along the way
const char *ssid = "salvatore ferragamo";
const char *password = "macncheese";
const char *PARAM_MESSAGE = "message";
int webServerPort = 80;

int DIR_PIN = D5;
int STEP_PIN = D4;
int ENABLE_PIN = D6;
int POT_PIN = D1;
int potValue;
int adjustPot;
int prevAdjustPot = 0;
int interval = 3000;

int stepCount = 10;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void sendResponse(AsyncWebServerRequest *request, String message) {
  AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", message);
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void setup() {

  pinMode(STEP_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  pinMode(POT_PIN, INPUT);
  digitalWrite(ENABLE_PIN, LOW);

  Serial.begin(19200);
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    sendResponse(request, "Hello!");
  });

  server.on("/motor", HTTP_GET, [](AsyncWebServerRequest *request) {
    int state;  // motor state
    int stepValue; // range slider value, which will be turned into stepCount
    
    if (request->hasParam("state")) {
      // The incoming params are Strings
      String param = request->getParam("state")->value();
      // .. so we have to interpret or cast them
      if (param == "CWturn") {
        state = 2;
      } else if (param == "CCWturn") {
        state = 3;
      } else {
        state = 0;
      }
    } else {
      state = 0;
    }

    if (request->hasParam("stepValue")){
      stepValue = request->getParam("stepValue")->value().toInt();
    }

     

    // Send back message to human

    String stateString;  // Declare the variable outside the if statement

    if (state == 2) {
      Serial.println("turningcw");
      digitalWrite(DIR_PIN, LOW);
      stepCount = stepValue;
      constantAccel();
      stateString = "isTurningCW";
    } else if (state == 3) {
      Serial.println("turningCCW");
      digitalWrite(DIR_PIN, HIGH);
      stepCount = stepValue;
      constantAccel();
      stateString = "isTurningCCW";
    } else {
      stateString = "notTurning";
    }

    String responseJSON = "{\"motorState\":\"" + stateString + "\"}";
    sendResponse(request, responseJSON);

  });

  server.on("/params", HTTP_GET, [](AsyncWebServerRequest *request) {
    int param1 = random(100);
    int param2 = random(100);
    int param3 = random(100);
    int param4 = random(100);

    String responseJSON = "{";
    responseJSON += "\"param1\":" + String(param1) + ",";
    responseJSON += "\"param2\":" + String(param2) + ",";
    responseJSON += "\"param3\":" + String(param3) + ",";
    responseJSON += "\"param4\":" + String(param4) + ",";
    responseJSON += "}";

    sendResponse(request, responseJSON);
  });

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();
}

void loop() {

}

void moveClock(int steps) {
  digitalWrite(DIR_PIN, LOW);
  for (int i = 0; i < steps; i++) {
    digitalWrite(STEP_PIN, HIGH);
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(interval);
  }
}

void moveCounterClock(int steps) {
  digitalWrite(DIR_PIN, HIGH);
  for (int i = 0; i < steps; i++) {
    digitalWrite(STEP_PIN, HIGH);
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(interval);
  }
}

void constantAccel(){
  int delays[stepCount];
  float angle = 1;
  float accel = 0.01;
  float c0 = 2000 * sqrt(2 * angle / accel ) * 0.67703;
  float lastDelay = 0;
  int highSpeed = 100;
  for (int i=0; i< stepCount; i++){
    float d = c0;
    if (i>0)
      d = lastDelay - (2 * lastDelay)/(4*i+1);
    if (d<highSpeed)
      d = highSpeed;
    delays[i] = d;
    lastDelay = d;   
  }

  for (int i= 0; i<stepCount; i++){
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds (delays[i]);
    digitalWrite(STEP_PIN, LOW);
  }    

    for (int i= 0; i<stepCount; i++){
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds (delays[stepCount-i-1]);
    digitalWrite(STEP_PIN, LOW);
  }    
}
```

#### jQuery

```go
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>BootStrap Rest DEMO</title>
  <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
</head>

<body>

  <header class="container mt-3">
    <div id="status-bar" class="alert alert-light" role="alert">
      API Status: <span id="status-api">Undefined</span>
    </div>
  </header>

  <main class="container">
    <h1>stepper control test</h1>
    <div class="form-group">
      <label for="rangeInput">Range Input:</label>
      <input type="range" class="form-range" id="rangeInput" min="0" max="385" value="50"> 
    </div>
    <p>Selected value: <span id="rangeValue"> 50 </span></p>
    
    <div id="motor-sign">Motor Rotation</div>
    <button id="button-counter-clockwise" class="btn btn-warning">Turn Counter-clockwise</button>
    <button id="button-clockwise" class="btn btn-info">Turn Clockwise</button>
  </main>

  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/jquery/jquery-3.6.4.min.js"></script>
  <script>
    function checkAPIStatus() {
      $.ajax({
        url: "http://192.168.223.93/",
        timeout: 5000
      })
        .done(function () {
          $("#status-api").text("Connected");
          if ($("#status-bar").hasClass("alert-light")) {
            $("#status-bar").removeClass("alert-light");
          }
          $("#status-bar").addClass("alert-success");
        })
        .fail(function () {
          $("#status-api").text("Not connected");
        })
    }

    function triggerTurn(direction) {
      if (direction == 2) {
        $("#motor-sign").data("motorState", "CWturn");
      } else if (direction == 3) {
        $("#motor-sign").data("motorState", "CCWturn");
      } else {
        $("#motor-sign").data("motorState", "noAction");
      }

      const buttonState = $("#motor-sign").data("motorState");
      const rangeValue = $('#rangeValue').text(); // Get the value of #rangeValue span

      console.log("sent: " + buttonState);
      console.log("sent: " + rangeValue);

      $.ajax({
        url: "http://192.168.223.93/motor",
        data: {
          state: buttonState,
          stepValue: rangeValue // Include the range value in the AJAX request
        },
        timeout: 5000
      })
        .done(function (response) {
          const responseJSON = JSON.parse(response); //we convert this into an object so that:
          console.log("received: " + responseJSON.motorState);
          if (responseJSON.motorState == "isTurningCW") {
            $("#motor-sign").css("background", "cyan");
          } else if (responseJSON.motorState == "isTurningCCW") {
            $("#motor-sign").css("background", "orange");
          } else {
            $("#motor-sign").css("background", "gray");
          }
        })
        .fail(function () {
          console.log("motor trigger call failed.");
        })
    }

    $(document).ready(function () {
      console.log("Document has loaded!");

      setInterval(checkAPIStatus, 2000);

      $("#button-clockwise").click(function () {
        triggerTurn(2);
      });

      $("#button-counter-clockwise").click(function () {
        triggerTurn(3);
      });

      $('#rangeInput').on('input', function() {
    var value = $(this).val();
    $('#rangeValue').text(value);
      });

    });
  </script>
</body>

</html>
```

{{< /detail-tag >}}

{{< detail-tag "CODE: Accelerating Stepper Motor / WiFi Controlled / With Angle Controlled / With Speed Controlled / Final Project [WORKING v3]" >}}

### Accelerating Stepper Motor / WiFi Controlled / With Angle Controlled / With Speed Controlled / Final Project [WORKING v3]

#### Arduino IDE

```go
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

//#define STEPS 400

// Some variables we will need along the way
const char *ssid = "salvatore ferragamo";
const char *password = "macncheese";
const char *PARAM_MESSAGE = "message";
int webServerPort = 80;

int DIR_PIN = D5;
int STEP_PIN = D4;
int ENABLE_PIN = D6;
int interval = 3000;

int stepCount = 10;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void sendResponse(AsyncWebServerRequest *request, String message) {
  AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", message);
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void setup() {

  pinMode(STEP_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);

  Serial.begin(19200);
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    sendResponse(request, "Hello!");
  });

  server.on("/motor", HTTP_GET, [](AsyncWebServerRequest *request) {
    int state;  // motor state
    int stepValue; // range slider value, which will be turned into stepCount
    float accelValue; // range slider value, which will be turned into accel

    if (request->hasParam("state")) {
      // The incoming params are Strings
      String param = request->getParam("state")->value();
      // .. so we have to interpret or cast them
      if (param == "CWturn") {
        state = 2;
      } else if (param == "CCWturn") {
        state = 3;
      } else {
        state = 0;
      }
    } else {
      state = 0;
    }

    if (request->hasParam("stepValue")){
      stepValue = request->getParam("stepValue")->value().toInt();
    }

    if (request->hasParam("accelValue")){
      accelValue = request->getParam("accelValue")->value().toFloat();
    }

     

    // Send back message to human

    String stateString;  // Declare the variable outside the if statement

    if (state == 2) {
      Serial.println("turningcw");
      digitalWrite(DIR_PIN, LOW);
      stepCount = stepValue;
      constantAccel(accelValue);
      stateString = "isTurningCW";
    } else if (state == 3) {
      Serial.println("turningCCW");
      digitalWrite(DIR_PIN, HIGH);
      stepCount = stepValue;
      constantAccel(accelValue);
      stateString = "isTurningCCW";
    } else {
      stateString = "notTurning";
    }

    String responseJSON = "{\"motorState\":\"" + stateString + "\"}";
    sendResponse(request, responseJSON);

  });

  server.on("/params", HTTP_GET, [](AsyncWebServerRequest *request) {
    int param1 = random(100);
    int param2 = random(100);
    int param3 = random(100);
    int param4 = random(100);

    String responseJSON = "{";
    responseJSON += "\"param1\":" + String(param1) + ",";
    responseJSON += "\"param2\":" + String(param2) + ",";
    responseJSON += "\"param3\":" + String(param3) + ",";
    responseJSON += "\"param4\":" + String(param4) + ",";
    responseJSON += "}";

    sendResponse(request, responseJSON);
  });

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();
}

void constantAccel(float accelVal){
  int delays[stepCount];
  float angle = 1;
  float accel = accelVal;
  float c0 = 2000 * sqrt(2 * angle / accel ) * 0.67703;
  float lastDelay = 0;
  int highSpeed = 100;
  for (int i=0; i< stepCount; i++){
    float d = c0;
    if (i>0){
      d = lastDelay - (2 * lastDelay)/(4*i+1);
    }      
    if (d<highSpeed){      
      d = highSpeed;
    }
      
    delays[i] = d;
    lastDelay = d;   
  }

  for (int i= 0; i<stepCount; i++){
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds (delays[i]);
    digitalWrite(STEP_PIN, LOW);
  }    

    for (int i= 0; i<stepCount; i++){
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds (delays[stepCount-i-1]);
    digitalWrite(STEP_PIN, LOW);
  }    
}

void loop() {

}
```

#### jQuery

```go
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>BootStrap Rest DEMO</title>
  <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
</head>

<body>

  <header class="container mt-3">
    <div id="status-bar" class="alert alert-light" role="alert">
      API Status: <span id="status-api">Undefined</span>
    </div>
  </header>

  <main class="container">
    <h1>stepper control test</h1>
    <div class="form-group">
      <label for="rangeInput">Angle Input:</label>
      <input type="range" class="form-range" id="rangeInput" min="0" max="192" value="50"> 
    </div>
    <p>Selected value: <span id="rangeValue"> 50 </span></p>

    <div class="form-group">
      <label for="speedInput">Speed Input:</label>
      <input type="range" class="form-range" id="speedInput" min="0.0001" max="0.03"  step="0.0001" value="0.01"> 
    </div>
    <p>Selected value: <span id="speedValue"> 0.01 </span></p>
    
    <div id="motor-sign">Motor Rotation</div>
    <button id="button-counter-clockwise" class="btn btn-warning">Turn Counter-clockwise</button>
    <button id="button-clockwise" class="btn btn-info">Turn Clockwise</button>
  </main>

  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/jquery/jquery-3.6.4.min.js"></script>
  <script>
    function checkAPIStatus() {
      $.ajax({
        url: "http://192.168.223.93/",
        timeout: 5000
      })
        .done(function () {
          $("#status-api").text("Connected");
          if ($("#status-bar").hasClass("alert-light")) {
            $("#status-bar").removeClass("alert-light");
          }
          $("#status-bar").addClass("alert-success");
        })
        .fail(function () {
          $("#status-api").text("Not connected");
        })
    }

    function triggerTurn(direction) {
      if (direction == 2) {
        $("#motor-sign").data("motorState", "CWturn");
      } else if (direction == 3) {
        $("#motor-sign").data("motorState", "CCWturn");
      } else {
        $("#motor-sign").data("motorState", "noAction");
      }

      const buttonState = $("#motor-sign").data("motorState");
      const rangeValue = $('#rangeValue').text(); // Get the value of #rangeValue span
      const speedValue = $('#speedValue').text();

      console.log("sent: " + buttonState);
      console.log("sent: " + rangeValue);
      console.log("sent: " + speedValue);

      $.ajax({
        url: "http://192.168.223.93/motor",
        data: {
          state: buttonState,
          stepValue: rangeValue, // Include the range value in the AJAX request
          accelValue: speedValue
        },
        timeout: 5000
      })
        .done(function (response) {
          const responseJSON = JSON.parse(response); //we convert this into an object so that:
          console.log("received: " + responseJSON.motorState);
          if (responseJSON.motorState == "isTurningCW") {
            $("#motor-sign").css("background", "cyan");
          } else if (responseJSON.motorState == "isTurningCCW") {
            $("#motor-sign").css("background", "orange");
          } else {
            $("#motor-sign").css("background", "gray");
          }
        })
        .fail(function () {
          console.log("motor trigger call failed.");
        })
    }

    $(document).ready(function () {
      console.log("Document has loaded!");

      setInterval(checkAPIStatus, 2000);

      $("#button-clockwise").click(function () {
        triggerTurn(2);
      });

      $("#button-counter-clockwise").click(function () {
        triggerTurn(3);
      });

      $('#rangeInput').on('input', function() {
    var value = $(this).val();
    $('#rangeValue').text(value);
      });

      $('#speedInput').on('input', function() {
    var value = $(this).val();
    $('#speedValue').text(value);
      });

    });
  </script>
</body>

</html>
```

{{< /detail-tag >}}

{{< detail-tag "CODE: Precise Stepper Motor Control / WiFi/ Keyboard Control [WORKING v1]" >}}

### Precise Stepper Motor Control / WiFi/ Keyboard Control [WORKING v1]

#### Arduino IDE
```go
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

// Some variables we will need along the way
const char *ssid = "Fablab";
const char *password = "Fabricationlab1";
const char *PARAM_MESSAGE = "message";
int webServerPort = 80;

int STEP_PIN_A = D5;
int DIR_PIN_A = D4;
int ENABLE_PIN = D10;
int STEP_PIN_B = D7;
int DIR_PIN_B = D6;
int STEP_PIN_C = D9;
int DIR_PIN_C = D8;

int stepCount = 10;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void sendResponse(AsyncWebServerRequest *request, String message) {
  AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", message);
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void setup() {

  Serial.begin(19200);
  pinMode(STEP_PIN_A,OUTPUT);
  pinMode(DIR_PIN_A,OUTPUT);
  pinMode(STEP_PIN_B,OUTPUT);
  pinMode(DIR_PIN_B,OUTPUT);
  pinMode(STEP_PIN_C,OUTPUT);
  pinMode(DIR_PIN_C,OUTPUT);
  pinMode(ENABLE_PIN,OUTPUT);
  
  digitalWrite(ENABLE_PIN, LOW);
  
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    sendResponse(request, "Hello!");
  });

  server.on("/motor", HTTP_GET, [](AsyncWebServerRequest *request) {
    int state;  // motor state
    int stepValue; // range slider value, which will be turned into stepCount
    float accelValue; // range slider value, which will be turned into accel

    if (request->hasParam("state")) {
      // The incoming params are Strings
      String param = request->getParam("state")->value();
      // .. so we have to interpret or cast them
      if (param == "CWturn") {
        state = 2;
      } else if (param == "CCWturn") {
        state = 3;
      } else {
        state = 0;
      }
    } else {
      state = 0;
    }

    if (request->hasParam("stepValue")){
      stepValue = request->getParam("stepValue")->value().toInt();
    }

    if (request->hasParam("accelValue")){
      accelValue = request->getParam("accelValue")->value().toFloat();
    }

     

    // Send back message to human

    String stateString;  // Declare the variable outside the if statement

    if (state == 2) {
      Serial.println("turningcw");
      digitalWrite(DIR_PIN_A, LOW);
      stepCount = stepValue;
      constantAccel(accelValue);
      stateString = "isTurningCW";
    } else if (state == 3) {
      Serial.println("turningCCW");
      digitalWrite(DIR_PIN_A, HIGH);
      stepCount = stepValue;
      constantAccel(accelValue);
      stateString = "isTurningCCW";
    } else {
      stateString = "notTurning";
    }

    String responseJSON = "{\"motorState\":\"" + stateString + "\"}";
    sendResponse(request, responseJSON);

  });

  server.on("/params", HTTP_GET, [](AsyncWebServerRequest *request) {
    int param1 = random(100);
    int param2 = random(100);
    int param3 = random(100);
    int param4 = random(100);

    String responseJSON = "{";
    responseJSON += "\"param1\":" + String(param1) + ",";
    responseJSON += "\"param2\":" + String(param2) + ",";
    responseJSON += "\"param3\":" + String(param3) + ",";
    responseJSON += "\"param4\":" + String(param4) + ",";
    responseJSON += "}";

    sendResponse(request, responseJSON);
  });

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();
}

void constantAccel(float accelVal){
  int delays[stepCount];
  float angle = 1;
  float accel = accelVal;
  float c0 = 2000 * sqrt(2 * angle / accel ) * 0.67703;
  float lastDelay = 0;
  int highSpeed = 100;
  for (int i=0; i< stepCount; i++){
    float d = c0;
    if (i>0){
      d = lastDelay - (2 * lastDelay)/(4*i+1);
    }      
    if (d<highSpeed){      
      d = highSpeed;
    }
      
    delays[i] = d;
    lastDelay = d;   
  }

  for (int i= 0; i<stepCount; i++){
    digitalWrite(STEP_PIN_A, HIGH);
    delayMicroseconds (delays[i]);
    digitalWrite(STEP_PIN_A, LOW);
  }    

    for (int i= 0; i<stepCount; i++){
    digitalWrite(STEP_PIN_A, HIGH);
    delayMicroseconds (delays[stepCount-i-1]);
    digitalWrite(STEP_PIN_A, LOW);
  }    
}

void loop() {

}

```

#### jQuery

```go
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>BootStrap Rest DEMO</title>
  <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
</head>

<body>

  <header class="container mt-3">
    <div id="status-bar" class="alert alert-light" role="alert">
      API Status: <span id="status-api">Undefined</span>
    </div>
  </header>

  <main class="container">
    <h1>stepper control test</h1>
    <div class="form-group">
      <label for="rangeInput">Angle Input:</label>
      <input type="range" class="form-range" id="rangeInput" min="0" max="192" value="50"> 
    </div>
    <p>Selected value: <span id="rangeValue"> 50 </span></p>

    <div class="form-group">
      <label for="speedInput">Speed Input:</label>
      <input type="range" class="form-range" id="speedInput" min="0.0001" max="0.03"  step="0.0001" value="0.01"> 
    </div>
    <p>Selected value: <span id="speedValue"> 0.01 </span></p>
    
    <div id="motor-sign">Motor Rotation</div>
    <button id="button-counter-clockwise" class="btn btn-warning">Turn Counter-clockwise</button>
    <button id="button-clockwise" class="btn btn-info">Turn Clockwise</button>
  </main>

  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/jquery/jquery-3.6.4.min.js"></script>
  <script>
    function checkAPIStatus() {
      $.ajax({
        url: "http://193.167.5.175/",
        timeout: 5000
      })
        .done(function () {
          $("#status-api").text("Connected");
          if ($("#status-bar").hasClass("alert-light")) {
            $("#status-bar").removeClass("alert-light");
          }
          $("#status-bar").addClass("alert-success");
        })
        .fail(function () {
          $("#status-api").text("Not connected");
        })
    }

    function triggerTurn(direction) {
      if (direction == 2) {
        $("#motor-sign").data("motorState", "CWturn");
      } else if (direction == 3) {
        $("#motor-sign").data("motorState", "CCWturn");
      } else {
        $("#motor-sign").data("motorState", "noAction");
      }

      const buttonState = $("#motor-sign").data("motorState");
      const rangeValue = $('#rangeValue').text(); // Get the value of #rangeValue span
      const speedValue = $('#speedValue').text();

      console.log("sent: " + buttonState);
      console.log("sent: " + rangeValue);
      console.log("sent: " + speedValue);

      $.ajax({
        url: "http://193.167.5.175/motor",
        data: {
          state: buttonState,
          stepValue: rangeValue, // Include the range value in the AJAX request
          accelValue: speedValue
        },
        timeout: 5000
      })
        .done(function (response) {
          const responseJSON = JSON.parse(response); //we convert this into an object so that:
          console.log("received: " + responseJSON.motorState);
          if (responseJSON.motorState == "isTurningCW") {
            $("#motor-sign").css("background", "cyan");
          } else if (responseJSON.motorState == "isTurningCCW") {
            $("#motor-sign").css("background", "orange");
          } else {
            $("#motor-sign").css("background", "gray");
          }
        })
        .fail(function () {
          console.log("motor trigger call failed.");
        })
    }

    $(document).ready(function () {
      console.log("Document has loaded!");

        // Keyboard event listener
  $(document).on('keypress', function (event) {
    var keyPressed = event.key;
    if (keyPressed === 'd' || keyPressed === 'D') {
      triggerTurn(2); // Simulate Turn Clockwise button click
    } else if (keyPressed === 'a' || keyPressed === 'A') {
      triggerTurn(3); // Simulate Turn Counter-clockwise button click
    }
      });

      setInterval(checkAPIStatus, 2000);

      $("#button-clockwise").click(function () {
        triggerTurn(2);
      });

      $("#button-counter-clockwise").click(function () {
        triggerTurn(3);
      });

      $('#rangeInput').on('input', function() {
    var value = $(this).val();
    $('#rangeValue').text(value);
      });

      $('#speedInput').on('input', function() {
    var value = $(this).val();
    $('#speedValue').text(value);
      });

    });
  </script>
</body>

</html>
```
{{< /detail-tag >}}

{{< detail-tag "CODE: Precise Stepper Motor Control / WiFi/ Interface With Two Motors [jQuery side is WORKING v2]" >}}

### Precise Stepper Motor Control / WiFi/ Interface With Two Motors [jQuery side is WORKING v2]

#### Arduino IDE

-none for now-

#### jQuery

This code sends the following info:
- direction data for motor a and b
- step data for motor a and b
- speed data for both. is same

```go
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Toucan Motor Control Interface</title>
  <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
</head>

<body>

  <header class="container mt-3">
    <div id="status-bar" class="alert alert-light" role="alert">
      API Status: <span id="status-api">Undefined</span>
    </div>
  </header>

  <main class="container">
    <h1>Toucan Motor Control Interface</h1>

    <div class="form-group">
      <label for="speedInput">Speed Input:</label>
      <input type="range" class="form-range" id="speedInput" min="0.0001" max="0.03" step="0.0001" value="0.01">
    </div>
    <p>Selected value: <span id="speedValue"> 0.01 </span></p>

    <h2>Motor A</h2>
    <div class="form-group">
      <label for="stepInputB">Step Input A:</label>
      <input type="range" class="form-range" id="stepInputA" min="0" max="192" value="50">
    </div>
    <p>Selected value: <span id="step-value-A"> 50 </span></p>
    
    <button id="button-motor-A-left" type="button" class="btn btn-outline-danger">left</button>
    <button id="button-motor-A-none" type="button" class="btn btn-outline-danger">none</button>
    <button id="button-motor-A-right" type="button" class="btn btn-outline-danger">right</button>
    <p>Motor A Direction: <span id="direction-motor-A"> none </span></p>

    <h2>Motor B</h2>
    <div class="form-group">
      <label for="stepInputB">Step Input B:</label>
      <input type="range" class="form-range" id="stepInputB" min="0" max="192" value="50">
    </div>
    <p>Selected value: <span id="step-value-B"> 50 </span></p>

    <button id="button-motor-B-left" type="button" class="btn btn-outline-danger">left</button>
    <button id="button-motor-B-none" type="button" class="btn btn-outline-danger">none</button>
    <button id="button-motor-B-right" type="button" class="btn btn-outline-danger">right</button>
    <p>Motor B Direction: <span id="direction-motor-B"> none </span></p>

    <br>
    <button id="button-turn" type="button" class="btn btn-primary btn-lg">TURN!</button>


  </main>

  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/jquery/jquery-3.6.4.min.js"></script>
  <script>
    function checkAPIStatus() {
      $.ajax({
        url: "http://193.167.5.175/",
        timeout: 5000
      })
        .done(function () {
          $("#status-api").text("Connected");
          if ($("#status-bar").hasClass("alert-light")) {
            $("#status-bar").removeClass("alert-light");
          }
          $("#status-bar").addClass("alert-success");
        })
        .fail(function () {
          $("#status-api").text("Not connected");
        })
    }

    function triggerTurn() {
      const directionMotorA = $("#direction-motor-A").text();
      const directionMotorB = $("#direction-motor-B").text();
      const stepValueA = $('#step-value-A').text();
      const stepValueB = $('#step-value-B').text();
      const speedValue = $('#speedValue').text();

      console.log("DirA: " + directionMotorA);
      console.log("DirB: " + directionMotorB);
      console.log("StepA: " + stepValueA);
      console.log("StepB: " + stepValueB);
      console.log("Speed: " + speedValue);

      $.ajax({
        url: "http://193.167.5.175/motor",
        data: {
          sentDirA: directionMotorA,
          sentDirB: directionMotorB,
          sentStepValueA: stepValueA,
          sentStepValueB: stepValueB,
          sentAccelValue: speedValue
        },
        timeout: 5000
      })
        .done(function (response) {
          const responseJSON = JSON.parse(response); //we convert this into an object so that:
          console.log("received: " + responseJSON.motorNo);
          if (responseJSON.motorNo == "isTurningCW") {
            $("#motor-selection").css("background", "cyan");
          } else if (responseJSON.motorNo == "isTurningCCW") {
            $("#motor-selection").css("background", "orange");
          } else {
            $("#motor-selection").css("background", "gray");
          }
        })
        .fail(function () {
          console.log("motor trigger call failed.");
        })
    }

    $(document).ready(function () {
      console.log("Document has loaded!");

      // Keyboard event listener
      $(document).on('keypress', function (event) {
        var keyPressed = event.key;
        if (keyPressed === 'd' || keyPressed === 'D') {
          triggerTurn(2); // Simulate Turn Clockwise button click
        } else if (keyPressed === 'a' || keyPressed === 'A') {
          triggerTurn(3); // Simulate Turn Counter-clockwise button click
        }
      });

      setInterval(checkAPIStatus, 2000);

      // Button click event
  // Motor A buttons
  $("#button-motor-A-left, #button-motor-A-none, #button-motor-A-right").click(function () {
    var direction = $(this).text();
    $("#direction-motor-A").text(direction);
  });

  // Motor B buttons
  $("#button-motor-B-left, #button-motor-B-none, #button-motor-B-right").click(function () {
    var direction = $(this).text();
    $("#direction-motor-B").text(direction);
  });

      $("#button-turn").click(function () {
        triggerTurn();
      });

      $('#stepInputA').on('input', function () {
        var value = $(this).val();
        $('#step-value-A').text(value);
      });

      $('#stepInputB').on('input', function () {
        var value = $(this).val();
        $('#step-value-B').text(value);
      });

      $('#speedInput').on('input', function () {
        var value = $(this).val();
        $('#speedValue').text(value);
      });

    });
  </script>
</body>

</html>
```

{{< /detail-tag >}}

{{< detail-tag "CODE: Precise Stepper Motor Control / WiFi/ Interface With Two Motors [PARTIALLY WORKING v3]" >}}

### Precise Stepper Motor Control / WiFi/ Interface With Two Motors [PARTIALLY WORKING v3]

#### Arduino IDE

- There is acceleration going on, but the motors start and stop two times. May work on it more.

```go
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

// Some variables we will need along the way
const char *ssid = "Fablab";
const char *password = "Fabricationlab1";
const char *PARAM_MESSAGE = "message";
int webServerPort = 80;

int STEP_PIN_A = D5;
int DIR_PIN_A = D4;
int ENABLE_PIN = D10;
int STEP_PIN_B = D7;
int DIR_PIN_B = D6;
int STEP_PIN_C = D9;
int DIR_PIN_C = D8;

int stepCount = 10;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void sendResponse(AsyncWebServerRequest *request, String message) {
  AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", message);
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void setup() {

  Serial.begin(19200);
  pinMode(STEP_PIN_A,OUTPUT);
  pinMode(DIR_PIN_A,OUTPUT);
  pinMode(STEP_PIN_B,OUTPUT);
  pinMode(DIR_PIN_B,OUTPUT);
  pinMode(STEP_PIN_C,OUTPUT);
  pinMode(DIR_PIN_C,OUTPUT);
  pinMode(ENABLE_PIN,OUTPUT);
  
  digitalWrite(ENABLE_PIN, LOW);
  
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    sendResponse(request, "Hello!");
  });

  server.on("/motor", HTTP_GET, [](AsyncWebServerRequest *request) {
    int stepValueA;
    int stepValueB;
    float accelValue; 

    if (request->hasParam("sentStepValueA")){
      stepValueA = request->getParam("sentStepValueA")->value().toInt();
    }

    if (request->hasParam("sentStepValueB")){
      stepValueB = request->getParam("sentStepValueB")->value().toInt();
    }

    if (request->hasParam("sentAccelValue")){
      accelValue = request->getParam("sentAccelValue")->value().toFloat();
    }


    if (request->hasParam("sentDirA")) {
      // The incoming params are Strings
      String param = request->getParam("sentDirA")->value();
      // .. so we have to interpret or cast them
      if (param == "counter-clockwise") {
        digitalWrite(DIR_PIN_A, LOW);
        Serial.println("dir a ccw");
      } else if (param == "clockwise") {
        digitalWrite(DIR_PIN_A, HIGH);
           Serial.println("dir a cw");
      } else {
        stepValueA = 0;
      }
    } 

    if (request->hasParam("sentDirB")) {
      // The incoming params are Strings
      String param = request->getParam("sentDirB")->value();
      // .. so we have to interpret or cast them
      if (param == "counter-clockwise") {
        digitalWrite(DIR_PIN_B, LOW);
        Serial.println("dir b ccw");
      } else if (param == "clockwise") {
        digitalWrite(DIR_PIN_B, HIGH);
        Serial.println("dir b cw");
      } else {
        stepValueB=0;
      }
    }     
     

    // Send back message to human

    String stateString;  // Declare the variable outside the if statement
    stateString = "done";
        
    Serial.println(stepValueA); 
    Serial.println(stepValueB);    
    Serial.println(accelValue);   

    motorMove(stepValueA, stepValueB, accelValue);
    
    /*
    if (state == 2) {
      Serial.println("turningcw");
      digitalWrite(DIR_PIN_A, LOW);
      digitalWrite(DIR_PIN_B, LOW);
      stepCount = stepValue;
      constantAccel(accelValue);
      stateString = "isTurningCW";
    } else if (state == 3) {
      Serial.println("turningCCW");
      digitalWrite(DIR_PIN_A, HIGH);
      digitalWrite(DIR_PIN_B, HIGH);
      stepCount = stepValue;
      constantAccel(accelValue);
      stateString = "isTurningCCW";
    } else {
      stateString = "notTurning";
    }
*/    

    String responseJSON = "{\"motorState\":\"" + stateString + "\"}";
    sendResponse(request, responseJSON);

  });

  server.on("/params", HTTP_GET, [](AsyncWebServerRequest *request) {
    int param1 = random(100);
    int param2 = random(100);
    int param3 = random(100);
    int param4 = random(100);

    String responseJSON = "{";
    responseJSON += "\"param1\":" + String(param1) + ",";
    responseJSON += "\"param2\":" + String(param2) + ",";
    responseJSON += "\"param3\":" + String(param3) + ",";
    responseJSON += "\"param4\":" + String(param4) + ",";
    responseJSON += "}";

    sendResponse(request, responseJSON);
  });

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();
}

/*

void constantAccel(float accelVal){
  int delays[stepCount];
  float angle = 1;
  float accel = accelVal;
  float c0 = 2000 * sqrt(2 * angle / accel ) * 0.67703;
  float lastDelay = 0;
  int highSpeed = 100;
  for (int i=0; i< stepCount; i++){
    float d = c0;
    if (i>0){
      d = lastDelay - (2 * lastDelay)/(4*i+1);
    }      
    if (d<highSpeed){      
      d = highSpeed;
    }
      
    delays[i] = d;
    lastDelay = d;   
  }

  for (int i= 0; i<stepCount || i<100; i++){
    if(i<stepCount)
    digitalWrite(STEP_PIN_A, HIGH);
    if(i<100)
    digitalWrite(STEP_PIN_B, HIGH);

    delayMicroseconds (delays[i]);

    if(i<stepCount)
    digitalWrite(STEP_PIN_A, LOW);
    if(i<100)
    digitalWrite(STEP_PIN_B, LOW);
  }    

    for (int i= 0; i<stepCount || i<100 ; i++){
    if(i<stepCount)
    digitalWrite(STEP_PIN_A, HIGH);
    if(i<100)    
    digitalWrite(STEP_PIN_B, HIGH);
    
    delayMicroseconds (delays[stepCount-i-1]);

    if(i<stepCount)
    digitalWrite(STEP_PIN_A, LOW);
    if(i<100)
    digitalWrite(STEP_PIN_B, LOW);
  }    
}

*/

void motorMove(int stepValueA, int stepValueB, float accelValue){
  int delaysA[stepValueA];
  float angleA = 1;
  float accelA = accelValue;
  float c0A = 2000 * sqrt(2 * angleA / accelA ) * 0.67703;
  float lastDelayA = 0;
  int highSpeedA = 100;
  for (int i=0; i< stepValueA; i++){
    float d = c0A;
    if (i>0){
      d = lastDelayA - (2 * lastDelayA)/(4*i+1);
    }      
    if (d<highSpeedA){      
      d = highSpeedA;
    }
      
    delaysA[i] = d;
    lastDelayA = d;   
  }


  int delaysB[stepValueB];
  float angleB = 1;
  float accelB = accelValue;
  float c0B = 2000 * sqrt(2 * angleB / accelB ) * 0.67703;
  float lastDelayB = 0;
  int highSpeedB = 100;
  for (int i=0; i< stepValueB; i++){
    float d = c0B;
    if (i>0){
      d = lastDelayB - (2 * lastDelayB)/(4*i+1);
    }      
    if (d<highSpeedB){      
      d = highSpeedB;
    }
      
    delaysB[i] = d;
    lastDelayB = d;   
  }
/*
  
  for (int i= 0 int j=0; i<stepValueA || j<stepValueB; j++; i++){
    if(i<stepValueA)
    digitalWrite(STEP_PIN_A, HIGH);
    if(i<stepValueB)
    digitalWrite(STEP_PIN_B, HIGH);

    if(i<stepValueA)
    delayMicroseconds (delaysA[i]);
    if(i<stepValueB)
    delayMicroseconds (delaysB[i]);

    if(i<stepValueA)
    digitalWrite(STEP_PIN_A, LOW);
    if(i<stepValueB)
    digitalWrite(STEP_PIN_B, LOW);
  }    

    for (int i= 0; i<stepValueA && i<stepValueB; i++){
    if(i<stepValueA)
    digitalWrite(STEP_PIN_A, HIGH);
    if(i<stepValueB)
    digitalWrite(STEP_PIN_B, HIGH);

    if(i<stepValueA)
    delayMicroseconds (delaysA[stepValueA-i-1]);
    if(i<stepValueB)
    delayMicroseconds (delaysB[stepValueB-i-1]);

    if(i<stepValueA)
    digitalWrite(STEP_PIN_A, LOW);
    if(i<stepValueB)
    digitalWrite(STEP_PIN_B, LOW);
  }   
  */
  int i = 0;
  int j = 0;

  while (i < stepValueA || j < stepValueB) {
    if (i < stepValueA) {
      digitalWrite(STEP_PIN_A, HIGH);
      delayMicroseconds(delaysA[i]);
      digitalWrite(STEP_PIN_A, LOW);
      i++;
    }

    if (j < stepValueB) {
      digitalWrite(STEP_PIN_B, HIGH);
      delayMicroseconds(delaysB[j]);
      digitalWrite(STEP_PIN_B, LOW);
      j++;
    }
  }

  i = 0;
  j = 0;

  while (i < stepValueA || j < stepValueB) {
    if (i < stepValueA) {
      digitalWrite(STEP_PIN_A, HIGH);
      delayMicroseconds(delaysA[stepValueA-i-1]);
      digitalWrite(STEP_PIN_A, LOW);
      i++;
    }

    if (j < stepValueB) {
      digitalWrite(STEP_PIN_B, HIGH);
      delayMicroseconds(delaysB[stepValueB-j-1]);
      digitalWrite(STEP_PIN_B, LOW);
      j++;
    }
  }
  

}


void loop() {

}
```

#### jQuery

```go
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Toucan Motor Control Interface</title>
  <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
</head>

<body>

  <header class="container mt-3">
    <div id="status-bar" class="alert alert-light" role="alert">
      API Status: <span id="status-api">Undefined</span>
    </div>
  </header>

  <main class="container">
    <h1>Toucan Motor Control Interface</h1>

    <div class="form-group">
      <label for="speedInput">Speed Input:</label>
      <input type="range" class="form-range" id="speedInput" min="0.0001" max="0.03" step="0.0001" value="0.01">
    </div>
    <p>Selected value: <span id="speedValue"> 0.01 </span></p>

    <h2>Motor A</h2>
    <div class="form-group">
      <label for="stepInputB">Step Input A:</label>
      <input type="range" class="form-range" id="stepInputA" min="0" max="192" value="50">
    </div>
    <p>Selected value: <span id="step-value-A"> 50 </span></p>
    
    <button id="button-motor-A-left" type="button" class="btn btn-outline-danger">counter-clockwise</button>
    <button id="button-motor-A-none" type="button" class="btn btn-outline-danger">none</button>
    <button id="button-motor-A-right" type="button" class="btn btn-outline-danger">clockwise</button>
    <p>Motor A Direction: <span id="direction-motor-A"> none </span></p>

    <h2>Motor B</h2>
    <div class="form-group">
      <label for="stepInputB">Step Input B:</label>
      <input type="range" class="form-range" id="stepInputB" min="0" max="192" value="50">
    </div>
    <p>Selected value: <span id="step-value-B"> 50 </span></p>

    <button id="button-motor-B-left" type="button" class="btn btn-outline-danger">counter-clockwise</button>
    <button id="button-motor-B-none" type="button" class="btn btn-outline-danger">none</button>
    <button id="button-motor-B-right" type="button" class="btn btn-outline-danger">clockwise</button>
    <p>Motor B Direction: <span id="direction-motor-B"> none </span></p>

    <br>
    <button id="button-turn" type="button" class="btn btn-primary btn-lg">TURN!</button>


  </main>

  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/jquery/jquery-3.6.4.min.js"></script>
  <script>
    function checkAPIStatus() {
      $.ajax({
        url: "http://193.167.5.175/",
        timeout: 5000
      })
        .done(function () {
          $("#status-api").text("Connected");
          if ($("#status-bar").hasClass("alert-light")) {
            $("#status-bar").removeClass("alert-light");
          }
          $("#status-bar").addClass("alert-success");
        })
        .fail(function () {
          $("#status-api").text("Not connected");
        })
    }

    function triggerTurn() {
      const directionMotorA = $("#direction-motor-A").text();
      const directionMotorB = $("#direction-motor-B").text();
      const stepValueA = $('#step-value-A').text();
      const stepValueB = $('#step-value-B').text();
      const speedValue = $('#speedValue').text();

      console.log("DirA: " + directionMotorA);
      console.log("DirB: " + directionMotorB);
      console.log("StepA: " + stepValueA);
      console.log("StepB: " + stepValueB);
      console.log("Speed: " + speedValue);

      $.ajax({
        url: "http://193.167.5.175/motor",
        data: {
          sentDirA: directionMotorA,
          sentDirB: directionMotorB,
          sentStepValueA: stepValueA,
          sentStepValueB: stepValueB,
          sentAccelValue: speedValue
        },
        timeout: 5000
      })
        .done(function (response) {
          const responseJSON = JSON.parse(response); //we convert this into an object so that:
          console.log("received: " + responseJSON.motorState);
        })
        .fail(function () {
          console.log("motor trigger call failed.");
        })
    }

    $(document).ready(function () {
      console.log("Document has loaded!");

      // Keyboard event listener
      $(document).on('keypress', function (event) {
        var keyPressed = event.key;
        if (keyPressed === 'd' || keyPressed === 'D') {
          triggerTurn(2); // Simulate Turn Clockwise button click
        } else if (keyPressed === 'a' || keyPressed === 'A') {
          triggerTurn(3); // Simulate Turn Counter-clockwise button click
        }
      });

      setInterval(checkAPIStatus, 2000);

      // Button click event
  // Motor A buttons
  $("#button-motor-A-left, #button-motor-A-none, #button-motor-A-right").click(function () {
    var direction = $(this).text();
    $("#direction-motor-A").text(direction);
  });

  // Motor B buttons
  $("#button-motor-B-left, #button-motor-B-none, #button-motor-B-right").click(function () {
    var direction = $(this).text();
    $("#direction-motor-B").text(direction);
  });

      $("#button-turn").click(function () {
        triggerTurn();
      });

      $('#stepInputA').on('input', function () {
        var value = $(this).val();
        $('#step-value-A').text(value);
      });

      $('#stepInputB').on('input', function () {
        var value = $(this).val();
        $('#step-value-B').text(value);
      });

      $('#speedInput').on('input', function () {
        var value = $(this).val();
        $('#speedValue').text(value);
      });

    });
  </script>
</body>

</html>
```

{{< /detail-tag >}}


{{< detail-tag "CODE: Precise Stepper Motor Control / WiFi/ Interface With Two Motors/ No Acceleration [PARTIALLY WORKING v4]" >}}

### Precise Stepper Motor Control / WiFi/ Interface With Two Motors/ No Acceleration [WORKING v4]

#### Arduino IDE
```go
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

// Some variables we will need along the way
const char *ssid = "Fablab";
const char *password = "Fabricationlab1";
const char *PARAM_MESSAGE = "message";
int webServerPort = 80;

int STEP_PIN_A = D5;
int DIR_PIN_A = D4;
int ENABLE_PIN = D10;
int STEP_PIN_B = D7;
int DIR_PIN_B = D6;
int STEP_PIN_C = D9;
int DIR_PIN_C = D8;

int stepCount = 10;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void sendResponse(AsyncWebServerRequest *request, String message) {
  AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", message);
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void setup() {

  Serial.begin(19200);
  pinMode(STEP_PIN_A,OUTPUT);
  pinMode(DIR_PIN_A,OUTPUT);
  pinMode(STEP_PIN_B,OUTPUT);
  pinMode(DIR_PIN_B,OUTPUT);
  pinMode(STEP_PIN_C,OUTPUT);
  pinMode(DIR_PIN_C,OUTPUT);
  pinMode(ENABLE_PIN,OUTPUT);
  
  digitalWrite(ENABLE_PIN, LOW);
  
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    sendResponse(request, "Hello!");
  });

  server.on("/motor", HTTP_GET, [](AsyncWebServerRequest *request) {
    int stepValueA;
    int stepValueB;
    int accelValue; 

    if (request->hasParam("sentStepValueA")){
      stepValueA = request->getParam("sentStepValueA")->value().toInt();
    }

    if (request->hasParam("sentStepValueB")){
      stepValueB = request->getParam("sentStepValueB")->value().toInt();
    }

    if (request->hasParam("sentAccelValue")){
      accelValue = request->getParam("sentAccelValue")->value().toInt();
    }


    if (request->hasParam("sentDirA")) {
      // The incoming params are Strings
      String param = request->getParam("sentDirA")->value();
      // .. so we have to interpret or cast them
      if (param == "counter-clockwise") {
        digitalWrite(DIR_PIN_A, LOW);
        Serial.println("dir a ccw");
      } else if (param == "clockwise") {
        digitalWrite(DIR_PIN_A, HIGH);
           Serial.println("dir a cw");
      } else {
        stepValueA = 0;
      }
    } 

    if (request->hasParam("sentDirB")) {
      // The incoming params are Strings
      String param = request->getParam("sentDirB")->value();
      // .. so we have to interpret or cast them
      if (param == "counter-clockwise") {
        digitalWrite(DIR_PIN_B, LOW);
        Serial.println("dir b ccw");
      } else if (param == "clockwise") {
        digitalWrite(DIR_PIN_B, HIGH);
        Serial.println("dir b cw");
      } else {
        stepValueB=0;
      }
    }     
     

    // Send back message to human

    String stateString;  // Declare the variable outside the if statement
    stateString = "done";
        
    Serial.println(stepValueA); 
    Serial.println(stepValueB);    
    Serial.println(accelValue);   

    motorMove(stepValueA, stepValueB, accelValue);
    

    String responseJSON = "{\"motorState\":\"" + stateString + "\"}";
    sendResponse(request, responseJSON);

  });

  server.on("/params", HTTP_GET, [](AsyncWebServerRequest *request) {
    int param1 = random(100);
    int param2 = random(100);
    int param3 = random(100);
    int param4 = random(100);

    String responseJSON = "{";
    responseJSON += "\"param1\":" + String(param1) + ",";
    responseJSON += "\"param2\":" + String(param2) + ",";
    responseJSON += "\"param3\":" + String(param3) + ",";
    responseJSON += "\"param4\":" + String(param4) + ",";
    responseJSON += "}";

    sendResponse(request, responseJSON);
  });

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();
}


void motorMove(int stepValueA, int stepValueB, int accelValue){

  for (int i = 0, j = 0; i < stepValueA || j < stepValueB; i++, j++) {
    if (i < stepValueA) {
      digitalWrite(STEP_PIN_A, HIGH);
      delayMicroseconds(accelValue);
      digitalWrite(STEP_PIN_A, LOW);
    }

    if (j < stepValueB) {
      digitalWrite(STEP_PIN_B, HIGH);
      delayMicroseconds(accelValue);
      digitalWrite(STEP_PIN_B, LOW);
    }

  }

}


void loop() {

}
```

#### jQuery
```go
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Toucan Motor Control Interface</title>
  <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
</head>

<body>

  <header class="container mt-3">
    <div id="status-bar" class="alert alert-light" role="alert">
      API Status: <span id="status-api">Undefined</span>
    </div>
  </header>

  <main class="container">
    <h1>Toucan Motor Control Interface</h1>

    <div class="form-group">
      <label for="speedInput">Speed Input:</label>
      <input type="range" class="form-range" id="speedInput" min="1000" max="30000" value="5000">
    </div>
    <p>Selected value: <span id="speedValue"> 5000 </span></p>

    <h2>Motor A</h2>
    <div class="form-group">
      <label for="stepInputB">Step Input A:</label>
      <input type="range" class="form-range" id="stepInputA" min="0" max="192" value="50">
    </div>
    <p>Selected value: <span id="step-value-A"> 50 </span></p>
    
    <button id="button-motor-A-left" type="button" class="btn btn-outline-danger">counter-clockwise</button>
    <button id="button-motor-A-none" type="button" class="btn btn-outline-danger">none</button>
    <button id="button-motor-A-right" type="button" class="btn btn-outline-danger">clockwise</button>
    <p>Motor A Direction: <span id="direction-motor-A"> none </span></p>

    <h2>Motor B</h2>
    <div class="form-group">
      <label for="stepInputB">Step Input B:</label>
      <input type="range" class="form-range" id="stepInputB" min="0" max="192" value="50">
    </div>
    <p>Selected value: <span id="step-value-B"> 50 </span></p>

    <button id="button-motor-B-left" type="button" class="btn btn-outline-danger">counter-clockwise</button>
    <button id="button-motor-B-none" type="button" class="btn btn-outline-danger">none</button>
    <button id="button-motor-B-right" type="button" class="btn btn-outline-danger">clockwise</button>
    <p>Motor B Direction: <span id="direction-motor-B"> none </span></p>

    <br>
    <button id="button-turn" type="button" class="btn btn-primary btn-lg">TURN!</button>


  </main>

  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/jquery/jquery-3.6.4.min.js"></script>
  <script>
    function checkAPIStatus() {
      $.ajax({
        url: "http://193.167.5.175/",
        timeout: 5000
      })
        .done(function () {
          $("#status-api").text("Connected");
          if ($("#status-bar").hasClass("alert-light")) {
            $("#status-bar").removeClass("alert-light");
          }
          $("#status-bar").addClass("alert-success");
        })
        .fail(function () {
          $("#status-api").text("Not connected");
        })
    }

    function triggerTurn() {
      const directionMotorA = $("#direction-motor-A").text();
      const directionMotorB = $("#direction-motor-B").text();
      const stepValueA = $('#step-value-A').text();
      const stepValueB = $('#step-value-B').text();
      const speedValue = $('#speedValue').text();

      console.log("DirA: " + directionMotorA);
      console.log("DirB: " + directionMotorB);
      console.log("StepA: " + stepValueA);
      console.log("StepB: " + stepValueB);
      console.log("Speed: " + speedValue);

      $.ajax({
        url: "http://193.167.5.175/motor",
        data: {
          sentDirA: directionMotorA,
          sentDirB: directionMotorB,
          sentStepValueA: stepValueA,
          sentStepValueB: stepValueB,
          sentAccelValue: speedValue
        },
        timeout: 5000
      })
        .done(function (response) {
          const responseJSON = JSON.parse(response); //we convert this into an object so that:
          console.log("received: " + responseJSON.motorState);
        })
        .fail(function () {
          console.log("motor trigger call failed.");
        })
    }

    $(document).ready(function () {
      console.log("Document has loaded!");

      // Keyboard event listener
      $(document).on('keypress', function (event) {
        var keyPressed = event.key;
        if (keyPressed === 'd' || keyPressed === 'D') {
          triggerTurn(2); // Simulate Turn Clockwise button click
        } else if (keyPressed === 'a' || keyPressed === 'A') {
          triggerTurn(3); // Simulate Turn Counter-clockwise button click
        }
      });

      setInterval(checkAPIStatus, 2000);

      // Button click event
  // Motor A buttons
  $("#button-motor-A-left, #button-motor-A-none, #button-motor-A-right").click(function () {
    var direction = $(this).text();
    $("#direction-motor-A").text(direction);
  });

  // Motor B buttons
  $("#button-motor-B-left, #button-motor-B-none, #button-motor-B-right").click(function () {
    var direction = $(this).text();
    $("#direction-motor-B").text(direction);
  });

      $("#button-turn").click(function () {
        triggerTurn();
      });

      $('#stepInputA').on('input', function () {
        var value = $(this).val();
        $('#step-value-A').text(value);
      });

      $('#stepInputB').on('input', function () {
        var value = $(this).val();
        $('#step-value-B').text(value);
      });

      $('#speedInput').on('input', function () {
        var value = $(this).val();
        $('#speedValue').text(value);
      });

    });
  </script>
</body>

</html>
```
{{< /detail-tag >}}

{{< detail-tag "CODE: Sequential Stepper Motor Control / WiFi/ Interface With Two Motors [WORKING - VIDEO 07.06.23]" >}}

### Sequential Stepper Motor Control / WiFi/ Interface With Two Motors [WORKING - VIDEO 07.06.23]

#### Arduino IDE

```go
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

// Some variables we will need along the way
const char *ssid = "Fablab";
const char *password = "Fabricationlab1";
const char *PARAM_MESSAGE = "message";
int webServerPort = 80;

int STEP_PIN_A = D5;
int DIR_PIN_A = D4;
int ENABLE_PIN = D10;
int STEP_PIN_B = D7;
int DIR_PIN_B = D6;
//int STEP_PIN_C = D9;
//int DIR_PIN_C = D8;

int stepCount = 10;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void sendResponse(AsyncWebServerRequest *request, String message) {
  AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", message);
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void setup() {

  Serial.begin(19200);
  pinMode(STEP_PIN_A,OUTPUT);
  pinMode(DIR_PIN_A,OUTPUT);
  pinMode(STEP_PIN_B,OUTPUT);
  pinMode(DIR_PIN_B,OUTPUT);
 // pinMode(STEP_PIN_C,OUTPUT);
 // pinMode(DIR_PIN_C,OUTPUT);
  pinMode(ENABLE_PIN,OUTPUT);
  
  digitalWrite(ENABLE_PIN, LOW);
  
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    sendResponse(request, "Hello!");
  });

  server.on("/motor", HTTP_GET, [](AsyncWebServerRequest *request) {
    int stepValueA;
    int stepValueB;
    int accelValue; 

    if (request->hasParam("sentStepValueA")){
      stepValueA = request->getParam("sentStepValueA")->value().toInt();
    }

    if (request->hasParam("sentStepValueB")){
      stepValueB = request->getParam("sentStepValueB")->value().toInt();
    }

    if (request->hasParam("sentAccelValue")){
      accelValue = request->getParam("sentAccelValue")->value().toInt();
    }


    if (request->hasParam("sentDirA")) {
      // The incoming params are Strings
      String param = request->getParam("sentDirA")->value();
      // .. so we have to interpret or cast them
      if (param == "counter-clockwise") {
        digitalWrite(DIR_PIN_A, HIGH);
        Serial.println("dir a ccw");
      } else if (param == "clockwise") {
        digitalWrite(DIR_PIN_A, LOW);
           Serial.println("dir a cw");
      } else {
        stepValueA = 0;
      }
    } 

    if (request->hasParam("sentDirB")) {
      // The incoming params are Strings
      String param = request->getParam("sentDirB")->value();
      // .. so we have to interpret or cast them
      if (param == "counter-clockwise") {
        digitalWrite(DIR_PIN_B, HIGH);
        Serial.println("dir b ccw");
      } else if (param == "clockwise") {
        digitalWrite(DIR_PIN_B, LOW);
        Serial.println("dir b cw");
      } else {
        stepValueB=0;
      }
    }     
     

    // Send back message to human

    String stateString;  // Declare the variable outside the if statement
    stateString = "done";
        
    Serial.println(stepValueA); 
    Serial.println(stepValueB);    
    Serial.println(accelValue);   

    motorMove(stepValueA, stepValueB, accelValue);
    

    String responseJSON = "{\"motorState\":\"" + stateString + "\"}";
    sendResponse(request, responseJSON);

  });

  server.on("/params", HTTP_GET, [](AsyncWebServerRequest *request) {
    int param1 = random(100);
    int param2 = random(100);
    int param3 = random(100);
    int param4 = random(100);

    String responseJSON = "{";
    responseJSON += "\"param1\":" + String(param1) + ",";
    responseJSON += "\"param2\":" + String(param2) + ",";
    responseJSON += "\"param3\":" + String(param3) + ",";
    responseJSON += "\"param4\":" + String(param4) + ",";
    responseJSON += "}";

    sendResponse(request, responseJSON);
  });

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();
}


void motorMove(int stepValueA, int stepValueB, float accelValue) {
  int i = 0;
  int j = 0;

  while (i < stepValueA) {
    digitalWrite(STEP_PIN_A, HIGH);
    delayMicroseconds(accelValue);
    digitalWrite(STEP_PIN_A, LOW);
    i++;
  }

  while (j < stepValueB) {
    digitalWrite(STEP_PIN_B, HIGH);
    delayMicroseconds(accelValue);
    digitalWrite(STEP_PIN_B, LOW);
    j++;
  }
}



void loop() {

}
```

#### jQuery

```go
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Toucan Motor Control Interface</title>
  <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
</head>

<body>

  <header class="container mt-3">
    <div id="status-bar" class="alert alert-light" role="alert">
      API Status: <span id="status-api">Undefined</span>
    </div>
  </header>

  <main class="container">
    <h1>Toucan Motor Control Interface</h1>

    <div class="form-group">
      <label for="speedInput">Speed Input:</label>
      <input type="range" class="form-range" id="speedInput" min="1000" max="30000" value="5000">
    </div>
    <p>Selected value: <span id="speedValue"> 5000 </span></p>

    <h2>Motor A</h2>
    <div class="form-group">
      <label for="stepInputB">Step Input A:</label>
      <input type="range" class="form-range" id="stepInputA" min="0" max="192" value="50">
    </div>
    <p>Selected value: <span id="step-value-A"> 50 </span></p>
    
    <button id="button-motor-A-left" type="button" class="btn btn-outline-danger">counter-clockwise</button>
    <button id="button-motor-A-none" type="button" class="btn btn-outline-danger">none</button>
    <button id="button-motor-A-right" type="button" class="btn btn-outline-danger">clockwise</button>
    <p>Motor A Direction: <span id="direction-motor-A"> none </span></p>

    <h2>Motor B</h2>
    <div class="form-group">
      <label for="stepInputB">Step Input B:</label>
      <input type="range" class="form-range" id="stepInputB" min="0" max="192" value="50">
    </div>
    <p>Selected value: <span id="step-value-B"> 50 </span></p>

    <button id="button-motor-B-left" type="button" class="btn btn-outline-danger">counter-clockwise</button>
    <button id="button-motor-B-none" type="button" class="btn btn-outline-danger">none</button>
    <button id="button-motor-B-right" type="button" class="btn btn-outline-danger">clockwise</button>
    <p>Motor B Direction: <span id="direction-motor-B"> none </span></p>

    <br>
    <button id="button-turn" type="button" class="btn btn-primary btn-lg">TURN!</button>


  </main>

  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/jquery/jquery-3.6.4.min.js"></script>
  <script>
    function checkAPIStatus() {
      $.ajax({
        url: "http://193.167.5.175/",
        timeout: 5000
      })
        .done(function () {
          $("#status-api").text("Connected");
          if ($("#status-bar").hasClass("alert-light")) {
            $("#status-bar").removeClass("alert-light");
          }
          $("#status-bar").addClass("alert-success");
        })
        .fail(function () {
          $("#status-api").text("Not connected");
        })
    }

    function triggerTurn() {
      const directionMotorA = $("#direction-motor-A").text();
      const directionMotorB = $("#direction-motor-B").text();
      const stepValueA = $('#step-value-A').text();
      const stepValueB = $('#step-value-B').text();
      const speedValue = $('#speedValue').text();

      console.log("DirA: " + directionMotorA);
      console.log("DirB: " + directionMotorB);
      console.log("StepA: " + stepValueA);
      console.log("StepB: " + stepValueB);
      console.log("Speed: " + speedValue);

      $.ajax({
        url: "http://193.167.5.175/motor",
        data: {
          sentDirA: directionMotorA,
          sentDirB: directionMotorB,
          sentStepValueA: stepValueA,
          sentStepValueB: stepValueB,
          sentAccelValue: speedValue
        },
        timeout: 5000
      })
        .done(function (response) {
          const responseJSON = JSON.parse(response); //we convert this into an object so that:
          console.log("received: " + responseJSON.motorState);
        })
        .fail(function () {
          console.log("motor trigger call failed.");
        })
    }

    $(document).ready(function () {
      console.log("Document has loaded!");

      // Keyboard event listener
      $(document).on('keypress', function (event) {
        var keyPressed = event.key;
        if (keyPressed === 'd' || keyPressed === 'D') {
          triggerTurn(2); // Simulate Turn Clockwise button click
        } else if (keyPressed === 'a' || keyPressed === 'A') {
          triggerTurn(3); // Simulate Turn Counter-clockwise button click
        }
      });

      setInterval(checkAPIStatus, 2000);

      // Button click event
  // Motor A buttons
  $("#button-motor-A-left, #button-motor-A-none, #button-motor-A-right").click(function () {
    var direction = $(this).text();
    $("#direction-motor-A").text(direction);
  });

  // Motor B buttons
  $("#button-motor-B-left, #button-motor-B-none, #button-motor-B-right").click(function () {
    var direction = $(this).text();
    $("#direction-motor-B").text(direction);
  });

      $("#button-turn").click(function () {
        triggerTurn();
      });

      $('#stepInputA').on('input', function () {
        var value = $(this).val();
        $('#step-value-A').text(value);
      });

      $('#stepInputB').on('input', function () {
        var value = $(this).val();
        $('#step-value-B').text(value);
      });

      $('#speedInput').on('input', function () {
        var value = $(this).val();
        $('#speedValue').text(value);
      });

    });
  </script>
</body>

</html>
```

{{< /detail-tag >}}